-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 21, 2020 at 03:58 AM
-- Server version: 5.7.24
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `other_same`
--

-- --------------------------------------------------------

--
-- Table structure for table `hospitalization_reasons`
--

CREATE TABLE `hospitalization_reasons` (
  `id` int(11) NOT NULL,
  `reason` varchar(500) DEFAULT NULL,
  `is_user` tinyint(1) DEFAULT '0',
  `added_by` varchar(80) DEFAULT NULL,
  `modified_by` varchar(80) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hospitalization_reasons`
--

INSERT INTO `hospitalization_reasons` (`id`, `reason`, `is_user`, `added_by`, `modified_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Cardiac arrhythmias', 0, NULL, NULL, 1, NULL, NULL),
(2, 'Congestive heart failure', 0, NULL, NULL, 1, NULL, NULL),
(3, 'Chronic obstructive pulmonary disease (COPD)', 0, NULL, NULL, 1, NULL, NULL),
(4, 'Coronary atherosclerosis', 0, NULL, NULL, 1, NULL, NULL),
(5, 'Diabetes', 0, NULL, NULL, 1, NULL, NULL),
(6, 'Infection', 0, NULL, NULL, 1, NULL, NULL),
(7, 'Medication problems', 0, NULL, NULL, 1, NULL, NULL),
(8, 'Pneumonia', 0, NULL, NULL, 1, NULL, NULL),
(9, 'Stroke', 0, NULL, NULL, 1, NULL, NULL);
-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `id` int(11) NOT NULL,
  `userId` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `token` varchar(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` varchar(50) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `profile` varchar(300) DEFAULT NULL,
  `language` varchar(50) DEFAULT 'english',
  `type` varchar(50) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `step1` tinyint(1) DEFAULT '0',
  `newsletter` tinyint(1) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_hospitalization`
--

CREATE TABLE `user_hospitalization` (
  `id` int(11) NOT NULL,
  `userId` varchar(80) DEFAULT NULL,
  `hospitalization_reason_id` int(11) DEFAULT NULL,
  `admitted_year` varchar(20) DEFAULT NULL,
  `admitted_month` varchar(20) DEFAULT NULL,
  `admitted_date` varchar(20) DEFAULT NULL,
  `is_discharged` tinyint(1) DEFAULT NULL,
  `discharged_year` varchar(20) DEFAULT NULL,
  `discharged_month` varchar(20) DEFAULT NULL,
  `discharged_date` varchar(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_hospitalization`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `id` int(11) NOT NULL,
  `userId` varchar(50) NOT NULL,
  `age` varchar(20) DEFAULT NULL,
  `sex` varchar(100) DEFAULT NULL,
  `pronoun` varchar(100) DEFAULT NULL,
  `gender_identity` varchar(100) DEFAULT NULL,
  `birth_year` int(11) DEFAULT NULL,
  `birth_month` int(11) DEFAULT NULL,
  `birth_day` int(11) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `zip` varchar(50) DEFAULT NULL,
  `brief_bio` varchar(500) DEFAULT NULL,
  `ethnicity` varchar(250) DEFAULT NULL,
  `race` varchar(250) DEFAULT NULL,
  `education` varchar(250) DEFAULT NULL,
  `insurance` varchar(250) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_interests`
--

CREATE TABLE `user_interests` (
  `id` int(11) NOT NULL,
  `userId` varchar(80) NOT NULL,
  `primary_interest` varchar(200) DEFAULT NULL,
  `secondary_interest` varchar(1000) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_interests`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_military`
--

CREATE TABLE `user_military` (
  `id` int(11) NOT NULL,
  `userId` varchar(50) NOT NULL,
  `military_status` varchar(300) DEFAULT NULL,
  `military_status_label` varchar(150) DEFAULT NULL,
  `service_start_year` int(11) DEFAULT NULL,
  `service_start_month` int(11) DEFAULT NULL,
  `service_end_year` int(11) DEFAULT NULL,
  `service_end_month` int(11) DEFAULT NULL,
  `military_branch` varchar(300) DEFAULT NULL,
  `military_rank` varchar(300) DEFAULT NULL,
  `military_job` varchar(300) DEFAULT NULL,
  `military_zone` varchar(300) DEFAULT NULL,
  `military_health` varchar(300) DEFAULT NULL,
  `military_updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Dumping data for table `user_military`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_password`
--

CREATE TABLE `user_password` (
  `id` int(11) NOT NULL,
  `userId` varchar(50) NOT NULL,
  `password` varchar(150) DEFAULT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_password`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hospitalization_reasons`
--
ALTER TABLE `hospitalization_reasons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_hospitalization`
--
ALTER TABLE `user_hospitalization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userId` (`userId`);

--
-- Indexes for table `user_interests`
--
ALTER TABLE `user_interests`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userId` (`userId`);

--
-- Indexes for table `user_military`
--
ALTER TABLE `user_military`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userId` (`userId`);

--
-- Indexes for table `user_password`
--
ALTER TABLE `user_password`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userId` (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hospitalization_reasons`
--
ALTER TABLE `hospitalization_reasons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `user_hospitalization`
--
ALTER TABLE `user_hospitalization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `user_interests`
--
ALTER TABLE `user_interests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `user_military`
--
ALTER TABLE `user_military`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `user_password`
--
ALTER TABLE `user_password`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;