
(function ($) {
    "use strict";
    $('#submitButton').on('click', function() {
        $('#errorMessage').text('');
        var email = $("#email").val();
        $("#submitButton").text('Please wait...');
        $("#submitButton").addClass('disabled');
        $("#submitButton").prop('disabled', true);
        $.ajax({
            type: "POST",
            url: BASE_URL+'contact/submit_support',
            dataType: "json",
            data: "email=" + email,
            success : function(text) {
                if (text.success) {
                    $("#submitButton").text('Submit');
                    $('#errorMessage').text(text.message);
                    $("#email").val('');
                } else {
                    $("#submitButton").text('Submit');
                    $('#errorMessage').text(text.message);
                }
                $("#submitButton").removeClass('disabled');
                $("#submitButton").prop('disabled', false);
            }
        });
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            // if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
            //     return false;
            // }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }

    
    

})(jQuery);