<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="<?php echo base_url('assets/img/favicon.png')?>" type="image/png">
	<title>SameCondition | Home</title>
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/bootstrap.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/themify-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/flaticon.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendors/fontawesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendors/owl-carousel/owl.carousel.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendors/animate-css/animate.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/style.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/responsive.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/styling.css')?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/jquery.toast.min.css')?>">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom/style.css')?>">
	<script type="text/javascript">
		var BASE_URL = "<?php echo base_url()?>";
	</script>
</head>
<body>
	<header class="header_area">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class="container">
					<a class="navbar-brand logo_h" href="<?php echo base_url('index')?>"><img src="<?php echo base_url('assets/img/logo.png')?>" alt="Logo"></a>
				</div>
			</nav>
		</div>
	</header>