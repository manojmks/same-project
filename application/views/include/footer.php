<footer class="footer-area area-padding-top">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-sm-6 single-footer-widget">
				<h4>General</h4>
				<ul>
					<li><a href="<?php echo base_url('#')?>" target="_blank">How it works</a></li>
					<li><a href="<?php echo base_url('about')?>" target="_blank">About us</a></li>
					<li><a href="<?php echo base_url('contact')?>" target="_blank">Contact us</a></li>
					<li><a href="<?php echo base_url('blog')?>" target="_blank">Blog</a></li>
					<li><a href="<?php echo base_url('terms')?>" target="_blank">Terms and Conditions</a></li>
				</ul>
			</div>
			<div class="col-lg-4 col-sm-6 single-footer-widget">
				<h4>Community Pages</h4>
				<ul>
					<li><a href="<?php echo base_url('patients')?>">Connect</a></li>
					<li><a href="<?php echo base_url('conditions')?>">Conditions</a></li>
					<li><a href="<?php echo base_url('treatments')?>">Treatements</a></li>
					<li><a href="<?php echo base_url('symptoms')?>">Symptoms</a></li>
				</ul>
			</div>
			<div class="col-lg-4 col-sm-6 single-footer-widget">
				<h4>Contact Us</h4>
				<ul>
					<li><a href="#">1528 Monroe St,</a></li>
					<li><a href="#">Chicago, IL 60607 USA</a></li>
					<li><a href="#">mail@samecondition.com</a></li>
				</ul>
			</div>
		</div>
		<div class="row footer-bottom d-flex justify-content-between">
			<p class="col-lg-8 col-sm-12 footer-text m-0">
				Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved || This website is made with
				<i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://samecondition.com" target="_blank">Same Condition</a>
			</p>
			<div class="btn-group">
				<button class="btn btn-light btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fa fa-language" aria-hidden="true"></i> English
				</button>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="<?php echo base_url('language/change/english')?>">English</a>
					<a class="dropdown-item" href="<?php echo base_url('language/change/russian')?>">Russian</a>
					<a class="dropdown-item" href="<?php echo base_url('language/change/german')?>">German</a>
				</div>
			</div>
			<div class="col-lg-4 col-sm-12 footer-social">
				<a href="https://www.facebook.com/SameCondition/?ti=as"><i class="fab fa-facebook-f"></i></a>
				<a href="https://twitter.com/SameCondition?s=08"><i class="fab fa-twitter"></i></a>
				<a href="https://instagram.com/samecondition?igshid=9dx8irozkcz6"><i class="fab fa-instagram"></i></a>
			</div>
		</div>
	</div>
</footer>
<script type="text/javascript">
	var error = "<?php echo $this->session->flashdata('error') ?>";
	var success = "<?php echo $this->session->flashdata('success') ?>";
	var warning = "<?php echo $this->session->flashdata('warning') ?>";
	var info = "<?php echo $this->session->flashdata('info') ?>";
</script>
<script src="<?php echo base_url('assets/lib/jquery-2.2.4.min.js')?>"></script>
<script src="<?php echo base_url('assets/lib/popper.js')?>"></script>
<script src="<?php echo base_url('assets/lib/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/lib/stellar.js')?>"></script>
<script src="<?php echo base_url('assets/vendors/owl-carousel/owl.carousel.min.js')?>"></script>
<script src="<?php echo base_url('assets/lib/jquery.ajaxchimp.min.js')?>"></script>
<script src="<?php echo base_url('assets/lib/waypoints.min.js')?>"></script>
<script src="<?php echo base_url('assets/lib/mail-script.js')?>"></script>
<script src="<?php echo base_url('assets/lib/contact.js')?>"></script>
<script src="<?php echo base_url('assets/lib/jquery.form.js')?>"></script>
<script src="<?php echo base_url('assets/lib/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url('assets/lib/mail-script.js')?>"></script>
<script src="<?php echo base_url('assets/lib/theme.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/lib/jquery.toast.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/globals.js')?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		if (error) {
			globals.showToastMessage('Error', error, 'error', 10000);
		}
		if (success) {
			globals.showToastMessage('Success', success, 'success', 6000);
		}
		if (warning) {
			globals.showToastMessage('Warning', warning, 'warning', 6000);
		}
		if (info) {
			globals.showToastMessage('Info', info, 'info', 6000);
		}
	});
</script>
</body>
</html>