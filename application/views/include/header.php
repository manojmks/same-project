<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="<?php echo base_url('assets/img/favicon.png')?>" type="image/png">
	<meta name="author" content="Manoj Kumar">
	<title>SameCondition | Home</title>
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/bootstrap.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/themify-icons.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/flaticon.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendors/fontawesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendors/owl-carousel/owl.carousel.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendors/animate-css/animate.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/style.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/responsive.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/styling.css')?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/jquery.toast.min.css')?>">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css')?>">
	<script type="text/javascript">
		var BASE_URL = "<?php echo base_url()?>";
	</script>
</head>
<body>
	<header class="header_area">
		<div class="top_menu row m0">
			<div class="container">
				<div class="float-left">
					<a class="dn_btn" href="mailto:mail@samecondition.com"><i class="ti-email"></i>mail@samecondition.com</a>
				</div>
				<div class="float-right">
					<ul class="list header_social"> 
						<li><a href="https://www.facebook.com/SameCondition/?ti=as"><i class="ti-facebook"></i></a></li>
						<li><a href="https://instagram.com/samecondition?igshid=9dx8irozkcz6"><i class="ti-instagram"></i></a></li>
						<li><a href="https://twitter.com/SameCondition?s=08"><i class="ti-twitter-alt"></i></a></li>
					</ul>	
				</div>
			</div>	
		</div>	
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class="container">
					<a class="navbar-brand logo_h" href="<?php echo base_url('index')?>"><img src="<?php echo base_url('assets/img/logo.png')?>" alt="Logo"></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav ml-auto">
							<li class="nav-item"><a class="nav-link" href="<?php echo base_url('home')?>">Home</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo base_url('patients')?>">Patients</a></li> 
							<li class="nav-item"><a class="nav-link" href="<?php echo base_url('conditions')?>">Conditions</a></li> 
							<li class="nav-item"><a class="nav-link" href="<?php echo base_url('treatements')?>">Treatment</a></li> 
							<li class="nav-item"><a class="nav-link" href="<?php echo base_url('symptoms')?>">Symptoms</a></li> 
							<li class="nav-item"><a class="nav-link" href="<?php echo base_url('index')?>">Blog</a></li> 
							<?php if ($this->session->userdata('usr_id')) { ?>
								<li class="nav-item submenu dropdown">
									<a href="#" class="nav-link dropdown-toggle user-menu-image" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
										<img src="<?php echo base_url('assets/img/user.svg')?>">
										<?php echo $this->session->userdata('name') ?>
									</a>
									<ul class="dropdown-menu">
										<li class="nav-item">
											<a class="nav-link" href="<?php echo base_url('dashboard')?>">
												<i class="fa fa-user" aria-hidden="true"></i> Dashboard
											</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="<?php echo base_url('profile')?>">
												<i class="fa fa-user" aria-hidden="true"></i> Profile
											</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="<?php echo base_url('settings')?>">
												<i class="fa fa-cog" aria-hidden="true"></i> Settings
											</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="<?php echo base_url('login/logout')?>">
												<i class="fa fa-sign-out" aria-hidden="true"></i> Logout
											</a>
										</li>
									</ul>
								</li>
							<?php } else { ?>
								<li class="nav-item submenu dropdown">
									<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Register</a>
									<ul class="dropdown-menu">
										<li class="nav-item"><a class="nav-link" href="<?php echo base_url('login/register/patient')?>">Patient</a></li>
										<li class="nav-item"><a class="nav-link" href="<?php echo base_url('login/register/clinician')?>">Clinician</a></li>
										<li class="nav-item"><a class="nav-link" href="<?php echo base_url('login/register/researcher')?>">Researcher</a></li>
									</ul>
								</li>
								<li class="nav-item"><a class="nav-link" href="<?php echo base_url('login')?>">Login</a></li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</header>