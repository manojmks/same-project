<script type="text/javascript">
	var error = "<?php echo $this->session->flashdata('error') ?>";
	var success = "<?php echo $this->session->flashdata('success') ?>";
	var warning = "<?php echo $this->session->flashdata('warning') ?>";
	var info = "<?php echo $this->session->flashdata('info') ?>";
</script>
<script src="<?php echo base_url('assets/lib/jquery-2.2.4.min.js')?>"></script>
<script src="<?php echo base_url('assets/lib/popper.js')?>"></script>
<script src="<?php echo base_url('assets/lib/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/lib/stellar.js')?>"></script>
<script src="<?php echo base_url('assets/lib/jquery.ajaxchimp.min.js')?>"></script>
<script src="<?php echo base_url('assets/lib/waypoints.min.js')?>"></script>
<script src="<?php echo base_url('assets/vendors/owl-carousel/owl.carousel.min.js')?>"></script>
<script src="<?php echo base_url('assets/lib/mail-script.js')?>"></script>
<script src="<?php echo base_url('assets/lib/contact.js')?>"></script>
<script src="<?php echo base_url('assets/lib/jquery.form.js')?>"></script>
<script src="<?php echo base_url('assets/lib/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url('assets/lib/mail-script.js')?>"></script>
<script src="<?php echo base_url('assets/lib/theme.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/lib/jquery.toast.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/globals.js')?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		if (error) {
			globals.showToastMessage('Error', error, 'error', 10000);
		}
		if (success) {
			globals.showToastMessage('Success', success, 'success', 6000);
		}
		if (warning) {
			globals.showToastMessage('Warning', warning, 'warning', 6000);
		}
		if (info) {
			globals.showToastMessage('Info', info, 'info', 6000);
		}
	});
</script>
</body>
</html>