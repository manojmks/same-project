<style type="text/css">
.profile-card-3 {
position: relative;
float: left;
overflow: hidden;
width: 100%;
text-align: center;
height:368px;
border:none;
}
.profile-card-3 .background-block {
float: left;
width: 100%;
height: 200px;
overflow: hidden;
}
.profile-card-3 .card-content {
width: 100%;
padding: 15px 25px;
color:#232323;
float:left;
background:#efefef;
height:50%;
border-radius:0 0 5px 5px;
position: relative;
z-index: 9999;
}
.profile-card-3 .card-content::before {
content: '';
background: #efefef;
width: 120%;
height: 100%;
left: 11px;
bottom: 51px;
position: absolute;
z-index: -1;
transform: rotate(-13deg);
}
.profile-card-3 .profile {
border-radius: 50%;
position: absolute;
bottom: 50%;
left: 50%;
opacity: 1;
box-shadow: 3px 3px 20px rgba(0, 0, 0, 0.5);
border: 2px solid rgba(255, 255, 255, 1);
-webkit-transform: translate(-50%, 0%);
transform: translate(-50%, 0%);
z-index:99999;
}
#more {display: none;}
#more_about {display: none;}
</style>
<section class="area-padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="card profile-card-3">
          <div class="background-block">
            <img src="https://www.ppt-backgrounds.net/thumbs/light-blue-art-ppt-backgrounds.jpg" width="1000px" />
          </div>
          <div class="profile-thumb-block">
            <img src="https://randomuser.me/api/portraits/men/41.jpg" alt="profile-image" class="profile"/>
          </div>
          <div class="card-content">
            <h2>Name</h2>
            <p>She's/he's age and lives in location, United States</p>
            <i class="fa fa-user" aria-hidden="true"></i>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis libero et arcu tempus semper. Sed sit amet augue condimentum,  </p>
          </div>
        </div>
        <div class="card">
          <div class="card-body text-center">
            <i class="fa fa-heart" aria-hidden="true"></i>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus imperdiet, nulla et dictum interdum, nisi lorem egestas vitae scel<span id="dots">...</span><span id="more">erisque enim ligula venenatis dolor. Maecenas nisl est, ultrices nec congue eget, auctor vitae massa. Fusce luctus vestibulum augue ut aliquet. Nunc sagittis dictum nisi, sed ullamcorper ipsum dignissim ac. In at libero sed nunc venenatis imperdiet sed ornare turpis. Donec vitae dui eget tellus gravida venenatis. Integer fringilla congue eros non fermentum. Sed dapibus pulvinar nibh tempor porta.</span></p>
            <button class="btn btn-primary" onclick="myFunction()" id="myBtn">View more</button>
            <br>
            <small class="text-muted"><i class="fa fa-clock" aria-hidden="true"></i> Latest activity earlier this week</small>
          </div>
        </div>
        <br>
        <div class="card text-center">
          <ul class="nav nav-tabs">
            <li class="nav-item">
              <a href="#about" class="nav-link active" data-toggle="tab">About</a>
            </li>
            <li class="nav-item">
              <a href="#activity" class="nav-link" data-toggle="tab">Activity</a>
            </li>
            <li class="nav-item">
              <a href="#contact" class="nav-link" data-toggle="tab">Contact</a>
            </li>
          </ul>
        </div>
        <div class="card-body text-center">
          <div class="tab-content">
            <div class="tab-pane fade show active" id="about">
              <h3 class="mt-2">About</h3>
              <p>Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui. Raw denim you probably haven't <span id="dots_about">...</span><span id="more_about">erisque enim ligula venenatis dolor. Maecenas nisl est, ultrices nec congue eget, auctor vitae massa. Fusce luctus vestibulum augue ut aliquet. Nunc sagittis dictum nisi, sed ullamcorper ipsum dignissim ac. In at libero sed nunc venenatis imperdiet sed ornare turpis. Donec vitae dui eget tellus gravida venenatis. Integer fringilla congue eros non fermentum. Sed dapibus pulvinar nibh tempor porta.</span></p>
              <button class="btn btn-primary" onclick="myAbout()" id="myBtn_about">View more</button>
              <hr>
            </div>
            <div class="tab-pane fade" id="activity">
              <h4 class="mt-2">Activity tab content</h4>
              <p>Vestibulum nec erat eu nulla rhoncus fringilla ut non neque. Vivamus nibh urna, ornare id gravida ut, mollis a magna. Aliquam porttitor condimentum nisi, eu viverra ipsum porta ut. Nam hendrerit bibendum turpis, sed molestie mi fermentum id. Aenean volutpat velit sem. Sed consequat ante in rutrum convallis. Nunc facilisis leo at faucibus adipiscing.</p>
            </div>
            <div class="tab-pane fade" id="contact">
              <h4 class="mt-2">Contact tab content</h4>
              <p>Donec vel placerat quam, ut euismod risus. Sed a mi suscipit, elementum sem a, hendrerit velit. Donec at erat magna. Sed dignissim orci nec eleifend egestas. Donec eget mi consequat massa vestibulum laoreet. Mauris et ultrices nulla, malesuada volutpat ante. Fusce ut orci lorem. Donec molestie libero in tempus imperdiet. Cum sociis natoque penatibus et magnis.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-body">
            <h2 class="card-title">Conditions</h2>
            <div class="card">
              <div class="card-body">
                <a href="#">xyz</a><small class="text-muted"> (Primary)</small>
                <p class="card-text">Diagnosed Oct 1992</p>
                <p class="card-text">Symptoms began Oct 1992</p>
              </div>
            </div>
            <div class="card mt-2">
              <div class="card-body">
                <a href="#">xyz</a>
                <p class="card-text">Diagnosed Oct 1992</p>
                <p class="card-text">Symptoms began Oct 1992</p>
                <br>
                <a href="#">xyz</a>
                <p class="card-text">Diagnosed Oct 1992</p>
                <p class="card-text">Symptoms began Oct 1992</p><br>
                <a href="#">xyz</a>
                <p class="card-text">Diagnosed Oct 1992</p>
                <p class="card-text">Symptoms began Oct 1992</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card">
          <div class="card-body">
            <h2 class="card-title">Quick Stats</h2>
            <p class="card-text">SameCondition member since @Date</p>
            <small class="text-muted"><i class="fa fa-clock" aria-hidden="true"></i> Latest activity earlier this week</small>
            <ul>
              <li><a href="#">#No Followers</a></li>
              <li><a href="#">#No Helpfull marks received</a></li>
              <li><a href="#">#No Forum Posts</a></li>
              <li><a href="#">#No Following</a></li>
            </ul>
          </div>
        </div>
        <div class="card mt-2">
          <div class="card-body">
            <h2 class="card-title">Interest</h2>
            <a href="#">xyz xyz xyz</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
function myFunction() {
var dots = document.getElementById("dots");
var moreText = document.getElementById("more");
var btnText = document.getElementById("myBtn");
if (dots.style.display === "none") {
dots.style.display = "inline";
btnText.innerHTML = "View more";
moreText.style.display = "none";
} else {
dots.style.display = "none";
btnText.innerHTML = "View less";
moreText.style.display = "inline";
}
}
function myAbout() {
var dots_about = document.getElementById("dots_about");
var moreText_about = document.getElementById("more_about");
var btnText_about = document.getElementById("myBtn_about");
if (dots_about.style.display === "none") {
dots_about.style.display = "inline";
btnText_about.innerHTML = "View more";
moreText_about.style.display = "none";
} else {
dots_about.style.display = "none";
btnText_about.innerHTML = "View less";
moreText_about.style.display = "inline";
}
}
</script>