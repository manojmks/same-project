<section class="page-container">
	<div class="container">
		<div class="my-app profile">
			<main>
				<section class="my-app__header">
					<div class="container">
						<div>
						</div>
						<div class="my-app__header-inner row">
							<div class="my-app__header-text media col-md-8">
								<div>
									<img width="82" height="82" transform="translate(165 100)" class="mr-3" src="<?php echo base_url('assets/img/users/no-user.svg')?>">
								</div>
								<div class="media-body">
									<h1 class="profile-title mb-0">Manoj Kumar</h1>
									<div class="mb-2 username">
										@manojkumar
									</div>
									<div class="mb-2">
										<span class="username"><i class="fa fa-heartbeat" aria-hidden="true"></i></span> Living with seasonal affective disorder since 2010
										Other conditions: Mild depression and 5 more
									</div>
									<div class="username mb-2">
										<span class="mr-3"><i class="fa fa-map-marker" aria-hidden="true"></i> Oregon, USA</span> 
										<span class="mr-3"><i class="fa fa-link" aria-hidden="true"></i> instagram.com/richalangella</span> 
										<span class="mr-3"><i class="fa fa-birthday-cake" aria-hidden="true"></i> Born March 20</span>
									</div>
									<div class="username mb-2">
										<i class="fa fa-calendar" aria-hidden="true"></i> Joined July 2009
									</div>
									<div class="mb-3">
										<span class="text-bold fs-15">127</span> <span class="username">Following</span>
										<span class="text-bold fs-15">720.3K</span> <span class="username">Followers</span>
									</div>
								</div>
							</div>
							<div class="my-app__header__buttons col-md-4">
								<button type="button" class="btn btn-primary mr-2">Follow</button>
								<button type="button" class="btn btn-outline-primary mr-2"><i class="fa fa-lock" aria-hidden="true"></i> Message</button>
								<button type="button" class="btn btn-outline-secondary">More...</button>
							</div>
						</div>
					</div>
				</section>
				<section class="my-app__body">
					<div class="container">
						<div class="row">
							<div class="col-4">
								<div class="my-card card">
									<div class="my-card__header card-header">
										<div class="my-card__header-title">
											<div class="my-text-overline text-black text-bold">About</div>
										</div>
									</div>
									<div class="my-card__body card-body">
										<div class="my-text-overline text-black">Bio</div>
										<dl class="my-list my-list--definitions my-dl">
											Living with Seasonal Affective Disorder, Attention Deficit Disorder, Mild Depression, and General Anxiety
										</dl>
									</div>
								</div>
								<div class="my-card card">
									<div class="my-card__body card-body">
										<div class="my-text-overline">Recent Activity</div>
										<ul class="timeline">
											<li>
												<p class="right-align text-primary mb-0">21 March, 2014</a>
												<p>Started taking Adderall XR</p>
											</li>
											<li>
												<p class="right-align text-primary mb-0">21 March, 2014</a>
												<p>updated his symptoms</p>
											</li>
											<li>
												<p class="right-align text-primary mb-0">21 March, 2014</a>
												<p>updated his symptoms</p>
											</li>
										</ul>
									</div>
								</div>
							</div>

							<div class="col-8">
								<!-- <div class="my-alert alert alert-info">
									<span class="my-alert__text">
										Your latest transaction may take a few minutes to show up in your activity.
									</span>
								</div>
							-->
							<div class="my-card card">
								<div class="my-card__header card-header">
									<h3 class="my-card__header-title card-title text-black text-bold">Conditions</h3>
									<a class="my-card__header-link" href="#">View all →</a>
								</div>
								<ul class="my-list list-group list-group-flush">
									<li class="my-list-item list-group-item">
										<div class="my-list-item__text">
											<h4 class="my-list-item__text-title text-primary">Seasonal affective disorder (primary)</h4>
											<p class="my-list-item__text-description">Diagnosed Oct 2014</p>
											<p class="my-list-item__text-description">Symptoms began Sep 2010</p>
										</div>
									</li>
									<li class="my-list-item list-group-item">
										<div class="my-list-item__text">
											<h4 class="my-list-item__text-title text-primary">Seasonal affective disorder (primary)</h4>
											<p class="my-list-item__text-description">Diagnosed Oct 2014</p>
											<p class="my-list-item__text-description">Symptoms began Sep 2010</p>
										</div>
									</li>
									<li class="my-list-item list-group-item">
										<div class="my-list-item__text">
											<h4 class="my-list-item__text-title text-primary">Seasonal affective disorder (primary)</h4>
											<p class="my-list-item__text-description">Diagnosed Oct 2014</p>
											<p class="my-list-item__text-description">Symptoms began Sep 2010</p>
										</div>
									</li>
								</ul>
							</div>
							<div class="my-card card">
								<div class="my-card__header card-header">
									<h3 class="my-card__header-title card-title text-black text-bold">Interests</h3>
								</div>
								<ul class="my-list list-group list-group-flush">
									<li class="my-list-item list-group-item">
										<div class="my-list-item__text">
											<p class="my-list-item__text-description text-primary">Advocacy, LGBTQ, Issues, Research</p>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</section>
		</main>
	</div>
</div>
</section>
<?php include_once(APPPATH . 'views/include/footer.php'); ?> 
<script type="text/javascript" src="<?php echo base_url('assets/js/member/index.js')?>"></script> 