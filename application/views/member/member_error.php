<section class="page-container">
	<div class="container">
		<div class="my-app profile">
			<main>
				<section class="my-app__header">
					<div class="container">
						<div>
						</div>
						<div class="my-app__header-inner row">
							<div class="my-app__header-text media col-md-8">
								<div>
									<img width="82" height="82" transform="translate(165 100)" class="mr-3" src="<?php echo base_url('assets/img/users/no-user.svg')?>">
								</div>
								<div class="media-body mb-4">
									<h1 class="profile-title mb-0">@<?php echo $member ?></h1>
									<div class="mb-2 username text-bold">
										@<?php echo $member ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="my-app__body">
					<div class="container">
						<div class="text-center mt-4 mb-4">
							<h1 class="profile-title mb-0">This account doesn’t exist</h1>
							<div class="username">Try searching for another.</div>
						</div>
					</div>
				</section>
			</main>
		</div>
	</div>
</section>
<?php include_once(APPPATH . 'views/include/footer.php'); ?> 
