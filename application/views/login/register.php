<?php
include APPPATH.'../assets/lib/social-login/google-login-api.php';
$login_url = 'https://accounts.google.com/o/oauth2/v2/auth?scope=' . urlencode('https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me') . '&redirect_uri=' . urlencode(CLIENT_REDIRECT_URL) . '&response_type=code&client_id=' . CLIENT_ID . '&access_type=online';

if ($this->session->userdata('LoginOK')) {
	//redirect(base_url('dashboard'));
}
?>
<section class="banner-area align-items-center" style="min-height: unset;">
	<br/>
	<br/>
	<div class="container">
		<div class="row">
			<div class="main card p-4">
				<h2 class="fs-18 text-center text-muted mb-4">Signup as <span style="text-transform: capitalize;"><?php echo $type ?></span></h2>
				<!-- <div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<a href="#" class="btn btn-md btn-primary btn-block disabled">Signup using Facebook</a>
						<br/>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12">
						<a href="#" class="btn btn-md btn-google btn-block disabled">Signup using Google</a>
					</div>
				</div> -->
				<!-- <div class="login-or">
					<hr class="hr-or">
					<span class="span-or">or</span>
				</div> -->
				<form role="form" class="needs-validation" novalidate>
					<div class="form-group">
						<label for="inputUsernameEmail">Name</label>
						<input type="text" class="form-control" name="name" placeholder="Your Name" required="">
						<div class="invalid-feedback">
							Please enter your Name
						</div>
					</div>
					<div class="form-group">
						<label for="inputUsernameEmail">Username</label>
						<input type="text" class="form-control" name="username" id="" placeholder="Username" required="">
						<div class="text-danger margin-unset errorMessage fs-12"></div>
						<div class="invalid-feedback">
							Please enter Username
						</div>
					</div> 
					<div class="form-group">
						<label for="inputUsernameEmail">Email</label>
						<input type="email" class="form-control" name="email" id="" placeholder="Your Email" required="">
						<div class="invalid-feedback">
							Please enter your Email
						</div>
					</div>
					<div class="form-group">
						<label for="inputUsernameEmail">Language</label>
						<select class="form-control">
							<option selected="" value="english">English</option>
							<option value="russian">Russian</option>
							<option value="spanish">Spanish</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inputUsernameEmail">Password</label>
						<input type="password" class="form-control" name="password" id="" placeholder="Password" required="">
						<div class="invalid-feedback">
							Please enter Password
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword">Confirm Password</label>
						<input type="password" class="form-control" name="cpassword" id="" placeholder="Confirm Password" required="">
						<div class="invalid-feedback">
							Please Confirm Password
						</div>
					</div>
					<button type="submit" class="btn btn btn-primary btn-block" id="Signup">
						Get Started
					</button>
					<p class="text-center fs-15 pt-2 margin-unset">Already have an account? <a href="<?php echo base_url('login')?>">Login</a></p>
				</form>
			</div>
		</div>
	</div>
	<br/>
	<br/>
</section>
<script type="text/javascript">
	var TYPE = "<?php echo $type ?>";
</script>
<?php include_once( APPPATH . 'views/include/footer.php' ); ?>
<script type="text/javascript" src="<?php echo base_url('assets/js/login/register.js')?>"></script>