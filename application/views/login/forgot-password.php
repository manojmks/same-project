<section class="banner-area align-items-center" style="min-height: unset;">
	<br/>
	<br/>
	<div class="container">
		<div class="row">
			<div class="main card p-4">
				<form role="form">
					<h3 class="text-black">Forgot Password</h3>
					<p class=" margin-unset">Please enter your registered email to reset your password.</p><br/>
					<div class="form-group">
						<label for="inputUsernameEmail">Email</label>
						<input type="email" class="form-control" name="email" placeholder="Your Email">
						<div class="text-danger margin-unset errorMessage fs-13"></div>
					</div>
					<button type="button" class="btn btn btn-primary btn-block" id="forgotEmailButton">
						Submit
					</button>
					<div class="alert alert-success text-center mt-4" style="margin: auto;display: none;" role="alert">
						<h4 class="alert-heading">Email Sent</h4>
						<p class="margin-unset">Password Reset email has been sent successfully.</p>
					</div>
					<br/>
					<p class="text-center fs-15 pt-2 margin-unset">Back to Login <a href="<?php echo base_url('login')?>">Login</a></p>
				</form>
			</div>
		</div>
	</div>
	<br/>
	<br/>
</section>
<?php include_once( APPPATH . 'views/include/footer.php' ); ?>
<script type="text/javascript" src="<?php echo base_url('assets/js/login/forgot-password.js')?>"></script>