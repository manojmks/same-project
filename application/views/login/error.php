<section class="banner-area align-items-center" style="min-height: unset;background-image: unset;">
	<br/>
	<br/>
	<div class="container">
		<div class="row text-center">
			<div class="alert alert-danger" style="margin: auto;" role="alert">
				<h4 class="alert-heading">Error Occured</h4>
				<p class="margin-unset">The link you are trying to access is invalid or expired.</p>
				<hr>
				<p class="mb-0"><a href="<?php echo base_url('login')?>" class="btn btn-primary btn-sm">Back to Login</a></p>
			</div>
		</div>
	</div>
	<br/>
	<br/>
</section>
<?php include_once(APPPATH . 'views/include/footer.php'); ?>