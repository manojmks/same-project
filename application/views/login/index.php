<?php
include APPPATH.'../assets/lib/social-login/google-login-api.php';
$login_url = 'https://accounts.google.com/o/oauth2/v2/auth?scope=' . urlencode('https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me') . '&redirect_uri=' . urlencode(CLIENT_REDIRECT_URL) . '&response_type=code&client_id=' . CLIENT_ID . '&access_type=online';

if ($this->session->userdata('LoginOK')) {
	//redirect(base_url('dashboard'));
}
?>
<section class="banner-area align-items-center" style="min-height: unset;">
	<br/>
	<br/>
	<div class="container">
		<div class="row">
			<div class="main card p-4">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<a href="#" class="btn btn-md btn-primary btn-block disabled">Login using Facebook</a>
						<br/>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12">
						<a href="#" class="btn btn-md btn-google btn-block disabled">Login using Google</a>
					</div>
				</div>
				<div class="login-or">
					<hr class="hr-or">
					<span class="span-or">or</span>
				</div>
				<form role="form" class="needs-validation" novalidate>
					<div class="form-group">
						<label for="inputUsernameEmail">Email</label>
						<input type="name" class="form-control" name="email" id="" placeholder="Your Email"required >
						<div class="invalid-feedback">
							Please enter your Email
						</div>
					</div>
					<div class="form-group">
						<label for="inputUsernameEmail">Password</label>
						<input type="password" class="form-control" name="password" id="" placeholder="Password" required>
						<div class="invalid-feedback">
							Please enter your login Password
						</div>
					</div>
					<div class="pull-right full-width">
						<a class="pull-right  fs-15" href="<?php echo base_url('login/forgotpassword')?>">Forgot password?</a>
					</div>
					<button type="submit" class="btn btn btn-primary btn-block" id="Signup">
						Login<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
					</button>
					<p class="text-center fs-15 pt-2 margin-unset"><a href="<?php echo base_url('login/register')?>">Create an Account</a></p>
				</form>
			</div>
		</div>
	</div>
	<br/>
	<br/>
</section>
<?php include_once( APPPATH . 'views/include/footer.php' ); ?>
<script type="text/javascript" src="<?php echo base_url('assets/js/login/login.js')?>"></script>