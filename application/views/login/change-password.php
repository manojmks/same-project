<section class="banner-area align-items-center" style="min-height: unset;background-image: unset;">
	<br/>
	<br/>
	<div class="container">
		<div class="row">
			<div class="main card p-4">
				<form role="form" class="password_reset">
					<h3 class="text-black">Change your password</h3>
					<br/>
					<div class="form-group">
						<label for="inputUsernameEmail">New Password</label>
						<input type="password" class="form-control" name="password" id="" placeholder="New Password">
					</div>
					<div class="form-group">
						<label for="inputUsernameEmail">Confirm Password</label>
						<input type="password" class="form-control" name="cpassword" id="" placeholder="Confirm Password">
						<div class="text-danger margin-unset errorMessage fs-13"></div>
					</div>
					<button type="button" class="btn btn btn-primary btn-block" id="resetPassword">
						Submit
					</button>
				</form>
				<div class="alert alert-success text-center mt-4" style="margin: auto;display: none;" role="alert">
					<p class="margin-unset">Your password has been changed successfully.</p>
					<br/>
					<p class="mb-0"><a href="<?php echo base_url('login')?>" class="btn btn-primary btn-sm">Back to Login</a></p>
				</div>
			</div>
		</div>
	</div>
	<br/>
	<br/>
</section>
<script type="text/javascript">
	var Token = '<?php echo $token; ?>'; 
</script>
<?php include_once( APPPATH . 'views/include/footer.php' ); ?>
<script type="text/javascript" src="<?php echo base_url('assets/js/login/password_reset.js')?>"></script>