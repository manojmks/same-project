<div style="width: 100%; margin-left: auto; margin-right: auto; background: #fbfbfb;    padding-bottom: 2%;padding-top: 2%;">
	<div style="border-bottom: 1px solid lightgray; margin-bottom: 5%;text-align: center;">
		<a  title="SameCondition" href="#">
			<img src="#" style="max-height: 50px;">
		</a>
	</div>
	<div style="padding-left: 4%;">
		<h3>Hi <?php echo $name ?>,</h3>
		<p>Your password has been changed.</p>
		<br>
		<p style="opacity: 0.5;padding-bottom: 3%; border-bottom: 1px solid lightgray;">If you didn't make this request, please contact us by replying to this email.
		</p>
		<br>
		<p>Thanks, <br>
			<strong>SameCondition Team</strong> <br>
			<a href="#">samecondition.com</a>
		</p>
		<p style="text-align: center;">
			<a title="Expert's Board" href="https://vedak.com" style="font-size: 20px;text-decoration: unset;color: #2a35b7"><strong>SameCondition</strong></a> <br>
			<a href="#">Terms </a>&nbsp;&nbsp; |&nbsp;&nbsp; 
			<a href="#"> Privacy Policy</a>
		</p>
	</div>
</div>