<section>
	<div class="container">
		<h1 class="">Height & weight</h1>
		<div class="card mt-5">
			<div class="card-body">
				<h4 class="card-title">Profile visibility setting</h4>
				<div class="form-group">
					<div class="custom-control custom-radio">
						<input type="radio" class="custom-control-input" id="private" name="visibility">
						<label class="custom-control-label" for="private">Me and researchers</label>
					</div>
					<div class="custom-control custom-radio">
						<input type="radio" class="custom-control-input" id="public" name="visibility" checked="checked">
						<label class="custom-control-label" for="public">Me, researchers, and others on PatientsLikeMe</label>
					</div>
				</div>
				<div class="form-group">
					<p>Preferred height units</p>
					<div class="btn-group btn-group-toggle" data-toggle="buttons">
						<label class="btn btn-sm btn-secondary">
						<input type="radio" name="height" value="feet/inches" autocomplet>Feet/Inches</label>
						<label class="btn btn-sm btn-secondary active">
							<input type="radio" name="height" value="centimeters" autocomplete checked="checked">Centimeters(cm)
						</label>
					</div>
				</div>
				<div class="form-group">
					<p>Preferred weight units</p>
					<div class="btn-group btn-group-toggle" data-toggle="buttons">
						<label class="btn btn-sm btn-secondary">
						<input type="radio" name="weight" value="pounds" autocomplet>Pounds(lb)</label>
						<label class="btn btn-sm btn-secondary active">
							<input type="radio" name="weight" value="kilogram" autocomplete checked="checked">Kilogram(kg)
						</label>
					</div>
				</div>
				<br>
				<button type="submit" name="save" class="btn btn-primary">Save</button>
				<button type="submit" name="cancel" class="btn btn-light">Cancel</button>
			</div>
		</div>
	</div>
</section>