<section class="banner-area d-flex align-items-center">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-lg-6 col-xl-5">
        <h1>Making Health<br>
        Care Better Together</h1>
        <p>Creating Connections for Better Health,
          Same Condition is a free global health community.
          It is a patient- to- patient network.
          <br>
          Healing and hope go together at Same Condition
          <br>
        Stop Waiting: Start Networking</p>
      </div>
    </div>
  </div>
</section>
<section class="contact-section area-padding">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="contact-title">Effective February 6, 2018</h2>
        <p>Please read the following Terms of Use carefully. SAMECONDITION is providing
          this website according to the Terms of Use that follow. Your use of this
        website and any linked website of the above indicates your acceptance of these terms and conditions of use.</p>
        <p>You acknowledge that health care information changes continually with advances in medical research, and that this website,
          which is updated periodically, may not contain the most recent, complete or useful information at the time you access the website.
          Please note that content for certain areas of the website has
          been provided by third parties and is subject to additional terms,
        conditions and privacy policies as noted and provided by those third parties, which will be found on those websites.</p>
        <br>
        <h2 class="contact-title">No medical advice</h2>
        <p>The content of this website, including text, graphics, images and other material,
          is provided for informational and educational purposes only and should not be relied upon
          as a substitute for personal consultation with a health care provider regarding your medical
          condition, diagnosis, advice or treatment; nor, is it provided in the course or within the context of a
          physician-patient relationship. Call your health care provider or emergency medical services in your area of
          residence or if you are residing in USA, call “911” immediately if you think you have a medical emergency.
          Always seek the advice of your physician or other qualified health care provider prior to starting any new treatment or
          if you have any questions regarding symptoms or a medical condition.
        Never disregard professional medical advice or delay in seeking it because of something included on the website.</p>
        <br>
        <h2 class="contact-title">Physician information</h2>
        <p>Please understand that the physicians whose information is available on this website may not be agents or
          employees of SAMECONDITION. Although SAMECONDITION has obtained the information provided on this website concerning
          physicians (including specialty, credentials and training) from sources believed to be reliable and accurate, SAMECONDITION
          does not recommend or endorse any specific tests, physicians, products, procedures or opinions, included on the website,
          nor makes no guarantee or warranty as to the accuracy, timeliness or completeness of that information.
          The use of, or submission of information to, this website does not create a physician-patient relationship between you
          and any physicians affiliated with SAMECONDITION, and does not obligate SAMECONDITION, or any affiliated or employed physicians to follow up or contact you.
        </p>
        <br>
        <h2 class="contact-title">Privacy and security</h2>
        <p>SAMECONDITION uses best efforts to comply with all applicable laws concerning privacy of online communications.
          The Internet is neither more nor less secure than other communications media, including mail, facsimile, and telephone
          services, all of which can be intercepted and otherwise compromised. As a matter of prudence, therefore, assume that all on-line communications are not secure.
        </p>
        <br>
        <h2 class="contact-title">Disclaimer/No Warranty</h2>
        <p>ALL INFORMATION IN THIS WEBSITE IS PROVIDED FREE AND “AS IS.” ACCORDINGLY, SAMECONDITION AND ITS
          CORPORATE AFFILIATES MAKE NO REPRESENTATIONS OR WARRANTIES (INCLUDING ANY EXPRESS OR IMPLIED WARRANTIES)
          OF ANY KIND, WITH RESPECT TO THE CONTENT OR OTHERWISE REGARDING THIS SITE OR ANY LINKED WEBSITE.
          TO THE FULL EXTENT PERMITTED UNDER LAW, SAMECONDITION AND ITS CORPORATE AFFILIATES DISCLAIM ALL WARRANTIES,
          EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. FURTHER,
        SAMECONDITION AND ITS AFFILIATES DO NOT WARRANT THE ACCURACY, RELIABILITY, COMPLETENESS, CURRENTNESS, TIMELINESS OR AVAILABILITY OF THIS SITE OR ITS CONTENT OR OF ANY LINKED WEBSITE.</p>
        <p>In using this website, you assume the risk of accessing and using the information provided herein.
          SAMECONDITION is not responsible, in any way, to you or others for any loss that results from the use of this website,
          use of linked web sites, use of or failure to use information contained in this website or information contained in linked websites.
          SAMECONDITION does not guarantee the information provided on this website and not responsible for any errors or omissions in the information provided.
          SAMECONDITION and any physicians affiliated with SAMECONDITION assume no responsibility and shall not be liable for any damages arising out of
          your use of the website or linked websites, including but not limited to, direct, indirect, special, consequential, compensatory damage, incidental
          damage, lost profits or data, damages to your computer resulting from viruses, loss of or damage to other property, or claims of third parties arising
          out of the use, copying or display of this website or its contents, regardless of whether SAMECONDITION or its corporate affiliates have been advised,
          knew or should have known of the possibility of such damages or claims. Your sole and exclusive remedy against SAMECONDITION and its corporate affiliates
          is to cease use of this website.
          Without limiting the foregoing, you expressly release SAMECONDITION and its affiliated physicians of any responsibility
          and/or liability pertaining to your use of this website, use of linked websites, information contained in this website or
          linked web sites, including any liability or responsibility pertaining to the use, administration, and/or nonuse of any quiz, assessment,
          questionnaire, survey, and the like, and your responses thereto and results thereof, and the use and/or disclosure of your information as provided herein.
        </p>
        <br>
        <h2 class="contact-title">Indemnification</h2>
        <p>You agree that you will indemnify SAMECONDITION against any damages, losses, liabilities,
          judgments, costs or expenses (including reasonable attorneys' fees and costs) arising out of a
          claim by a third party relating to materials you have posted or other actions taken by you on the website.
        </p>
        <br>
        <h2 class="contact-title">Intended for users 18 and older</h2>
        <p>The website is intended for use by individuals 18 years of age or older.
        Users under the age of 18 should get the assistance of a parent or guardian to use this website.</p>
        <br>
        <h2 class="contact-title">Limited use</h2>
        <p>You agree that you will not:</p>
        <ul>
          <li>Upload or transmit any communications or content of any type that may infringe or violate any rights of any party</li>
          <li>Use this website for any purpose in violation of local, state, national or international laws</li>
          <li>Use this website as a means to distribute advertising or other unsolicited material to any third party</li>
          <li>Use this website to post or transmit material that is unlawful, obscene, defamatory, threatening, harassing, abusive, slanderous, hateful or embarrassing to any other person or entity</li>
          <li>Attempt to disable, "hack" or otherwise interfere with the proper functioning of this website</li>
        </ul>
        <br>
        <h2 class="contact-title">Amendments</h2>
        <p>SAMECONDITION reserves the right to change these Terms of Use and/or the information, services,
          content of, and access to this website, at any time without prior notice. If you use this website after
          we post changes to the Terms of Use, you accept and agree to be bound by the changed terms and conditions.
          We suggest that you check these Terms of Use periodically for changes.
        </p>
        <br>
        <h2 class="contact-title">Applicable law; enforcement</h2>
        <p>These Terms of Use are governed and interpreted pursuant to the laws of the State of Illinois,
          notwithstanding any principles of conflicts of law. If you take legal action relating to these Terms
          of Use or your interactions with or relationship to SAMECONDITION you agree to file such action only in
          the state and federal courts located in the State of Illinois. If any part of these Terms of Use is found
          by a court of competent jurisdiction to be invalid, unlawful, void or unenforceable,
          that part will be deemed severable and will not affect the validity and enforceability of
          remaining provisions, which will remain in full force and effect. No waiver of any of these Terms of Use shall
          be deemed valid unless in writing signed by an authorized representative of SAMECONDITION and any such writing shall
          not be deemed a further or continuing waiver of such term or any other term or condition. Except as expressly supplemented elsewhere
          on this website, these Terms of Use constitute the entire agreement between you and SAMECONDITION with respect to your use of this website.
        </p>
      </div>
    </div>
  </div>
</section>