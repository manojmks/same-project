<section >
  <br><br>
  <div class="container">
    <div class="d-none d-sm-block mb-5 pb-4">
      <div class="row">
        <div class="col-12">
          <h2 class="contact-title">Have you been hospitalized?</h2>
          <p>Add the date and purpose of your hospitalizations to your profile so other patients can understand your whole journey.</p>
        </div>
        <div class="card col-lg-12">
          <div class="card-body">
            <h6 class="card-title">Please choose a reason(s) for your hospitalization</h6>
            <p class="card-text">Common reasons for a hospitalization include your condition(s), an especially severe symptom, or a surgical procedure.</p>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <input class="form-control" name="suggestion" id="suggestion" type="text" placeholder="Start typing to see suggestions" required>
                </div>
              </div>
            </div>
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
              <label class="btn btn-primary active">
                <input type="radio" name="options" id="option1" autocomplete="off" checked> Active
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="options" id="option2" autocomplete="off"> Radio
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="options" id="option3" autocomplete="off"> Radio
              </label>
            </div>
            <h6 class="card-title">When were you admitted?</h6>
            <p class="card-text">Enter your best estimate of the year, month and day if you can’t remember the exact date.</p>
            <div class="row">
              <div class="col-sm-4">
                <div class="form-group">
                  <select class="form-control" name="year" id="year">
                    <option>Select Date</option>
                    <?php
                    $Start_Year = date("Y")-20;
                    $End_Year = date("Y");
                    while ( $Start_Year<= $End_Year) { ?>
                      <option value="<?php echo $Start_Year?>"><?php echo $Start_Year?></option>
                      <?php
                      $Start_Year++;
                    }
                    ?>
                  </select>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <select class="form-control" name="year" id="year">
                    <option>Select Date</option>
                    <?php
                    for ($m=1; $m<=12; $m++) {
                      $month = date('F', mktime(0,0,0,$m, 1, date('Y'))); ?>
                      <option value="<?php echo $month?>"><?php echo $month?></option>"
                      <?php
                    }
                    ?>
                  </select>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <select class="form-control" name="year" id="year">
                    <option>Select Date</option>
                    <?php
                    $Start_day = 0;
                    $End_day = 30;
                    while ( $Start_day<= $End_day) {
                      $Start_day++;
                      ?>
                      <option value="<?php echo $Start_day?>"><?php echo $Start_day?></option>
                      <?php
                    }
                    ?>
                  </select>
                </div>
              </div>
            </div>
            <h6 class="card-title">Have you been discharged?</h6>
            <div class="row">
              <div class="col-sm-4">
                <div class="form-group">
                  <button type="button" name="yes" id="yes" class="btn btn-success">Yes</button>
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <button type="button" name="no" id="no" class="btn btn-danger">No</button>
                </div>
              </div>
            </div>
            <h6 class="card-title">When were you discharged?</h6>
            <p class="card-text">Enter your best estimate of the year, month and day if you can’t remember the exact date.</p>
            <div class="row">
              <div class="col-sm-4">
                <div class="form-group">
                  <select class="form-control" name="year" id="year">
                    <option>Select Date</option>
                    <?php
                    $Start_Year = date("Y")-20;
                    $End_Year = date("Y");
                    while ( $Start_Year<= $End_Year) { ?>
                      <option value="<?php echo $Start_Year?>"><?php echo $Start_Year?></option>
                      <?php
                      $Start_Year++;
                    }
                    ?>
                  </select>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <select class="form-control" name="year" id="year">
                    <option>Select Date</option>
                    <?php
                    for ($m=1; $m<=12; $m++) {
                      $month = date('F', mktime(0,0,0,$m, 1, date('Y'))); ?>
                      <option value="<?php echo $month?>"><?php echo $month?></option>"
                      <?php
                    }
                    ?>
                  </select>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <select class="form-control" name="year" id="year">
                    <option>Select Date</option>
                    <?php
                    $Start_day = 0;
                    $End_day = 31;
                    while ( $Start_day<= $End_day) {
                      $Start_day++;
                      ?>
                      <option value="<?php echo $Start_day?>"><?php echo $Start_day?></option>
                      <?php
                    }
                    ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="form-group mt-3">
                <button type="submit" name="save" id="save" class="btn btn-success">Save this Hospitalization</button>
              </div>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <div class="form-group mt-3">
                <button type="submit" name="cancel" id="cancel" class="btn btn-danger">cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>