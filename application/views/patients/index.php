<section class="contact-section area-padding">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="col-md-12">
					<h2 class="contact-title">Filter Patients by:</h2>
				</div>
				<form method="post">

					<div class="col-md-12">
						<label>Age</label>
						<div class="form-group">
							<div class="row">
								<label>from:
									<select name="from_date" class="form-control">
										<option value="any">Any</option>
										<?php
										$start_age= 5;
										$end_age = 80;
										while ($start_age <= $end_age) { ?>
											<option value="<?php echo $start_age?>"><?php echo $start_age." "."Yrs" ?></option>
											<?php
											$start_age = $start_age+5;
										}
										?>
									</select>
								</label>&nbsp;&nbsp;
								<label>to:<select name="to_date" class="form-control">
									<option value="any">Any</option>
									<?php
									$start_age= 5;
									$end_age = 80;
									while ($start_age <= $end_age) { ?>
										<option value="<?php echo $start_age?>"><?php echo $start_age." "."Yrs" ?></option>
										<?php
										$start_age = $start_age+5;
									}
									?>
								</select></label>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<label>Gender</label>
						<div class="form-group">
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input" id="male" name="gender">
								<label class="custom-control-label" for="male">Male</label>
							</div>
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input" id="female" name="gender">
								<label class="custom-control-label" for="female">Female</label>
							</div>
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input" id="other" name="gender">
								<label class="custom-control-label" for="other">Other</label>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<label>Interest</label>
						<div class="form-group">
							<div class="row">
								<select name="from_date" class="form-control">
									<option value="any">Any</option>
									<option value="">Advocacy</option>
									<option value="">Alternative Medicine</option>
									<option value="">Faith</option>
									<option value="">Fundraising</option>
									<option value="">LGBTQ issues</option>
									<option value="">Parenting</option>
									<option value="">Relationships</option>
									<option value="">Research</option>
									<option value="">Vetiran's Issue</option>
									<option value="">Working with my condition</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-12">
						<label>Distance from me</label>
						<div class="form-group">
							<div class="row">
								<p><a href="#"> Register or log in</a> to find patients near you.</p>
							</div>
						</div>
					</div>
					<div class="col-12">
						<label>Conditions</label>
						<div class="form-group">
							<div class="row">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="all" name="example1">
									<label class="custom-control-label" for="all">All</label>
								</div>
								<select class="form-control" name="condition">
									<option value="">xyz</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-12">
						<label>Treatments</label>
						<div class="form-group">
							<div class="row">
								<select class="form-control" name="treatment">
									<option value="">xyz</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-12">
						<label>Symptoms</label>
						<div class="form-group">
							<div class="row">
								<select class="form-control" name="symptoms">
									<option value="">xyz</option>
								</select>
							</div>
						</div>
					</div>
					<h4>Additional Filters</h4>
					<div class="col-12">
						<label>Username</label>
						<div class="form-group">
							<div class="row">
								<input type="text" name="username" class="form-control">
							</div>
						</div>
					</div>
					<div class="col-12">
						<label>Country</label>
						<div class="form-group">
							<div class="row">
								<select class="form-control" name="country">
									<option value="">xyz</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-12">
						<label>Ethnicity</label>
						<div class="form-group">
							<div class="row">
								<select class="form-control" name="ethnicity">
									<option value="">Hispanic or Latino</option>
									<option value="">Non Hispanic or Latino</option>
									<option value="">Any</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-12">
						<label>Race</label>
						<div class="form-group">
							<div class="row">
								<select class="form-control" name="ethnicity">
									<option value="">Any</option>
									<option value="">Black</option>
									<option value="">White or African American</option>
									<option value="">Asian</option>
									<option value="">Native Hawaiian or Islander</option>
									<option value="">American Indian or Alaskan Native</option>
									<option value="">Mixed Race</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-12">
						<label>Military Status</label>
						<div class="form-group">
							<div class="row">
								<select class="form-control" name="military">
									<option value="">Any</option>
									<option value="">Currently serving</option>
									<option value="">Previously served</option>
									<option value="">Current or previous</option>
								</select>
							</div>
						</div>
					</div>
					<div class="custom-control custom-checkbox mb-3">
						<input type="checkbox" class="custom-control-input" id="active_patients" name="active_patients">
						<label class="custom-control-label" for="active_patients">Only Active Patients</label>
					</div>
				</form>
			</div>
			<div class="col-lg-8">
				<p class="totalMembersMessage"></p>
				<p><span class="totalMembers"></span> members have decided to share their profiles only with other members of PatientsLikeMe.</p>
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label>Sort By</label>
							<select name="sort" class="form-control">
								<option value="">Last Update</option>
								<option value="">Newiest patients</option>
								<option value="">Username</option>
								<option value="">Helpfull marks</option>
								<option value="">Forum posts</option>
							</select><br>
						</div>
						<table class="table patientsList">
							<thead>
								<tr>
									<th>Who</th>
									<th>Conditions</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<p class="text-muted">Updated 3 minutes ago</p>
										<img src="assets/img/icon/i1.png" class="rounded-circle" alt="Profile image" width="80" height="70">
										<p>Name</p>
									</td>
									<td>
										<a href="#" class="text-muted">Condition1</a>
										<a href="#" class="text-muted">Condition2</a>
										<a href="#" class="text-muted">Condition3</a>
										<p class="text-primary">Show more</p>
										<p>Interest:</p>
										<p>xxx,yyy,zzz</p>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php include_once(APPPATH . 'views/include/footer-without-menu.php'); ?> 
<script type="text/javascript" src="<?php echo base_url('assets/js/patients/index.js')?>"></script> 