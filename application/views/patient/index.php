<section class="page-container">
	<div class="container">
		<div class="my-app profile">
			<main>
				<section class="my-app__header">
					<div class="container">
						<div>
						</div>
						<div class="my-app__header-inner row">
							<div class="my-app__header-text media col-md-8">
								<div>
									<img width="82" height="82" transform="translate(165 100)" class="mr-3" src="<?php echo base_url('assets/img/users/no-user.svg')?>">
								</div>
								<div class="media-body">
									<h1 class="profile-title mb-0">Manoj Kumar</h1>
									<div class="mb-2 username">
										@manojkumar
									</div>
									<div class="mb-2">
										<span class="username"><i class="fa fa-heartbeat" aria-hidden="true"></i></span> Living with seasonal affective disorder since 2010
										Other conditions: Mild depression and 5 more
									</div>
									<div class="username mb-2">
										<span class="mr-3"><i class="fa fa-map-marker" aria-hidden="true"></i> Oregon, USA</span> 
										<span class="mr-3"><i class="fa fa-link" aria-hidden="true"></i> instagram.com/richalangella</span> 
										<span class="mr-3"><i class="fa fa-birthday-cake" aria-hidden="true"></i> Born March 20</span>
									</div>
									<div class="username mb-2">
										<i class="fa fa-calendar" aria-hidden="true"></i> Joined July 2009
									</div>
									<div class="mb-3">
										<span class="text-bold fs-15">127</span> <span class="username">Following</span>
										<span class="text-bold fs-15">720.3K</span> <span class="username">Followers</span>
									</div>
								</div>
							</div>
							<div class="my-app__header__buttons col-md-4">
								<button type="button" class="btn btn-primary mr-2">Follow</button>
								<button type="button" class="btn btn-outline-primary mr-2"><i class="fa fa-lock" aria-hidden="true"></i> Message</button>
								<button type="button" class="btn btn-outline-secondary">More...</button>
							</div>
						</div>
					</div>
				</section>
				<section class="my-app__body">
					<div class="container">
						<div class="row">
							<div class="col-4">
								<div class="my-card card">
									<div class="my-card__header card-header">
										<div class="my-card__header-title">
											<div class="my-text-overline">Payment Balance</div>
											<h3 class="my-text-headline">$52,100.00</h3>
										</div>
										<a class="my-card__header-link" href="#">Details →</a>
									</div>
									<div class="my-card__body card-body">
										<div class="my-text-overline">Available Currencies</div>
										<dl class="my-list my-list--definitions my-dl">
											<dt>US Dollars</dt>
											<dd>32,220.00 USD</dd>
											<dt>British Pounds</dt>
											<dd>8,560.00 GBP</dd>
											<dt>Czech Koruna</dt>
											<dd>98,444.00 CZK</dd>
										</dl>
										<hr class="my-divider">
										<ul class="my-list my-list--simple list-inline mb-0">
											<li><a href="#">Add Money →</a></li>
											<li><a href="#">Top Up →</a></li>
											<li><a href="#">Add funds using PayBuddy →</a></li>
											<li><a href="#">Withdraw funds →</a></li>
										</ul>
									</div>
								</div>
								<div class="my-card card">
									<div class="my-card__body card-body">
										<div class="my-text-overline">Bank accounts and cards</div>
										<ul class="my-list my-list--simple list-inline mb-0">
											<li class="my-list-item">
												<span class="list__icon">
													<img src="./images/icon-bank-of-america.svg" alt>
												</span>
												<span>Bank of America x-9966</span>
											</li>
											<li class="my-list-item">
												<span class="my-list-item__graphic list__icon">
													<img src="./images/icon-mastercard.svg" alt>
												</span>
												<span class="my-list-item__text">MasterCard x-1144</span>
											</li>
										</ul>
										<hr class="my-divider">
										<ul class="my-list-inline list-inline mb-0">
											<li><a href="#">Add a Bank Account or Card →</a></li>
										</ul>
									</div>
								</div>
							</div>

							<div class="col-8">
								<div class="my-alert alert alert-info">
									<img class="my-alert__icon" src="./images/icon-alert.svg" alt>
									<span class="my-alert__text">
										Your latest transaction may take a few minutes to show up in your activity.
									</span>
								</div>

								<div class="my-card card">
									<div class="my-card__header card-header">
										<h3 class="my-card__header-title card-title">Pending</h3>
										<a class="my-card__header-link" href="#">See all →</a>
									</div>
									<ul class="my-list list-group list-group-flush">
										<li class="my-list-item list-group-item">
											<div class="my-list-item__date">
												<span class="my-list-item__date-day">28</span>
												<span class="my-list-item__date-month">jul</span>
											</div>
											<div class="my-list-item__text">
												<h4 class="my-list-item__text-title">Bank of America</h4>
												<p class="my-list-item__text-description">Withdraw to bank account</p>
											</div>
											<div class="my-list-item__fee">
												<span class="my-list-item__fee-delta">+250.00</span>
												<span class="my-list-item__fee-currency">USD</span>
											</div>
										</li>
										<li class="my-list-item list-group-item">
											<div class="my-list-item__date">
												<span class="my-list-item__date-day">28</span>
												<span class="my-list-item__date-month">jul</span>
											</div>
											<div class="my-list-item__text">
												<h4 class="my-list-item__text-title">Bank of America</h4>
												<p class="my-list-item__text-description">Withdraw to bank account</p>
											</div>
											<div class="my-list-item__fee">
												<span class="my-list-item__fee-delta">+250.00</span>
												<span class="my-list-item__fee-currency">USD</span>
											</div>
										</li>
										<li class="my-list-item list-group-item">
											<div class="my-list-item__date">
												<span class="my-list-item__date-day">28</span>
												<span class="my-list-item__date-month">jul</span>
											</div>
											<div class="my-list-item__text">
												<h4 class="my-list-item__text-title">Bank of America</h4>
												<p class="my-list-item__text-description">Withdraw to bank account</p>
											</div>
											<div class="my-list-item__fee">
												<span class="my-list-item__fee-delta">+250.00</span>
												<span class="my-list-item__fee-currency">USD</span>
											</div>
										</li>
									</ul>
								</div>
								<div class="my-card card">
									<div class="my-card__header card-header">
										<h3 class="my-card__header-title card-title">Completed</h3>
										<a class="my-card__header-link" href="#">See all →</a>
									</div>
									<ul class="my-list list-group list-group-flush">
										<li class="my-list-item list-group-item">
											<div class="my-list-item__date">
												<span class="my-list-item__date-day">28</span>
												<span class="my-list-item__date-month">jul</span>
											</div>
											<div class="my-list-item__text">
												<h4 class="my-list-item__text-title">Gumroad, Inc</h4>
												<p class="my-list-item__text-description">Withdraw to bank account</p>
											</div>
											<div class="my-list-item__fee">
												<span class="my-list-item__fee-delta">+250.00</span>
												<span class="my-list-item__fee-currency">USD</span>
											</div>
										</li>
										<li class="my-list-item list-group-item">
											<div class="my-list-item__date">
												<span class="my-list-item__date-day">28</span>
												<span class="my-list-item__date-month">jul</span>
											</div>
											<div class="my-list-item__text">
												<h4 class="my-list-item__text-title">Spotify Limited</h4>
												<p class="my-list-item__text-description">Preapproved Payment</p>
											</div>
											<div class="my-list-item__fee">
												<span class="my-list-item__fee-delta">+250.00</span>
												<span class="my-list-item__fee-currency">USD</span>
											</div>
										</li>
										<li class="my-list-item list-group-item">
											<div class="my-list-item__date">
												<span class="my-list-item__date-day">28</span>
												<span class="my-list-item__date-month">jul</span>
											</div>
											<div class="my-list-item__text">
												<h4 class="my-list-item__text-title">Bank of America</h4>
												<p class="my-list-item__text-description">Withdraw to bank account</p>
											</div>
											<div class="my-list-item__fee">
												<span class="my-list-item__fee-delta">+250.00</span>
												<span class="my-list-item__fee-currency">USD</span>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section>
			</main>
		</div>
	</div>
</section>