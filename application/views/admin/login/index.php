<!DOCTYPE html>
<html lang="en">
<head>
	<title>SameCondition | Login</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="description" content="SameCondition Admin Panel">
	<meta name="author" content="Manoj Kumar">
	<link rel="icon" href="<?php echo base_url('assets/img/favicon.png')?>" type="image/png">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendors/fontawesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/admin/lib/vendor/bootstrap/css/bootstrap.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/admin/lib/css/main.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/admin/lib/css/color_skins.css')?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/jquery.toast.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/admin/lib/vendor/chartist/css/chartist.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/admin/lib/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css')?>">
	<script type="text/javascript">
		var BASE_URL = "<?php echo base_url('admin/')?>";
	</script>
	<style type="text/css">
		.auth-main::after {
			background-image: url(<?php echo base_url('assets/img/banner/home-banner.jpg')?>)
		}
	</style>
</head>
<body class="theme-blue">
	<div class="page-loader-wrapper">
		<div class="loader">
			<div class="m-t-30"><img src="http://www.wrraptheme.com/templates/lucid/hospital/assets/images/logo-icon.svg" width="48" height="48" alt="Lucid"></div>
			<p>Please wait...</p>        
		</div>
	</div>
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle auth-main">
				<div class="auth-box">
					<div class="top">
						<img src="<?php echo base_url('assets/img/logo.png')?>" alt="SameCondition" class="bg-white mt-2">
					</div> 
					<div class="card"> 
						<div class="header">
							<p class="lead">Login to your account</p>
						</div>
						<div class="body">
							<form class="needs-validation form-auth-small" novalidate role="form">
								<div class="form-group">
									<label for="signin-email" class="control-label sr-only">Email</label>
									<input type="email" class="form-control" id="signin-email" placeholder="Email" name="email" required="">
									<div class="invalid-feedback">
										Please enter your Email
									</div>
								</div>
								<div class="form-group">
									<label for="signin-password" class="control-label sr-only">Password</label>
									<input type="password" class="form-control" id="signin-password" placeholder="Password" name="password" required="">
									<div class="invalid-feedback">
										Please enter your login Password
									</div>
								</div>
								<button type="submit" class="btn btn-primary btn-lg btn-block" id="Signup">Login</button>
								<div class="bottom">
									<span class="helper-text m-b-10"><i class="fa fa-lock"></i> 
										<a href="<?php echo base_url('admin/login/forgotpassword')?>">Forgot password?</a>
									</span>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<?php include_once( APPPATH . 'views/admin/include/footer.php' ); ?>
<script type="text/javascript" src="<?php echo base_url('assets/admin/login/index.js')?>"></script>