<!DOCTYPE html>
<html lang="en">
<head>
	<title>SameCondition | Login</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="description" content="SameCondition Admin Panel">
	<meta name="author" content="Manoj Kumar">
	<link rel="icon" href="<?php echo base_url('assets/img/favicon.png')?>" type="image/png">
	<link rel="stylesheet" href="<?php echo base_url('assets/vendors/fontawesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/admin/lib/vendor/bootstrap/css/bootstrap.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/admin/lib/css/main.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/admin/lib/css/color_skins.css')?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/lib/jquery.toast.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/admin/lib/vendor/chartist/css/chartist.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/admin/lib/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css')?>">
	<script type="text/javascript">
		var BASE_URL = "<?php echo base_url('admin/')?>";
	</script>
</head>
<body class="theme-blue">
	<div class="page-loader-wrapper">
		<div class="loader">
			<div class="m-t-30"><img src="http://www.wrraptheme.com/templates/lucid/hospital/assets/images/logo-icon.svg" width="48" height="48" alt="Lucid"></div>
			<p>Please wait...</p>
		</div>
	</div>
	<div id="wrapper">
		<nav class="navbar navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-btn">
					<button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
				</div>
				<div class="navbar-brand">
					<a href="index.html"><img src="<?php echo base_url('assets/img/logo.png')?>" alt="Logo"></a>
				</div>
				<div class="navbar-right">
					<form id="navbar-search" class="navbar-form search-form">
						<input value="" class="form-control" placeholder="Search here..." type="text">
						<button type="button" class="btn btn-default"><i class="icon-magnifier"></i></button>
					</form>
					<div id="navbar-menu">
						<ul class="nav navbar-nav">
							<li>
								<a href="#" class="icon-menu d-none d-sm-block"><i class="icon-bell"></i><span class="notification-dot"></span></a>
							</li>
							<li>
								<a href="#" class="icon-menu d-none d-sm-block"><i class="icon-login"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</nav>

		<div id="left-sidebar" class="sidebar">
			<div class="sidebar-scroll">
				<div class="user-account">
					<div class="dropdown">
						<span>Welcome,</span>
						<a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong>Dr. Chandler Bing</strong></a>
						<ul class="dropdown-menu dropdown-menu-right account">
							<li><a href="#"><i class="icon-user"></i>My Profile</a></li>
							<li><a href="javascript:void(0);"><i class="icon-settings"></i>Settings</a></li>
							<li class="divider"></li>
							<li><a href="#"><i class="icon-power"></i>Logout</a></li>
						</ul>
					</div>
				</div>
				<ul class="nav nav-tabs">
					<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menu">Menu</a></li>                
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#sub_menu"><i class="icon-book-open"></i></a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#setting"><i class="icon-settings"></i></a></li>                
				</ul>
				<div class="tab-content p-l-0 p-r-0">
					<div class="tab-pane active" id="menu">
						<nav class="sidebar-nav">
							<ul class="main-menu metismenu">
								<li class="active"><a href="#"><i class="icon-home"></i><span>Dashboard</span></a></li>
								<li class="active"><a href="#"><i class="icon-home"></i><span>Patients</span></a></li>
								<li><a href="javascript:void(0);" class="has-arrow"><i class="icon-user-follow"></i><span>Admins</span> </a>
									<ul>
										<li><a href="doctors-all.html">All Admins</a></li>
										<li><a href="doctor-add.html">Add Admin</a></li>
									</ul>
								</li>
								<li><a href="javascript:void(0);" class="has-arrow"><i class="icon-user"></i><span>Hosp. Reasons</span> </a>
									<ul>
										<li><a href="patients-all.html">All Reasons</a></li>
										<li><a href="patient-add.html">Add Reasons</a></li>
										<li><a href="patient-profile.html">Reason Requests </a></li>
									</ul>
								</li>
								<li>
									<a href="javascript:void(0);" class="has-arrow"><i class="icon-wallet"></i><span>Labs & Tests</span> </a>
									<ul>
										<li><a href="payments.html">All Labs & Tests</a></li>
										<li><a href="payments-add.html">Add Lab-Test</a></li>
										<li><a href="patient-profile.html">Lab-Test Requests </a></li>
									</ul>
								</li>
								<li>
									<a href="javascript:void(0);" class="has-arrow"><i class="icon-wallet"></i><span>Conditions</span> </a>
									<ul>
										<li><a href="payments.html">All Conditions</a></li>
										<li><a href="payments-add.html">Add Condition</a></li>
										<li><a href="patient-profile.html">Condition Requests </a></li>
									</ul>
								</li>
								<li>
									<a href="javascript:void(0);" class="has-arrow"><i class="icon-wallet"></i><span>Symptoms</span> </a>
									<ul>
										<li><a href="payments.html">All Symptoms</a></li>
										<li><a href="payments-add.html">Add Symptom</a></li>
										<li><a href="patient-profile.html">Symptom Requests </a></li>
									</ul>
								</li>
							</ul>
						</nav>
					</div>
					<div class="tab-pane" id="sub_menu">
						<nav class="sidebar-nav">
							<ul class="main-menu metismenu">
								<li>
									<a href="javascript:void(0);" class="has-arrow"><i class="icon-wallet"></i><span>FAQs</span> </a>
									<ul>
										<li><a href="payments.html">All FAQs</a></li>
										<li><a href="payments-add.html">Add FAQ</a></li>
									</ul>
								</li>
								<li>
									<a href="javascript:void(0);" class="has-arrow"><i class="icon-wallet"></i><span>Testimonials</span> </a>
									<ul>
										<li><a href="payments.html">All Testimonials</a></li>
										<li><a href="payments-add.html">Add Testimonial</a></li>
									</ul>
								</li>
							</ul>
						</nav>
					</div>
					<div class="tab-pane p-l-15 p-r-15" id="setting">
						<h6>Choose Admin Panel Skin</h6>
						<ul class="choose-skin list-unstyled">
							<li data-theme="purple">
								<div class="purple"></div>
								<span>Purple</span>
							</li>                   
							<li data-theme="blue" class="active">
								<div class="blue"></div>
								<span>Blue</span>
							</li>
							<li data-theme="cyan">
								<div class="cyan"></div>
								<span>Cyan</span>
							</li>
							<li data-theme="green">
								<div class="green"></div>
								<span>Green</span>
							</li>
							<li data-theme="orange">
								<div class="orange"></div>
								<span>Orange</span>
							</li>
							<li data-theme="blush">
								<div class="blush"></div>
								<span>Blush</span>
							</li>
						</ul>
					</div>             
				</div>          
			</div>
		</div>