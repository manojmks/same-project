<script src="<?php echo base_url('assets/lib/jquery-2.2.4.min.js')?>"></script>
<script src="<?php echo base_url('assets/lib/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/admin/lib/bundles/libscripts.bundle.js')?>"></script>
<script src="<?php echo base_url('assets/admin/lib/bundles/vendorscripts.bundle.js')?>"></script>
<script src="<?php echo base_url('assets/admin/lib/bundles/chartist.bundle.js')?>"></script>
<script src="<?php echo base_url('assets/admin/lib/bundles/knob.bundle.js')?>"></script>
<script src="<?php echo base_url('assets/admin/lib/bundles/flotscripts.bundle.js')?>"></script>
<script src="<?php echo base_url('assets/admin/lib/vendor/flot-charts/jquery.flot.selection.js')?>"></script>
<script src="<?php echo base_url('assets/admin/lib/bundles/mainscripts.bundle.js')?>"></script>
<script src="<?php echo base_url('assets/admin/lib/js/index.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/lib/jquery.toast.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/globals.js')?>"></script>