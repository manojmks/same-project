<section class="page-container">
	<div class="container">
		<div class="card" >
			<div class="card-header bg-white">
				<h2 class="card-feature__title">Get the most out of SameCondition</h2>
				<p>Complete your profile and get continue, add a condition to compare and learn about symptoms.</p>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="form-group col-md-6">
						<label for="inputUsernameEmail">Your Age</label>
						<input type="number" class="form-control" name="age" placeholder="Your Age" required="">
					</div>
					<div class="form-group col-md-6">
					</div>
					<div class="form-group col-md-6">
						<label for="inputUsernameEmail">Sex</label>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="sex" id="sex" value="male" checked>
							<label class="form-check-label" for="exampleRadios1">
								Male
							</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="sex" id="sex" value="female" checked>
							<label class="form-check-label" for="exampleRadios1">
								Female
							</label>
						</div>
					</div>
					<div class="form-group col-md-6">
					</div>
					<div class="form-group col-md-6">
						<label for="inputUsernameEmail">Country</label>
						<select name="country" class="form-control" id="countires">
							<option value="" selected="" disabled="">Select Country</option>
						</select>
					</div>
					<div class="form-group col-md-6">
					</div>
					<div class="form-group col-md-6">
						<label for="inputUsernameEmail">Military Status</label>
						<select class="form-control" name="military">
							<option value="" selected="" disabled="">Select Status</option>
							<option value="not">I have not served in the military</option>
							<option value="present">I'm currently serving</option>
							<option value="past">I previously served</option>
							<option value="not-to-answer">I prefer not to answer</option>
						</select>
					</div>
					<div class="form-group col-md-6">
					</div>
					<div class="form-group col-md-6">
						<label for="inputUsernameEmail">Ethnicity</label>
						<select class="form-control" name="ethnicity">
							<option value="" selected="" disabled="">Ethnicity</option>
							<option value="I'm Hispanic or Latino">I'm Hispanic or Latino</option>
							<option value="I'm not Hispanic or Latino">I'm not Hispanic or Latino</option>
							<option value="I prefer not to answer">I prefer not to answer</option>
						</select>
					</div>
					<div class="form-group col-md-6">
					</div>
					<div class="form-group col-md-6">
						<label for="inputUsernameEmail">Race</label>
						<select class="form-control" name="race">
							<option value="" selected="" disabled="">Select Race</option>
							<option value="White">White</option>
							<option value="Black or African American">Black or African American</option>
							<option value="Asian">Asian</option>
							<option value="Native Hawaiian or Pacific Islander">Native Hawaiian or Pacific Islander</option>
							<option value="Americam Indian or Alaska Native">Americam Indian or Alaska Native</option>
							<option value="I prefer not to answer">I prefer not to answer</option>
						</select>
					</div>
					<div class="form-group col-md-6">
					</div>
					<div class="form-group col-md-6">
						<button class="btn btn-primary continueButton" type="button" data-toggle="modal" data-target="#imageUpload">Continue 
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript" src="<?php echo base_url('assets/lib/countries.js')?>"></script>
<?php include_once( APPPATH . 'views/include/footer.php' ); ?>
<script type="text/javascript" src="<?php echo base_url('assets/js/profile/get_started.js')?>"></script> 