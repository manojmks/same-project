<section>
  <br><br>
  <div class="container">
    <div class="d-none d-sm-block mb-5 pb-4">
      <div class="row">
        <div class="col-12">
          <h2 class="title">Account</h2>
        </div>
        <div class="card col-lg-12">
          <div class="card-body">
            <h3 class="card-title"><a href="account_email_setting">My Email settings</a></h3>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <h5 class="card-title">My messages</h5>
                  <p>As a member, you will receive Private Messages from patients, your nonprofits, and SameCondition staff (including invitations to research surveys).</p>
                  <br>
                  <p>Private message notifications</p>
                  <i class="fa fa-check text-primary" aria-hidden="true"></i> Yes, email me when I receive a private message.
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <h5 class="card-title">My notifications</h5>
                  <p>When you’re on the site, you’ll receive notifications when other patients interact with your profile. This includes notifications about profile comments, member ratings, and members following you.</p>
                  <br>
                  <p>Notification emails</p>
                  <i class="fa fa-check text-primary" aria-hidden="true"></i> Yes, email me a daily digest when I receive notifications.
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <h5 class="card-title">My alerts</h5>
                  <p>When you’re on the site, you’ll see when information is added to your forum threads.</p>
                  <br>
                  <p>Forum alerts</p>
                  <i class="fa fa-check text-primary" aria-hidden="true"></i> Yes, email me when my forum threads are updated.
                  To manage your individual thread alerts, go to the “My Threads" tab on each forum.<br>
                  <i class="fa fa-check text-primary" aria-hidden="true"></i> Yes, automatically sign me up to receive email alerts for forum threads where I post a reply.
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <h5 class="card-title">Messages from SameCondition</h5>
                  <p>We occasionally send out messages directly via email from SameCondition, or our partners, with information that is relevant to you or your condition. (This information is not sent out via Private Message.)</p>
                  <br>
                  <p>Welcome messages</p>
                  <i class="fa fa-check text-primary" aria-hidden="true"></i> Yes, email me a few welcome messages.
                  </br><br>
                  <p>DigitalMe™ news and updates</p>
                  <i class="fa fa-check text-primary" aria-hidden="true"></i> Yes, email me about DigitalMe™ news and updates</br><br>
                  <p>My DigitalMe™</p>
                  <i class="fa fa-check text-primary" aria-hidden="true"></i> Yes, email me about my DigitalMe™ (scheduling, monthly check ins, and blood draw reminders)
                  <br><br>
                  <p>Monthly newsletters</p>
                  <i class="fa fa-check text-primary" aria-hidden="true"></i> Yes, email me the SameCondition monthly newsletter
                  <br><br>
                  <p>Condition newsletters</p>
                  <i class="fa fa-check text-primary" aria-hidden="true"></i> Yes, email me other SameCondition newsletters specific to me or my condition
                  <br><br>
                  <p>Research opportunities</p>
                  <i class="fa fa-check text-primary" aria-hidden="true"></i> Yes, email me new research opportunities (e.g. surveys, questionnaires or studies)
                  <br><br>
                  <p>Partner announcements</p>
                  <i class="fa fa-check text-primary" aria-hidden="true"></i> Yes, email me relevant partner information (e.g. clinical trial opportunities or announcements)
                  <br><br>
                  <p>Calendar events</p>
                  <i class="fa fa-check text-primary" aria-hidden="true"></i> Yes, email me calendar events (e.g. when it’s your birthday, New Year’s, the anniversary of when you joined the community)
                  <br><br>
                  <p>Content relevant to my condition</p>
                  <i class="fa fa-check text-primary" aria-hidden="true"></i> Yes, email me other SameCondition content that is relevant to me or my condition<br>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <h5 class="card-title">My Health reminders</h5>
                  <p>While logged into SameCondition, you will see prompts asking you to update your treatments, symptoms, and other health charts at regular intervals.</p>
                  <i class="fa fa-check text-primary" aria-hidden="true"></i> Yes, email me reminders to update my DailyMe daily<br>
                  <i class="fa fa-check text-primary" aria-hidden="true"></i> Yes, email me reminders to update my Health checklist
                </div>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <h3 class="card-title"><a href="#">Manage account</a></h3>
                  <div class = "text-right">
                    <button class="btn btn-sm btn-primary" type="button"><i class='fas fa-edit'></i></button>
                  </div>
                  <p>Create you doctor's visit sheet, deactivate, or delete your account</p>
                </div>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <h3 class="card-title"><a href="#">Name</a></h3>
                  <div class = "text-right">
                    <button class="btn btn-sm btn-primary" type="button"><i class='fas fa-edit'></i></button>
                  </div>
                  <p>You haven’t entered your name yet.</p>
                </div>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <h3 class="card-title"><a href="#">Privacy Settings</a></h3>
                  <div class = "text-right">
                    <button class="btn btn-sm btn-primary" type="button"><i class='fas fa-edit'></i></button>
                  </div>
                  <p>My profile is visible to other SameCondition community members.</p>
                </div>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <h3 class="card-title"><a href="#">Height & Weight</a></h3>
                  <div class = "text-right">
                    <button class="btn btn-sm btn-primary" type="button"><i class='fas fa-edit'></i></button>
                  </div>
                  <p>Height displayed in centimeters (cm).</p>
                </p>Weight displayed in kilograms (kg).</p>
              </div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <h3 class="card-title"><a href="#">Password and linked accounts</a></h3>
                <div class = "text-right">
                  <button class="btn btn-sm btn-primary" type="button"><i class='fas fa-edit'></i></button>
                </div>
                <p>Facebook sign-in is inactive for your account.</p><br>
                </p>You can update your password <a href="">here</a></p>
              </div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <h3 class="card-title"><a href="#">Email address</a></h3>
                <div class = "text-right">
                  <button class="btn btn-sm btn-primary" type="button"><i class='fas fa-edit'></i></button>
                </div>
                <p>xyz@gmail.com</p>
              </div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <h3 class="card-title"><a href="#">Time zone</a></h3>
                <div class = "text-right">
                  <button class="btn btn-sm btn-primary" type="button"><i class='fas fa-edit'></i></button>
                </div>
                <p>Knowing your time zone allows us to log your DailyMe status in your local time.</p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="form-group mt-3">
              <button type="submit" name="save" id="save" class="btn btn-primary">Save Changes</button>
            </div>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <div class="form-group mt-3">
              <button type="submit" name="cancel" id="cancel" class="btn btn-secondary">cancel</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>