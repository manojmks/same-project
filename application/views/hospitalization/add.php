<section class="page-container">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
				<div class="list-group">
					<a href="<?php echo base_url('dashboard')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-home fs-12" aria-hidden="true"></i> About Me
					</a>
					<a href="#" class="list-group-item list-group-item-action active">
						<i class="fa fa-ambulance fs-12" aria-hidden="true"></i> Hospitalizations
					</a>
					<a href="<?php echo base_url('labs')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-user-md fs-12" aria-hidden="true"></i> Labs & Tests
					</a>
					<a href="<?php echo base_url('charts')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-signal fs-12" aria-hidden="true"></i> My Charts
					</a>
					<a href="<?php echo base_url('conditions')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-medkit fs-12" aria-hidden="true"></i> My Conditions
					</a>
					<a href="<?php echo base_url('symptoms')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-check-square fs-12" aria-hidden="true"></i> My Symptoms
					</a>
					<a href="<?php echo base_url('treatments')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-user-md fs-12" aria-hidden="true"></i> My Treatments
					</a>
					<a href="<?php echo base_url('updates')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-user fs-12" aria-hidden="true"></i> My Updates
					</a>
					<a href="<?php echo base_url('weight')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-square fs-12" aria-hidden="true"></i> My Weight
					</a>
				</div>
			</div>
			<div class="col-md-9 col-lg-9 col-sm-9 col-xs-12">
				<section>
					<div class="container">
						<div class="d-none d-sm-block mb-5 pb-4">
							<div class="row">
								<div class="card col-lg-12">
									<div class="card-body">
										<label class="card-title">Please choose a reason(s) for your hospitalization</label>
										<p class="card-text">Common reasons for a hospitalization include your condition(s), an especially severe symptom, or a surgical procedure.</p>
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
													<select class="form-control chosen" name="hospitalization_reason_id">
													</select>
												</div>
											</div>
										</div>
										<label class="card-title text-black">When were you admitted?</label>
										<p class="card-text">Enter your best estimate of the year, month and day if you can’t remember the exact date.</p>
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group">
													<label>Year</label>
													<select class="form-control" name="admitted_year" id="year">
														<option value="" disabled="" selected="">Select Year</option>
														<?php
														$Start_Year = date("Y")-100;
														$End_Year = date("Y");
														while ( $Start_Year <= $End_Year) { ?>
															<option value="<?php echo $Start_Year?>"><?php echo $Start_Year?></option>
															<?php
															$Start_Year++;
														}
														?>
													</select>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group">
													<label>Month</label>
													<select class="form-control" name="admitted_month" id="year">
														<option value="" disabled="" selected="">Select Month</option>
														<?php
														for ($m=1; $m <=12 ; $m++) {
															$month = date('F', mktime(0,0,0,$m, 1, date('Y'))); ?>
															<option value="<?php echo $m?>"><?php echo $month?></option>"
															<?php
														}
														?>
													</select>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group">
													<label>Date</label>
													<select class="form-control" name="admitted_date" id="year">
														<option value="" disabled="" selected="">Select Date</option>
														<?php
														$Start_day = 0;
														$End_day = 30;
														while ( $Start_day<= $End_day) {
															$Start_day++;
															?>
															<option value="<?php echo $Start_day?>"><?php echo $Start_day?></option>
															<?php
														}
														?>
													</select>
												</div>
											</div>
										</div>
										<label class="card-title text-black">Have you been discharged?</label>
										<div class="row">
											<div class="col-sm-4">
												<div class="btn-group btn-group-toggle" data-toggle="buttons">
													<label class="btn btn-sm btn-secondary">
														<input type="radio" name="is_discharged" value="yes" autocomplete="off"> Yes
													</label>
													<label class="btn btn-sm btn-secondary active">
														<input type="radio" name="is_discharged" value="no" autocomplete="off" checked="checked"> No
													</label>
												</div>
											</div>
										</div>
										<div class="is_discharged none">
											<label class="card-title text-black">When were you discharged?</label>
											<p class="card-text">Enter your best estimate of the year, month and day if you can’t remember the exact date.</p>
											<div class="row">
												<div class="col-sm-4">
													<div class="form-group">
														<label>Year</label>
														<select class="form-control" name="discharged_year" id="year">
															<option value="" disabled="" selected="">Select Year</option>
															<?php
															$Start_Year = date("Y")-100;
															$End_Year = date("Y");
															while ( $Start_Year<= $End_Year) { ?>
																<option value="<?php echo $Start_Year?>"><?php echo $Start_Year?></option>
																<?php
																$Start_Year++;
															}
															?>
														</select>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="form-group">
														<label>Month</label>
														<select class="form-control" name="discharged_month" id="year">
															<option value="" disabled="" selected="">Select Month</option>
															<?php
															for ($m=1; $m<=12; $m++) {
																$month = date('F', mktime(0,0,0,$m, 1, date('Y'))); ?>
																<option value="<?php echo $m?>"><?php echo $month?></option>"
																<?php
															}
															?>
														</select>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="form-group">
														<label>Date</label>
														<select class="form-control" name="discharged_date" id="year">
															<option value="" disabled="" selected="">Select Date</option>
															<?php
															$Start_day = 0;
															$End_day = 31;
															while ( $Start_day<= $End_day) {
																$Start_day++;
																?>
																<option value="<?php echo $Start_day?>"><?php echo $Start_day?></option>
																<?php
															}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>
										<br/>
										<div class="row">
											<div class="form-group mt-3">
												<button type="button" name="save" id="save" class="btn btn-sm btn-primary addHospitalization">Save this Hospitalization</button>
											</div>
											&nbsp;&nbsp;&nbsp;&nbsp;
											<div class="form-group mt-3">
												<a href="<?php echo base_url('hospitalization')?>" type="button" name="cancel" id="cancel" class="btn btn-sm btn-secondary">cancel</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</section>
<?php include_once(APPPATH . 'views/include/footer-without-menu.php'); ?> 
<script type="text/javascript" src="<?php echo base_url('assets/js/hospitalization/add.js')?>"></script> 