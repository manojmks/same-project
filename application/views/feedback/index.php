<section >
  <br><br>
  <div class="container">
    <div class="d-none d-sm-block mb-5 pb-4">
      <div class="row">
        <div class="col-12">
          <h2 class="contact-title">DailyMe Feedback</h2>
        </div>
        <div class="card col-lg-12">
          <div class="card-body">
            <h6 class="card-title">We'd love to hear about your experience with DailyMe. We'll be in touch if we have any followup questions. Thanks!</h6>
            <div class="row">
              <div class="form-group">
                <textarea class="form-control" rows="7" cols="200" id="feedback"></textarea>
              </div>
            </div>
            <div class="row">
              <div class="form-group mt-3">
                <button type="submit" name="send" id="send" class="btn btn-success">Send</button>
              </div>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <div class="form-group mt-3">
                <button type="submit" name="cancel" id="cancel" class="btn btn-danger">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>