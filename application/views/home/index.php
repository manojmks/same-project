<section class="banner-area d-flex align-items-center">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-lg-6 col-xl-5">
				<h1>Making Health<br>
				Care Better Together</h1>
				<p>Creating Connections for Better Health,
					Same Condition is a free global health community.
					It is a patient- to- patient network.
					<br>
					Healing and hope go together at Same Condition
					<br>
				Stop Waiting: Start Networking</p>
			</div>
		</div>
	</div>
</section>

<section class="feature-section">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="card card-feature text-center text-lg-left">
					<h6 class="card-feature__title">
						<span class="card-feature__icon">
						<i class="ti-layers"></i>
					</span>Find and Connect with PATIENTS</h6>
					<p class="card-feature__subtitle">Same Condition is a free Health Network to know more about health issues from people 
					who have health conditions like yours or like your loved ones.</p>
					<br>
					<p class="card-feature__subtitle">SAt Same Condition users can interact with each other for sharing their 
						experiences and perspective about their medical condition. Same Condition is also a platform where you can have a 
						better understanding about a disease, wellness and well-being, and also about aging. Once you start networking with 
					people like you, you will have a better grip on your existing condition and your future life.</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card card-feature text-center text-lg-left">
					<h6 class="card-feature__title">
						<span class="card-feature__icon">
						<i class="ti-heart-broken"></i>
					</span>Find and Connect with RESEARCHERS</h6>
					<p class="card-feature__subtitle">Are you looking for new research and trials going on in the field of medical 
					sciences that may have hope and answers to your queries? </p>
					<br>
					<p class="card-feature__subtitle">SameCondition provides a bridge between seekers and ongoing experiments and trials. 
					Researchers are welcome to join in this journey for better solutions.</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card card-feature text-center text-lg-left">
					<h6 class="card-feature__title">
						<span class="card-feature__icon">
						<i class="ti-headphone-alt"></i>
					</span>Find and Connect with PHYSICIANS</h6>
					<p class="card-feature__subtitle">Doctors are healers. SameCondition is a global platform where Hope and Healing go together. </p>
					<br>
					<p class="card-feature__subtitle">Doctors play a profound role in channelizing the solutions that our users are seeking. Join today!</p>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="service-area area-padding-top">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-lg-4">
				<div class="card-service text-center text-lg-left mb-4 mb-lg-0">
					<h3 class="card-service__title">Track your health data</h3>
					<hr>
					<p class="card-service__subtitle">SameCondition gives you an opportunity to create your profile and record your medical information.
					By regularly updating your medical information... </p>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
						Know more
					</button>
				</div>
				<div class="modal" id="myModal">
					<div class="modal-dialog ">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">Track your Health Data</h4>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
							<div class="modal-body">
								SameCondition gives you an opportunity to create your profile and record your medical information. By regularly updating your medical information, you are creating your own medical records. As a patient you can access your digital medical records from anywhere in the world without carrying their hard copies. Moreover, you don't have to worry about losing them for good in the wake of unforeseen happening.
								And above all, it costs you nothing. It is free to use. 
								<h6> <br><br> Join Today!  Be a part of this journey of  Sharing & Tracking your health!   </h6>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-lg-4">
				<div class="card-service text-center text-lg-left mb-4 mb-lg-0">
					<h3 class="card-service__title">Compare patient statistics</h3>
					<hr>
					<p class="card-service__subtitle">At SameCondition, shared data becomes a powerful tool for you to assess, 
					compare and analyse your health and disease with people having medical conditions like you...</p>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2">
						Know more
					</button>
				</div>
				<div class="modal" id="myModal2">
					<div class="modal-dialog ">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">Compare Patient Statistics</h4>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
							<div class="modal-body">
								At SameCondition, shared data becomes a powerful tool for you to assess, compare and analyse your health and disease with people having medical conditions like you. Interact with them, explore their experiences, take community advises, explore what specialist doctors across the world have to offer and much more.
								The insight you gain here, empowers you to manage your medical condition in a better way.  Being a part of SameCondition opens you to the world of patients like you. Joining SameCondition is like saying goodbye to your lonely sufferings!
								<h6> <br><br> Join Today! Be a part of this Sharing & Compare your Health Statistics!   </h6>
							</div>
						</div>
					</div>
				</div> 
			</div>

			<div class="col-md-6 col-lg-4">
				<div class="card-service text-center text-lg-left mb-4 mb-lg-0">
					<h3 class="card-service__title">Connect & chat with others</h3>
					<hr>
					<p class="card-service__subtitle">SameCondition is a place where people with Same Condition(s) meet and share their perspective and experiences.
					As mentioned elsewhere,...</p>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal3">
						Know more
					</button>
				</div>
				<div class="modal" id="myModal3">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">Connect & Chat with others</h4>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
							<div class="modal-body">
								SameCondition is a place where people with Same Condition(s) meet and share their perspective and experiences. As mentioned elsewhere, opening up to SameCondition is goodbye to lonely sufferings and saying a hello to fellow feeling, hope and healing together!
								We firmly believe that you and other patients like you have exclusive innovative ways, with which you cope with your condition. Also, that's you and patients like you who know the best, what it is like to be in a medical condition.
								Make health connection at SameCondition. Get better insight from fellow members of this health community.
								<h6> <br><br> Join Today! Be a part of this journey of Sharing, Connecting & Hope!   </h6>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<section class="about-area">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-10 offset-md-1 col-lg-6 offset-lg-6 offset-xl-7 col-xl-5">
				<div class="about-content">
					<h4>Find helpful publications</h4>
					<p>SameCondition provides you with authentic educational health and medical content. To keep you well informed, we will keep you updated by sharing health and wellness related articles, blogs and videos. </p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="brands-area background_one">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10">
				<div class="owl-carousel brand-carousel">
					<div class="single-brand-item d-table">
						<div class="d-table-cell">
							<img src="<?php echo base_url('assets/img/brand/1.png')?>" alt="">
						</div>
					</div>
					<div class="single-brand-item d-table">
						<div class="d-table-cell">
							<img src="<?php echo base_url('assets/img/brand/2.png')?>" alt="">
						</div>
					</div>
					<div class="single-brand-item d-table">
						<div class="d-table-cell">
							<img src="<?php echo base_url('assets/img/brand/3.png')?>" alt="">
						</div>
					</div>
					<div class="single-brand-item d-table">
						<div class="d-table-cell">
							<img src="<?php echo base_url('assets/img/brand/4.png')?>" alt="">
						</div>
					</div>
					<div class="single-brand-item d-table">
						<div class="d-table-cell">
							<img src="<?php echo base_url('assets/img/brand/5.png')?>" alt="">
						</div>
					</div>
					<div class="single-brand-item d-table">
						<div class="d-table-cell">
							<img src="<?php echo base_url('assets/img/brand/3.png')?>" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>