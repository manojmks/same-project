<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<title>Healing and hope go together | Same Condition</title>
	<meta name="description" content="Creating Connections for Better Health. Healing and hope go together at Same Condition. Same Condition is a free global health community. Stop Waiting: Start Networking.">
	<meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1">
	<link rel="canonical" href="http://samecondition.com">
	<meta property="og:locale" content="en_US">
	<meta property="og:type" content="website">
	<meta name="keywords" content="samecondition, patients, similar diseases, Condition, symptoms, treatment, hospitlization">
	<meta NAME="geo.position" content="latitude; longitude">
	<meta NAME="geo.placename" content="Chicago, USA">
	<meta NAME="geo.region" content="Chicago, USA">
	<meta name="google-signin-client_id" content="531321664948-22l7977gcspqkkkf5kl19jteuqtj0eun.apps.googleusercontent.com">
	<meta property="og:title" content="Creating Connections for Better Health. Healing and hope go together at Same Condition. Same Condition is a free global health community. Stop Waiting: Start Networking.">
	<meta property="og:description" content="Creating Connections for Better Health. Healing and hope go together at Same Condition. Same Condition is a free global health community. Stop Waiting: Start Networking.">
	<meta property="og:url" content="http://samecondition.com">
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:description" content="Creating Connections for Better Health. Healing and hope go together at Same Condition. Same Condition is a free global health community. Stop Waiting: Start Networking.">
	<meta name="twitter:title" content="Creating Connections for Better Health. Healing and hope go together at Same Condition. Same Condition is a free global health community. Stop Waiting: Start Networking.">
	<script type="application/ld+json" class="yoast-schema-graph yoast-schema-graph--main">{"@context":"http://schema.org","@graph":[{"@type":"WebSite","@id":"http://samecondition.com","url":"http://samecondition.com","name":"","inLanguage":"en-US","description":"Creating Connections for Better Health. Healing and hope go together at Same Condition. Same Condition is a free global health community. Stop Waiting: Start Networking.","potentialAction":[{"@type":"SearchAction","target":"http://samecondition.com/?s={search_term_string}","query-input":"required name=search_term_string"}]},{"@type":"CollectionPage","@id":"http://samecondition.com","url":"http://samecondition.com","name":"Creating Connections for Better Health. Healing and hope go together at Same Condition. Same Condition is a free global health community. Stop Waiting: Start Networking.","isPartOf":{"@id":"http://samecondition.com"},"inLanguage":"en-US","description":"Creating Connections for Better Health. Healing and hope go together at Same Condition. Same Condition is a free global health community. Stop Waiting: Start Networking."}]}</script>
	<link rel="icon" href="http://samecondition.com/assets/img/favicon.webp" sizes="32x32">
	<link rel="icon" href="http://samecondition.com/assets/img/favicon.webp" sizes="192x192">
	<link rel="apple-touch-icon" href="http://samecondition.com/assets/img/favicon.webp">

	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/bootstrap.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/jquery-ui.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/icofont.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/animate.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/owl.carousel.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/magnific-popup.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/owl.theme.default.min.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/style.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/lib/css/responsive.css')?>">
	<script type="text/javascript">
		var BASE_URL = "<?php echo base_url()?>";
	</script>
</head>

<body data-spy="scroll" data-offset="70">
	<div class="preloader-area">
		<div class="spinner">
			<div class="rect1"></div>
			<div class="rect2"></div>
			<div class="rect3"></div>
			<div class="rect4"></div>
			<div class="rect5"></div>
		</div>
	</div>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-2 col-md-3">
					<a class="navbar-brand" href="<?php echo base_url('index')?>"><img src="<?php echo base_url('assets/img/logo.png')?>" alt="logo"></a>
				</div>
				<div class="col-12 col-lg-7 col-md-5">
					<div class="navbar-toggle-btn">
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mx-auto">
							<li class="nav-item"><a class="nav-link" href="#">Home</a></li>
							<li class="nav-item"><a class="nav-link" href="#">Patients</a></li>
							<li class="nav-item"><a class="nav-link" href="#">Conditions</a></li>
							<li class="nav-item"><a class="nav-link" href="#">Treatment</a></li>
							<li class="nav-item"><a class="nav-link" href="#">Symptoms</a></li>
							<li class="nav-item"><a class="nav-link" href="#">Blogs</a></li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-lg-3 col-md-4 text-right">
					<a href="<?php echo base_url('#')?>" class="appointment-btn">Login</a>
					<a href="<?php echo base_url('#')?>" class="btn">Join Now</a>
				</div>
			</div>
		</div>
	</nav>
	<div id="home" class="main-banner">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="row">
						<div class="col-lg-7 col-md-12">
							<div class="hero-slides">
								<div class="hero-content">
									<h1>Creating Connections for Better Health<div> Healing and hope go together at<span> Same Condition</span></div>
									<p>Same Condition is a free global health community. <br/> Stop Waiting: Start Networking</p>
									<a href="<?php echo base_url('login/register')?>" class="btn">Join Now</a>
								</div>
								<div class="hero-content">
									<h1>It is a patient- to- patient network</h1>
									<p>Stop Waiting: Start Networking</p>
									<a href="<?php echo base_url('login/register')?>" class="btn">Join Now</a>
								</div>
								<div class="hero-content">
									<h1>Healing and hope go together at<span> Same Condition</span></h1>
									<p>Stop Waiting: Start Networking</p>
									<a href="<?php echo base_url('login/register')?>" class="btn">Join Now</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<section id="contact" class="contact-area ptb-100">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6">
					<div class="contact-box">
						<h3><i class="icofont-google-map"></i> Address</h3>
						<p>1528 Monroe St, Chicago, IL 60607 USA</p>
					</div>
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="contact-box">
						<h3><i class="icofont-envelope"></i> Email</h3>
						<p><a href="#">mail@samecondition.com</a></p>
					</div>
				</div>
				<div class="col-lg-4 col-md-12">
					<div class="get-in-touch">
						<h3>Get in Touch</h3>
						<p>How can we help you?</p>
						<ul>
							<li><a href="#"><i class="icofont-facebook"></i></a></li>
							<li><a href="#"><i class="icofont-twitter"></i></a></li>
							<li><a href="#"><i class="icofont-linkedin"></i></a></li>
							<li><a href="#"><i class="icofont-instagram"></i></a></li>
						</ul>
					</div>
				</div>

				<div class="col-lg-8 col-md-12">
					<form id="contactForm">
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" name="name" id="name" required data-error="Please enter your name" placeholder="Name">
									<div class="help-block with-errors"></div>
								</div>
							</div>

							<div class="col-lg-6 col-md-6">
								<div class="form-group">
									<input type="email" class="form-control" name="email" id="email" required data-error="Please enter your email" placeholder="Email">
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-lg-12 col-md-12">
								<div class="form-group">
									<input type="text" class="form-control" name="email" id="msg_subject" data-error="Subject" placeholder="Subject">
									<div class="help-block with-errors"></div>
								</div>
							</div>

							<div class="col-lg-12 col-md-12">
								<div class="form-group">
									<textarea name="message" class="form-control" id="message" cols="30" rows="4" required data-error="Write your message" placeholder="Message"></textarea>
									<div class="help-block with-errors"></div>
								</div>
							</div>

							<div class="col-lg-12 col-md-12">
								<button id="SubmitButton" type="submit" class="btn">Send Message</button>
								<div id="msgSubmit" class="h3 text-center hidden"></div>
								<div class="clearfix"></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<footer class="footer-area bg-f9faff">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6">
					<p>Copyright @ 2020 SameCondition. All rights reserved</p>
				</div>
				<div class="col-lg-6 col-md-6">
					<ul>
						<li><a href="#">Terms & Conditions</a></li>
						<li><a href="#">Privacy Policy</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	<script src="<?php echo base_url('assets/lib/js/jquery.min.js')?>"></script>
	<script src="<?php echo base_url('assets/lib/js/jquery-ui.js')?>"></script>
	<script src="<?php echo base_url('assets/lib/js/popper.min.js')?>"></script>
	<script src="<?php echo base_url('assets/lib/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('assets/lib/js/owl.carousel.min.js')?>"></script>
	<script src="<?php echo base_url('assets/lib/js/jquery.magnific-popup.min.js')?>"></script>
	<script src="<?php echo base_url('assets/lib/js/jquery.mixitup.min.js')?>"></script>
	<script src="<?php echo base_url('assets/lib/js/waypoints.min.js')?>"></script>
	<script src="<?php echo base_url('assets/lib/js/jquery.counterup.min.js')?>"></script>
	<script src="<?php echo base_url('assets/lib/js/form-validator.min.js')?>"></script>
	<script src="<?php echo base_url('assets/lib/js/contact-form-script.js')?>"></script>
	<script src="<?php echo base_url('assets/lib/js/main.js')?>"></script>
	<script src="<?php echo base_url('assets/js/main.js')?>"></script>
</body>
</html>