<section >
	<br><br>
	<div class="container">
		<div class="d-none d-sm-block mb-5 pb-4">
			<div class="row">
				<div class="card col-lg-7">
					<div class="card-body">
						<div class="row">
							<img src="<?php echo base_url('assets/img/users/'.($profile?$profile:'no-user.svg'))?>" class="rounded-circle" alt="Profile image" width="60" height="60">&nbsp;&nbsp;
							<div class="form-group">
								<textarea class="form-control" rows="2" cols="60"  id="comment"></textarea>
							</div>
						</div>
						<div class="row pull-right">
							<div class="form-group mt-3 pull-right">
								<button type="submit" name="send" id="send" class="btn btn-primary">Post</button>
							</div>
						</div>
					</div>
				</div>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<div class="card col-lg-4">
					<div class="card-body text-black">
						<h4 class="card-title">My Treatments</h4>
						<div class="row">
							<p>Keep your treatments up to date to help others get real time reviews on what works best for you.</p>
							<hr>
							<h4>Treatment Name</h4>
							<hr>
							<button type="button" name="evaluate" id="evaluate" class="btn btn-sm btn-warning">Evaluate</button>
							<p class="text-blur">Time to evaluate---it just takes minutes!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php include_once(APPPATH . 'views/include/footer-without-menu.php'); ?> 
<script type="text/javascript" src="<?php echo base_url('assets/js/home/home.js')?>"></script> 