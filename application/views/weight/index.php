<section class="page-container">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
				<div class="list-group">
					<a href="<?php echo base_url('dashboard')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-home fs-12" aria-hidden="true"></i> About Me
					</a>
					<a href="<?php echo base_url('hospitalization')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-ambulance fs-12" aria-hidden="true"></i> Hospitalizations
					</a>
					<a href="<?php echo base_url('labs')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-user-md fs-12" aria-hidden="true"></i> Labs & Tests
					</a>
					<a href="<?php echo base_url('charts')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-signal fs-12" aria-hidden="true"></i> My Charts
					</a>
					<a href="<?php echo base_url('conditions')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-medkit fs-12" aria-hidden="true"></i> My Conditions
					</a>
					<a href="<?php echo base_url('symptoms')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-check-square fs-12" aria-hidden="true"></i> My Symptoms
					</a>
					<a href="<?php echo base_url('treatments')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-user-md fs-12" aria-hidden="true"></i> My Treatments
					</a>
					<a href="<?php echo base_url('updates')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-user fs-12" aria-hidden="true"></i> My Updates
					</a>
					<a href="<?php echo base_url('weight')?>" class="list-group-item list-group-item-action active">
						<i class="fa fa-square fs-12" aria-hidden="true"></i> My Weight
					</a>
				</div>
			</div>
			<div class="col-md-9 col-lg-9 col-sm-9 col-xs-12">
				<section>
					<div class="container" style="color:black">
						<div class="d-none d-sm-block mb-5 pb-4">
							<div class="row">
								<div class="col-md-12 padding-unset">
									<div class="card">
										<div class="card-body">
											<div class="card-title">
												<h4>My Weight</h4>
												<div class="text-right">
													<a href="#" class="btn btn-sm btn-primary" type="button">Add Weight</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</section>
<?php include_once(APPPATH . 'views/include/footer-without-menu.php'); ?> 