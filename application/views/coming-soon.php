<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<title>Healing and hope to go together | Same Condition</title>
	<meta name="description" content="Creating Connections for Better Health. Healing and hope go together at Same Condition. Same Condition is a free global health community. Stop Waiting: Start Networking.">
	<meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1">
	<link rel="canonical" href="http://samecondition.com">
	<meta property="og:locale" content="en_US">
	<meta property="og:type" content="website">
	<meta name="keywords" content="samecondition, patients, similar diseases, Condition, symptoms, treatment, hospitlization">
	<meta NAME="geo.position" content="latitude; longitude">
	<meta NAME="geo.placename" content="Chicago, USA">
	<meta NAME="geo.region" content="Chicago, USA">
	<meta name="google-signin-client_id" content="531321664948-22l7977gcspqkkkf5kl19jteuqtj0eun.apps.googleusercontent.com">
	<meta property="og:title" content="Creating Connections for Better Health. Healing and hope go together at Same Condition. Same Condition is a free global health community. Stop Waiting: Start Networking.">
	<meta property="og:description" content="Creating Connections for Better Health. Healing and hope go together at Same Condition. Same Condition is a free global health community. Stop Waiting: Start Networking.">
	<meta property="og:url" content="http://samecondition.com">
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:description" content="Creating Connections for Better Health. Healing and hope go together at Same Condition. Same Condition is a free global health community. Stop Waiting: Start Networking.">
	<meta name="twitter:title" content="Creating Connections for Better Health. Healing and hope go together at Same Condition. Same Condition is a free global health community. Stop Waiting: Start Networking.">
	<script type="application/ld+json" class="yoast-schema-graph yoast-schema-graph--main">{"@context":"http://schema.org","@graph":[{"@type":"WebSite","@id":"http://samecondition.com","url":"http://samecondition.com","name":"","inLanguage":"en-US","description":"Creating Connections for Better Health. Healing and hope go together at Same Condition. Same Condition is a free global health community. Stop Waiting: Start Networking.","potentialAction":[{"@type":"SearchAction","target":"http://samecondition.com/?s={search_term_string}","query-input":"required name=search_term_string"}]},{"@type":"CollectionPage","@id":"http://samecondition.com","url":"http://samecondition.com","name":"Creating Connections for Better Health. Healing and hope go together at Same Condition. Same Condition is a free global health community. Stop Waiting: Start Networking.","isPartOf":{"@id":"http://samecondition.com"},"inLanguage":"en-US","description":"Creating Connections for Better Health. Healing and hope go together at Same Condition. Same Condition is a free global health community. Stop Waiting: Start Networking."}]}</script>
	<link rel="icon" href="http://samecondition.com/assets/img/favicon.webp" sizes="32x32">
	<link rel="icon" href="http://samecondition.com/assets/img/favicon.webp" sizes="192x192">
	<link rel="apple-touch-icon" href="http://samecondition.com/assets/img/favicon.webp">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('vendor/bootstrap/css/bootstrap.min.css')?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('fonts/font-awesome-4.7.0/css/font-awesome.min.css')?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('vendor/animate/animate.css')?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('vendor/select2/select2.min.css')?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/util.css')?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/main.css')?>">
	<script type="text/javascript">
		var BASE_URL = "<?php echo base_url()?>";
	</script>
</head>
<body>
	<div class="bg-img1 size1 overlay1" style="background-image: url(<?php echo base_url('assets/img/banner/about1.png')?>">
		<div class="size1 p-l-15 p-r-15 p-t-30 p-b-50">
			<div class="flex-w flex-sb-m p-l-75 p-r-60 p-b-60 respon1">
				<div class="wrappic1 m-r-30 m-t-10 m-b-10">
					<a href="#"><img src="<?php echo base_url('assets/img/logo.png')?>" alt="LOGO"></a>
				</div>
			</div>
			<div class="wsize1 m-lr-auto">
				<h1 class="txt-center l1-txt1"><span class="l1-txt2">Same Condition</span> patient-to-patient network</span>
				<p class="txt-center l1-txt1" style="font-size: 25px;margin-bottom: 5%;margin-top: 5%">
					Our website is <span style="font-size: 25px" class="l1-txt2">Coming Soon</span>, follow us for update now!
				</p>
				<form class="w-full flex-w flex-c-m validate-form">
					<div class="wrap-input100 validate-input m-b-20" data-validate = "Valid email is required: ex@abc.xyz">
						<input id="email" class="input100 placeholder0 m1-txt1" required type="text" name="email" placeholder="Email Address">
						<span class="focus-input100"></span>
					</div>
					<button class="flex-c-m size3 m1-txt2 how-btn1 trans-04 m-b-20" id="submitButton" type="button">
						Subscribe
					</button>
				</form>
				<div style="text-align: left;font-size: 18px;padding-left: 18px;" id="errorMessage"></div>
				<p class="txt-center s1-txt1 p-t-5">
					And don’t worry, we hate spam too! You can unsubcribe at anytime.
				</p>
			</div>
			<div class="flex-w flex-c-m cd100 wsize1 m-lr-auto p-t-116">
				<div class="flex-col-c-m size2 bor1 m-l-10 m-r-10 m-b-15">
					<span class="l1-txt3 p-b-9 days">35</span>
					<span class="s1-txt2">Days</span>
				</div>

				<div class="flex-col-c-m size2 bor1 m-l-10 m-r-10 m-b-15">
					<span class="l1-txt3 p-b-9 hours">17</span>
					<span class="s1-txt2">Hours</span>
				</div>

				<div class="flex-col-c-m size2 bor1 m-l-10 m-r-10 m-b-15">
					<span class="l1-txt3 p-b-9 minutes">50</span>
					<span class="s1-txt2">Minutes</span>
				</div>

				<div class="flex-col-c-m size2 bor1 m-l-10 m-r-10 m-b-15">
					<span class="l1-txt3 p-b-9 seconds">39</span>
					<span class="s1-txt2">Seconds</span>
				</div>
			</div>
		</div>
	</div>
	<script src="<?php echo base_url('vendor/jquery/jquery-3.2.1.min.js')?>"></script>
	<script src="<?php echo base_url('vendor/bootstrap/js/popper.js')?>"></script>
	<script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('vendor/select2/select2.min.js')?>"></script>
	<script src="<?php echo base_url('vendor/countdowntime/moment.min.js')?>"></script>
	<script src="<?php echo base_url('vendor/countdowntime/moment-timezone.min.js')?>"></script>
	<script src="<?php echo base_url('vendor/countdowntime/moment-timezone-with-data.min.js')?>"></script>
	<script src="<?php echo base_url('vendor/countdowntime/countdowntime.js')?>"></script>
	<script>
		$('.cd100').countdown100({
			endtimeYear: 0,
			endtimeMonth: 0,
			endtimeDate: 10,
			endtimeHours: 0,
			endtimeMinutes: 0,
			endtimeSeconds: 0,
			timeZone: "" 
		});
	</script>
	<script src="<?php echo base_url('vendor/tilt/tilt.jquery.min.js')?>"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
	<script src="<?php echo base_url('js/main.js')?>"></script>
</body>
</html>