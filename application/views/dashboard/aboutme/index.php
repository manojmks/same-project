<section class="area-padding">
  <div class="container">
    <div class="card">
      <div class="card-header">
        <h2>Help us get to know you</h2>
        <p>Tell the community about yourself so they can get to know you better.</p>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-4">
            <center><img src="assets/img/icon/i1.png" class="rounded-circle" alt="Profile image" width="210" height="191">
            <h5>xyz@gmail.com</h5></center>
            <br>
            <div class="row">
              <button class="btn btn-success" type="button" name="upload_img" data-toggle="modal" data-target="#imageUpload">Upload Image</button>&nbsp;&nbsp;&nbsp;&nbsp;
              <button class="btn btn-danger" type="button" name="remove_img" data-toggle="modal" data-target="#imageRemove">Remove Image</button>
            </div>
          </div>
          <div class="col-md-6">
            <div class="row">
              <h4>Name: </h4>&nbsp;&nbsp;
              <p>xyz</p>
            </div>
            <div class="row">
              <h4>Gender:</h4>&nbsp;&nbsp;
              <p>Male/Female</p>
            </div>
            <div class="row">
              <h4>DOB:</h4>&nbsp;&nbsp;
              <p>00-00-0000</p>
            </div>
            <div class="row">
              <h4>Pronoun:</h4>&nbsp;&nbsp;
              <p>He/She</p>
            </div>
            <div class="row">
              <h4>Mobile:</h4>&nbsp;&nbsp;
              <p>00000000</p>
            </div>
            <div class="row">
              <h4>Address:</h4>&nbsp;&nbsp;
              <p>India,AP,Telangana,Hyderabad,500000</p>
            </div>
          </div>
          <div class="col-md-2">
            <button class="btn btn-info" type="button" data-toggle="modal" data-target="#EditInfo">Edit</button>
          </div>
        </div>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <h2 class="card-header">Interests</h2>
          <br>
          <div class="text-right">
            <button type="button" class="btn btn-info" name="AddInterest" data-toggle="modal" data-target="#addInterest">Add</button>&nbsp;&nbsp;
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col">
                <h4>Primary Interest</h4>
              </div>
              <div class="col">
                <p>xyz</p>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <h4>Secondary Interest</h4>
              </div>
              <div class="col">
                <p>xyz</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card">
          <h2 class="card-header">Military Status</h2>
          <br>
          <div class="text-right">
            <button type="button" class="btn btn-info" name="AddInterest" data-toggle="modal" data-target="#addMilitary">Add</button>&nbsp;&nbsp;
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col">
                <h4>Military Status</h4>
              </div>
              <div class="col">
                <p>xyz</p>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <h4>Military Branch</h4>
              </div>
              <div class="col">
                <p>xyz</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <h2 class="card-header">More Demographics</h2>
          <br>
          <div class="text-right">
            <button type="button" class="btn btn-info" name="AddInterest" data-toggle="modal" data-target="#addMoreDemo">Add</button>&nbsp;&nbsp;
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col">
                <h4>Ethnicity</h4>
              </div>
              <div class="col">
                <p>xyz</p>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <h4>Race</h4>
              </div>
              <div class="col">
                <p>xyz</p>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <h4>Highest level of Education</h4>
              </div>
              <div class="col">
                <p>xyz</p>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <h4>Type of Health Insurance</h4>
              </div>
              <div class="col">
                <p>xyz</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card">
          <h2 class="card-header">Brief Bio about yourself</h2>
          <br>
          <div class="text-right">
            <button type="button" class="btn btn-info" name="AddBio" data-toggle="modal" data-target="#addBio">Add</button>&nbsp;&nbsp;
          </div>
          <div class="card-body">
            <div class="row">
              <p>this is test bio</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h2>Share your story</h2>
            <p>Help the community get to know you. Your story will be visible on your profile page.</p>
          </div>
          <div class="card-body">
            <textarea class="form-control" name="story" cols="150" placeholder="Start typing here"></textarea>
            <br>
            <button type="button" class="btn btn-success">Add to my profile</button>
          </div>
        </div>
      </div>
    </section>
    <!-- =====================Upload Image=========================== -->
    <div class="modal fade" id="imageUpload" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="exampleModalCenterTitle">Choose a photo, then click upload</h3>
            <p>Pick a JPEG, GIF or PNG that's no bigger than 10MB.</p>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="post" enctype="multipart/form-data">
            <div class="modal-body">
              <input type="file" name="image_upload">
            </div>
          </form>
          <div class="modal-footer">
            <button type="submit" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success">Upload</button>
          </div>
        </div>
      </div>
    </div>
    <!-- ==================Remove Image================== -->
    <div class="modal fade" id="imageRemove" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="post">
            <div class="modal-body">
              <h3>Are you sure you want to remove your picture? You will not be able to undo this.</h3>
            </div>
          </form>
          <div class="modal-footer">
            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger">Remove</button>
          </div>
        </div>
      </div>
    </div>
    <!-- ==================Edit Basic Info================== -->
    <div class="modal fade" id="EditInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="exampleModalCenterTitle">Edit Basic Demographics</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="post">
            <div class="modal-body">
              <b>Name</b>
              <input type="text" class="form-control" name="name" placeholder="Enter your name">
              <b>Gender</b>
              <select class="form-control" name="gender">
                <option value="Male">Male</option>
                <option value="Female">Female</option>
                <option value="-">Prefer not to say</option>
              </select>
              <b>DOB</b>
              <input type="date" class="form-control" name="dob">
              <b>Country</b>
              <select class="form-control" name="country">
                <option value="">Please select</option>
                <option value="1">India</option>
              </select>
              <b>State/Province</b>
              <input type="text" class="form-control" name="state" placeholder="Enter State Name">
              <b>District</b>
              <input type="text" class="form-control" name="district" placeholder="Enter District Name">
              <b>City</b>
              <input type="text" class="form-control" name="city" placeholder="city">
              <b>Pincode</b>
              <input type="text" class="form-control" name="pincode" placeholder="Enter Pincode">
            </div>
          </form>
          <div class="modal-footer">
            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-info">Save</button>
          </div>
        </div>
      </div>
    </div>
    <!-- ==================Add/Edit Interest================== -->
    <div class="modal fade" id="addInterest" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="exampleModalCenterTitle">Add Interest</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="post">
            <div class="modal-body">
              <h5>Primary Interest</h5>
              <select name="primary_interest_select" class="form-control" >
                <option value="">Please select...</option>
                <option value="1">Advocacy</option>
                <option value="3">Alternative Medicine</option>
                <option value="5">Faith</option>
                <option value="2">Fundraising</option>
                <option value="10">LGBTQ Issues</option>
                <option value="7">Parenting</option>
                <option value="8">Relationships</option>
                <option value="6">Research</option>
                <option value="9">Veterans' Issues</option>
                <option value="4">Working with my Condition</option>
              </select> <br>
              <h5>Secondary Interest</h5>
              <lable><input type="checkbox" name="secondary_interest_select">&nbsp;&nbsp;Advocacy</lable><br>
              <lable><input type="checkbox" name="secondary_interest_select">&nbsp;&nbsp;Alternative Medicine</lable><br>
              <lable><input type="checkbox" name="secondary_interest_select">&nbsp;&nbsp;Faith</lable><br>
              <lable><input type="checkbox" name="secondary_interest_select">&nbsp;&nbsp;Fundraising</lable><br>
              <lable><input type="checkbox" name="secondary_interest_select">&nbsp;&nbsp;LGBTQ Issues</lable><br>
              <lable><input type="checkbox" name="secondary_interest_select">&nbsp;&nbsp;Parenting</lable><br>
              <lable><input type="checkbox" name="secondary_interest_select">&nbsp;&nbsp;Relationships</lable><br>
              <lable><input type="checkbox" name="secondary_interest_select">&nbsp;&nbsp;Research</lable><br>
              <lable><input type="checkbox" name="secondary_interest_select">&nbsp;&nbsp;Veterans' Issues</lable><br>
              <lable><input type="checkbox" name="secondary_interest_select">&nbsp;&nbsp;Working with my Condition</lable><br>
            </div>
          </form>
          <div class="modal-footer">
            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-info">Save</button>
          </div>
        </div>
      </div>
    </div>
    <!-- ==================Add/Edit Military Status================== -->
    <div class="modal fade" id="addMilitary" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="exampleModalCenterTitle">Military Status</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="post">
            <div class="modal-body">
              <h5>Military Status</h5>
              <select name="military_status_select" class="form-control" >
                <option value="">Please select...</option>
                <option value="1">I have't served in the military</option>
                <option value="3">I am currently serving</option>
                <option value="5">I previously served</option>
                <option value="2">I prefer not to answer</option>
              </select> <br>
              <h5>Military Branch</h5>
              <select name="military_branch_select" class="form-control" disabled="disabled">
                <option value="">Please select...</option>
              </select> <br>
              <h5>Military Rank</h5>
              <select name="military_rank_select" class="form-control" disabled="disabled">
                <option value="">Please select...</option>
              </select> <br>
              <h5>Military Job</h5>
              <select name="military_job_select" class="form-control" disabled="disabled">
                <option value="">Please select...</option>
              </select> <br>
            </div>
          </form>
          <div class="modal-footer">
            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-info">Save</button>
          </div>
        </div>
      </div>
    </div>
    <!-- ==================Add/Edit More Demographics================== -->
    <div class="modal fade" id="addMoreDemo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="exampleModalCenterTitle">More Demographics</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="post">
            <div class="modal-body">
              <h5>Ethnicity</h5>
              <select class="form-control" name="ethnicity">
                <option value="">Please select...</option>
                <option value="hispanic">I am Hispanic or Latino</option>
                <option value="not_hispanic">I am not Hispanic or Latino</option>
                <option value="no_answer">I prefer not to answer</option>
              </select> <br>
              <h5>Race</h5>
              <select class="form-control" name="race">
                <option value="">Please select...</option>
                <option value="white">White</option>
                <option value="black">Black or African American</option>
                <option value="asian">Asian</option>
                <option value="hawaiian">Native Hawaiian or other Pacific Islander</option>
                <option value="native_american">American Indian or Alaskan Native</option>
                <option value="mixed_race">Mixed Race</option>
                <option value="no_answer">I prefer not to answer</option>
              </select><br>
              <h5>Education</h5>
              <select class="form-control" name="education">
                <option value="">Please select...</option>
                <option value="8th_grade_or_less">8th grade or less (left school around 14)</option>
                <option value="some_high_school">Some high school, but did not graduate (left school around 16)</option>
                <option value="high_school_grad">High school graduate or GED (left school around 18)</option>
                <option value="some_college">Some college but less than a bachelor's / undergraduate degree</option>
                <option value="college">College bachelor's / undergraduate degree</option>
                <option value="post_grad">Postgraduate degree (Master's, doctorate, etc.)</option>
                <option value="no_answer">I prefer not to answer</option>
              </select> <br>
              <h5>Insurance</h5>
              <select class="form-control" name="insurance">
                <option value="">Please select...</option>
                <option value="employer">Private (through employer or union)</option>
                <option value="direct">Private (individual plan)</option>
                <option value="national">National health service</option>
                <option value="none">No Insurance</option>
                <option value="no_answer">Prefer not to answer</option>
              </select>
            </div>
          </form>
          <div class="modal-footer">
            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-info">Save</button>
          </div>
        </div>
      </div>
    </div>
    <!-- ==================Add/Edit More Demographics================== -->
    <div class="modal fade" id="addBio" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="exampleModalCenterTitle">Brief Bio about yourself</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="post">
            <div class="modal-body">
              <textarea cols="60" rows="7" name="bio" class="form-control" placeholder="Start typing here"></textarea>
            </div>
          </form>
          <div class="modal-footer">
            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-info">Save</button>
          </div>
        </div>
      </div>
    </div>