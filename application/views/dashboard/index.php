<section class="page-container">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
				<div class="list-group">
					<a href="#" class="list-group-item list-group-item-action active">
						<i class="fa fa-home fs-12" aria-hidden="true"></i> About Me
					</a>
					<a href="<?php echo base_url('hospitalization')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-ambulance fs-12" aria-hidden="true"></i> Hospitalizations
					</a>
					<a href="<?php echo base_url('labs')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-user-md fs-12" aria-hidden="true"></i> Labs & Tests
					</a>
					<a href="<?php echo base_url('charts')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-signal fs-12" aria-hidden="true"></i> My Charts
					</a>
					<a href="<?php echo base_url('conditions')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-medkit fs-12" aria-hidden="true"></i> My Conditions
					</a>
					<a href="<?php echo base_url('symptoms')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-check-square fs-12" aria-hidden="true"></i> My Symptoms
					</a>
					<a href="<?php echo base_url('treatments')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-user-md fs-12" aria-hidden="true"></i> My Treatments
					</a>
					<a href="<?php echo base_url('updates')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-user fs-12" aria-hidden="true"></i> My Updates
					</a>
					<a href="<?php echo base_url('weight')?>" class="list-group-item list-group-item-action">
						<i class="fa fa-square fs-12" aria-hidden="true"></i> My Weight
					</a>
				</div>
			</div>
			<div class="col-md-9 col-lg-9 col-sm-9 col-xs-12">
				<section>
					<div>
						<div class="card" >
							<div class="card-header bg-white">
								<h2 class="card-feature__title">Help us get to know you</h2>
								<p >Tell the community about yourself so they can get to know you better.</p>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-6">
								<div class="card">
									<div class="card-header bg-white">
										<h2 class="card-feature__title">Profile</h2>
									</div>
									<br>
									<div class="text-right">
										<!-- <button type="button" class="btn btn-sm btn-primary" name="AddInterest" data-toggle="modal" data-target="#addInterest">Add</button>&nbsp;&nbsp; -->
									</div>
									<div class="card-body fs-13">
										<center>
											<img src="<?php echo base_url('assets/img/users/'.$profile) ?>" class="rounded-circle" alt="Profile image" width="80" height="80">
										</center>
										<br>
										<div class="row text-center">
											<button class="btn btn-sm btn-primary margin-auto" type="button" name="upload_img" data-toggle="modal" data-target="#imageUpload">Change Image</button>&nbsp;&nbsp;&nbsp;&nbsp;
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card">
									<div class="card-header bg-white">
										<h2 class="card-feature__title">Interests</h2>
									</div>
									<br>
									<div class="text-right">
										<button type="button" class="btn btn-sm btn-primary" name="AddInterest" data-toggle="modal" data-target="#addInterest">Add</button>&nbsp;&nbsp;
									</div>
									<div class="card-body fs-13">
										<div class="row">
											<div class="col">
												<label class="text-black">Primary Interest</label>
											</div>
											<div class="col">
												<p class="primary_interest"></p>
											</div>
										</div>
										<div class="row">
											<div class="col">
												<label class="text-black">Secondary Interest</label>
											</div>
											<div class="col">
												<p class="secondary_interest"></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-6">
								<div class="card ">
									<div class="card-header bg-white">
										<h2 class="card-feature__title">Military Status</h2>
									</div>
									<br>
									<div class="text-right">
										<button type="button" class="btn btn-sm btn-primary" name="AddInterest" data-toggle="modal" data-target="#addMilitary">Add</button>&nbsp;&nbsp;
									</div>
									<div class="card-body fs-13">
										<div class="row">
											<div class="col">
												<label class="text-black">Military Status</label>
											</div>
											<div class="col">
												<div class="military_status"></div>
											</div>
										</div>
										<div class="row military_branch_show none">
											<div class="col">
												<label class="text-black">Military Branch</label>
											</div>
											<div class="col">
												<div class="military_branch"></div>
											</div>
										</div>
										<div class="row military_rank_show none">
											<div class="col">
												<label class="text-black">Military Rank</label>
											</div>
											<div class="col">
												<div class="military_rank"></div>
											</div>
										</div>
										<div class="row military_job_show none">
											<div class="col">
												<label class="text-black">Military Job</label>
											</div>
											<div class="col">
												<div class="military_job"></div>
											</div>
										</div>
										<div class="row military_zone_show none">
											<div class="col">
												<label class="text-black">Combat zone deployed</label>
											</div>
											<div class="col">
												<div class="military_zone"></div>
											</div>
										</div>
										<div class="row military_health_show none">
											<div class="col">
												<label class="text-black">Military Health care</label>
											</div>
											<div class="col">
												<div class="military_health"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card">
									<div class="card-header bg-white">
										<h2 class="card-feature__title">More Demographics</h2>
									</div>
									<br>
									<div class="text-right">
										<button type="button" class="btn btn-sm btn-primary" name="AddInterest" data-toggle="modal" data-target="#addMoreDemo">Edit</button>&nbsp;&nbsp;
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col">
												<label class="text-black">Ethnicity</label>
											</div>
											<div class="col">
												<p class="ethnicity"></p>
											</div>
										</div>
										<div class="row">
											<div class="col">
												<label class="text-black">Race</label>
											</div>
											<div class="col">
												<p class="race"></p>
											</div>
										</div>
										<div class="row">
											<div class="col">
												<label class="text-black">Highest level of Education</label>
											</div>
											<div class="col">
												<p class="education"></p>
											</div>
										</div>
										<div class="row">
											<div class="col">
												<label class="text-black">Type of Health Insurance</label>
											</div>
											<div class="col">
												<p class="insurance"></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="card-header bg-white">
										<h2 class="card-feature__title">Brief Bio about yourself</h2>
									</div>
									<br>
									<div class="text-right">
										<button type="button" class="btn btn-sm btn-primary" name="AddBio" data-toggle="modal" data-target="#addBio">Edit</button>&nbsp;&nbsp;
									</div>
									<div class="card-body fs-13">
										<div class="">
											<pre class="brief_bio"></pre>
										</div>
									</div>
								</div>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="card-header bg-white">
										<h2 class="card-feature__title">Share your story</h2>
										<p>Help the community get to know you. Your story will be visible on your profile page.</p>
									</div>
									<div class="card-body fs-13">
										<textarea class="form-control" name="story" cols="150" placeholder="Start typing here"></textarea>
										<br>
										<button type="button" class="btn btn-sm btn-primary">Add to my profile</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="imageUpload" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
								<div class="modal-content text-black">
									<?php echo form_open_multipart(base_url('profile/change_profile/'),array("class"=>"form-horizontal")); ?>
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalCenterTitle">Choose a photo, then click upload</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body row">
										<div class="col">
											<img src="assets/img/icon/i1.png" class="rounded-circle" alt="Profile image" width="60" height="60">
										</div>
										<div class="col">
											<label>Select Profile image to change</label>
											<input type="file" name="file" required="" accept="image/png,image/jpeg,image/gif">
											<div class="fs-11 text-blur">Maximum upload file size: 500KB.</div>
										</div>
									</div>
									<div class="modal-footer text-block">
										<button type="button" class="profileRemove btn btn-sm btn-link pull-left">Remove Profile Image</button>
										<button type="button" class="btn btn-sm btn-secondary pull-right" data-dismiss="modal">Close</button>
										<button type="submit" class="changeProfile btn btn-sm btn-primary pull-right">Upload</button>
									</div>
									<?php echo form_close(); ?>
								</div>
							</div>
						</div>
						<div class="modal fade" id="imageRemove" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<form class="post">
										<div class="modal-body">
											<h3>Are you sure you want to remove your picture? You will not be able to undo this.</h3>
										</div>
									</form>
									<div class="modal-footer">
										<button type="submit" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-sm btn-secondary">Remove</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="EditInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered modal-lg text-black" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalCenterTitle">Edit Basic Demographics</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<form class="post">
										<div class="modal-body">
											<div class="form-group">
												<label>Name</label>
												<input type="text" class="form-control" name="name" placeholder="Enter your name">
											</div>
											<div class="form-group">
												<label>Gender</label>
												<select class="form-control" name="gender">
													<option value="Male">Male</option>
													<option value="Female">Female</option>
													<option value="-">Prefer not to say</option>
												</select>
											</div>
											<div class="form-group">
												<label>DOB</label>
												<input type="date" class="form-control" name="dob">
											</div>
											<div class="form-group">
												<label>Country</label> 
												<select class="form-control" name="country" id="countires">
												</select>
											</div>
											<div class="form-group">
												<label>State/Province</label>
												<input type="text" class="form-control" name="state" placeholder="Enter State Name">
											</div>
											<div class="form-group">
												<label>District</label>
												<input type="text" class="form-control" name="district" placeholder="Enter District Name">
											</div>
											<div class="form-group">
												<label>City</label>
												<input type="text" class="form-control" name="city" placeholder="city">
											</div>
											<div class="form-group">
												<label>Pincode</label>
												<input type="text" class="form-control" name="pincode" placeholder="Enter Pincode">
											</div>
										</div>
									</form>
									<div class="modal-footer">
										<button type="submit" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-sm btn-primary">Save</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="addInterest" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
								<div class="modal-content text-black">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalCenterTitle">Add Interest</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<form class="post">
										<div class="modal-body">
											<label>Primary Interest</label>
											<select name="primary_interest" class="form-control" >
												<option value="" disabled="">Please select...</option>
												<option value="Advocacy">Advocacy</option>
												<option value="Alternative Medicine">Alternative Medicine</option>
												<option value="Faith">Faith</option>
												<option value="Fundraising">Fundraising</option>
												<option value="LGBTQ Issues">LGBTQ Issues</option>
												<option value="Parenting">Parenting</option>
												<option value="Relationships">Relationships</option>
												<option value="Research">Research</option>
												<option value="Veterans' Issues">Veterans' Issues</option>
												<option value="Working with my Condition">Working with my Condition</option>
											</select> <br>
											<label class="text-block text-bold">Secondary Interest</label>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input cursor" id="interest1">
												<label class="custom-control-label cursor" for="interest1">Advocacy</label>
											</div>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input cursor" id="interest2">
												<label class="custom-control-label cursor" for="interest2">Alternative Medicine</label>
											</div>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input cursor" id="interest3">
												<label class="custom-control-label cursor" for="interest3">Faith</label>
											</div>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input cursor" id="interest4">
												<label class="custom-control-label cursor" for="interest4">Fundraising</label>
											</div>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input cursor" id="interest5">
												<label class="custom-control-label cursor" for="interest5">LGBTQ Issues</label>
											</div>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input cursor" id="interest6">
												<label class="custom-control-label cursor" for="interest6">Parenting</label>
											</div>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input cursor" id="interest7">
												<label class="custom-control-label cursor" for="interest7">Relationships</label>
											</div>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input cursor" id="interest8">
												<label class="custom-control-label cursor" for="interest8">Research</label>
											</div>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input cursor" id="interest9">
												<label class="custom-control-label cursor" for="interest9">Veterans' Issues</label>
											</div>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input cursor" id="interest10">
												<label class="custom-control-label cursor" for="interest10">Working with my Condition</label>
											</div>
										</div>
									</form>
									<div class="modal-footer">
										<button type="submit" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-sm btn-primary saveInterests">Save</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="addMilitary" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered modal-lg text-black" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalCenterTitle">Military Status</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<form class="post">
										<div class="modal-body">
											<div class="form-group">
												<label>Military Status</label>
												<select name="military_status" class="form-control" >
													<option value="" disabled="">Please select...</option>
													<option value="1">I haven't served in the military</option>
													<option value="2">I am currently serving</option>
													<option value="3">I previously served</option>
													<option value="4">I prefer not to answer</option>
												</select>
											</div>
											<div class="form-group">
												<label>Military Branch</label>
												<select name="military_branch" class="form-control">
													<option value="">Please select...</option><option value="army">Army</option><option value="marine_corps">Marine Corps</option><option value="navy">Navy</option><option value="air_force">Air Force</option><option value="coast_guard">Coast Guard</option>
												</select>
											</div>
											<div class="form-group">
												<label>Military Rank</label>
												<select name="military_rank" class="form-control">
													<option value="">Please select...</option><option value="prefer_not_to_answer">I prefer not to answer</option><optgroup label="Enlisted"><option value="E-1 - Pvt">E-1 - Pvt</option><option value="E-2 - PFC">E-2 - PFC</option><option value="E-3 - LCpl">E-3 - LCpl</option><option value="E-4 - Cpl">E-4 - Cpl</option><option value="E-5 - Sgt">E-5 - Sgt</option><option value="E-6 - SSgt">E-6 - SSgt</option><option value="E-7 - GySgt">E-7 - GySgt</option><option value="E-8 - 1stSgt">E-8 - 1stSgt</option><option value="E-8 - MSgt">E-8 - MSgt</option><option value="E-9 - SgtMaj">E-9 - SgtMaj</option><option value="E-9 - MGySgt">E-9 - MGySgt</option></optgroup><optgroup label="Warrant officer"><option value="W-1 - WO1">W-1 - WO1</option><option value="W-2 - CWO2">W-2 - CWO2</option><option value="W-3 - CWO3">W-3 - CWO3</option><option value="W-4 - CWO4">W-4 - CWO4</option><option value="W-5 - CWO5">W-5 - CWO5</option></optgroup><optgroup label="Officer"><option value="O-1 - 2ndLt">O-1 - 2ndLt</option><option value="O-2 - 1stLt">O-2 - 1stLt</option><option value="O-3 - Capt">O-3 - Capt</option><option value="O-4 - Maj">O-4 - Maj</option><option value="O-5 - LtCol">O-5 - LtCol</option><option value="O-6 - Col">O-6 - Col</option><option value="O-7 - Bgen">O-7 - Bgen</option><option value="O-8 - MajGen">O-8 - MajGen</option><option value="O-9 - LtGen">O-9 - LtGen</option><option value="O-10 - Gen">O-10 - Gen</option></optgroup>
												</select>
											</div>
											<div class="form-group">
												<label>Military Job</label>
												<select name="military_job" class="form-control"><option value="">Please select...</option><option value="prefer_not_to_answer">I prefer not to answer</option><option value="job_not_listed">My job is not listed</option><option value="0121: Personnel Clerk">0121: Personnel Clerk</option><option value="0143: Career Retention Specialist">0143: Career Retention Specialist</option><option value="0147: Equal Opportunity Advisor (EOA)">0147: Equal Opportunity Advisor (EOA)</option><option value="0149: Substance Abuse Control Specialist">0149: Substance Abuse Control Specialist</option><option value="0151: Administrative Clerk">0151: Administrative Clerk</option><option value="0161: Construction Wireman">0161: Construction Wireman</option><option value="0161: Postal Clerk">0161: Postal Clerk</option><option value="0171: Manpower Information Systems">0171: Manpower Information Systems</option><option value="0172: Data Network Specialist">0172: Data Network Specialist</option><option value="0193: Personnel/Administrative Chief">0193: Personnel/Administrative Chief</option><option value="0211: Counterintelligence/HUMINT Specialist">0211: Counterintelligence/HUMINT Specialist</option><option value="0212: Technical Surveillance Countermeasures">0212: Technical Surveillance Countermeasures</option><option value="0231: Intelligence Specialist">0231: Intelligence Specialist</option><option value="0241: Imagery Analysis Specialist">0241: Imagery Analysis Specialist</option><option value="0261: Geographic Intelligence Specialist">0261: Geographic Intelligence Specialist</option><option value="0291: Intelligence Chief">0291: Intelligence Chief</option><option value="0311: Rifleman">0311: Rifleman</option><option value="0312: Riverine Assault Craft">0312: Riverine Assault Craft</option><option value="0313: LAV Crewman">0313: LAV Crewman</option><option value="0314: Rigid Raiding Craft">0314: Rigid Raiding Craft</option><option value="0316: Combat Rubber Reconnaissance Craft">0316: Combat Rubber Reconnaissance Craft</option><option value="0317: Scout Sniper">0317: Scout Sniper</option><option value="0321: Reconnaissance Man">0321: Reconnaissance Man</option><option value="0331: Machine Gunner">0331: Machine Gunner</option><option value="0341: Mortarman">0341: Mortarman</option><option value="0351: Infantry Assaultman">0351: Infantry Assaultman</option><option value="0352: Anti-tank Missileman">0352: Anti-tank Missileman</option><option value="0369: Infantry Unit Leader">0369: Infantry Unit Leader</option><option value="0372: Critical Skills Operator">0372: Critical Skills Operator</option><option value="0411: Maintenance Management Specialist">0411: Maintenance Management Specialist</option><option value="0431: Logistics/Embarkation Specialist">0431: Logistics/Embarkation Specialist</option><option value="0451: Airborne and Air Delivery Specialist">0451: Airborne and Air Delivery Specialist</option><option value="0471: Personnel Retrieval and Processing Specialist">0471: Personnel Retrieval and Processing Specialist</option><option value="0472: Personnel Retrieval and Processing Technician">0472: Personnel Retrieval and Processing Technician</option><option value="0481: Landing Support Specialist">0481: Landing Support Specialist</option><option value="0491: Logistics Mobility Chief">0491: Logistics Mobility Chief</option><option value="0511: MAGTF Planning Specialist">0511: MAGTF Planning Specialist</option><option value="0521: Psychological Operations Noncommissioned Officer">0521: Psychological Operations Noncommissioned Officer</option><option value="0531: Civil Affairs Noncommissioned Officer">0531: Civil Affairs Noncommissioned Officer</option><option value="0612: Field Wireman">0612: Field Wireman</option><option value="0614: Unit Level Circuit Switch (ULCS) Operator/Maintainer">0614: Unit Level Circuit Switch (ULCS) Operator/Maintainer</option><option value="0618: Electronic Switching Operator/Maintainer">0618: Electronic Switching Operator/Maintainer</option><option value="0619: Wire Chief">0619: Wire Chief</option><option value="0621: Field Radio Operator">0621: Field Radio Operator</option><option value="0622: Digital (Multi-channel) Wideband Transmission Equipment Operator">0622: Digital (Multi-channel) Wideband Transmission Equipment Operator</option><option value="0623: Tropospheric Scatter Radio Multi-channel. Equipment Operator">0623: Tropospheric Scatter Radio Multi-channel. Equipment Operator</option><option value="0627: SHF Satellite Communications Operator-Maintainer">0627: SHF Satellite Communications Operator-Maintainer</option><option value="0628: EHF Satellite Communications Operator-Maintainer">0628: EHF Satellite Communications Operator-Maintainer</option><option value="0629: Radio Chief">0629: Radio Chief</option><option value="0648: Strategic Spectrum Manager">0648: Strategic Spectrum Manager</option><option value="0651: Data Network Specialist">0651: Data Network Specialist</option><option value="0652: Certification Authority Workstation">0652: Certification Authority Workstation</option><option value="0653: Defense Message System">0653: Defense Message System</option><option value="0656: Tactical Network Specialist">0656: Tactical Network Specialist</option><option value="0658: Tactical Data Network Gateway Systems Administrator">0658: Tactical Data Network Gateway Systems Administrator</option><option value="0659: Data Chief">0659: Data Chief</option><option value="0681: Information Security Technician">0681: Information Security Technician</option><option value="0689: Information Assurance Technician">0689: Information Assurance Technician</option><option value="0699: Communications Chief">0699: Communications Chief</option><option value="0811: Field Artillery Cannoneer">0811: Field Artillery Cannoneer</option><option value="0814: High Mobility Artillery Rocket System">0814: High Mobility Artillery Rocket System</option><option value="0842: Field Artillery Radar Operator">0842: Field Artillery Radar Operator</option><option value="0844: Field Artillery Fire Control Man">0844: Field Artillery Fire Control Man</option><option value="0847: Artillery Meteorological Man">0847: Artillery Meteorological Man</option><option value="0848: Field Artillery Operations Man">0848: Field Artillery Operations Man</option><option value="0861: Fire Support Man">0861: Fire Support Man</option><option value="0911: Drill Instructor">0911: Drill Instructor</option><option value="0913: Marine Combat Instructor">0913: Marine Combat Instructor</option><option value="0916: Martial Arts Instructor">0916: Martial Arts Instructor</option><option value="0917: Martial Arts Instructor-Trainer">0917: Martial Arts Instructor-Trainer</option><option value="0918: Water Safety/Survival Instructor">0918: Water Safety/Survival Instructor</option><option value="0931: Marksmanship Instructor">0931: Marksmanship Instructor</option><option value="0932: Small Arms Weapons Instructor">0932: Small Arms Weapons Instructor</option><option value="0933: Marksmanship Coach">0933: Marksmanship Coach</option><option value="1141: Electrician">1141: Electrician</option><option value="1142: Engineer Equipment Electrical Systems Technician">1142: Engineer Equipment Electrical Systems Technician</option><option value="1161: Refrigeration and Air Conditioning Technician">1161: Refrigeration and Air Conditioning Technician</option><option value="1169: Utilities Chief">1169: Utilities Chief</option><option value="1171: Water Support Technician">1171: Water Support Technician</option><option value="1316: Metal Worker">1316: Metal Worker</option><option value="1341: Engineer Equipment Mechanic">1341: Engineer Equipment Mechanic</option><option value="1342: Small Craft Mechanic">1342: Small Craft Mechanic</option><option value="1343: Assault Breacher Vehicle/Joint Assault Bridge (JAB) Mechanic">1343: Assault Breacher Vehicle/Joint Assault Bridge (JAB) Mechanic</option><option value="1345: Engineer Equipment Operator">1345: Engineer Equipment Operator</option><option value="1349: Engineer Equipment Chief">1349: Engineer Equipment Chief</option><option value="1361: Engineer Assistant">1361: Engineer Assistant</option><option value="1371: Combat Engineer">1371: Combat Engineer</option><option value="1372: Assault Breacher Vehicle">1372: Assault Breacher Vehicle</option><option value="1391: Bulk Fuel Specialist">1391: Bulk Fuel Specialist</option><option value="1812: M1A1 Tank Crewman">1812: M1A1 Tank Crewman</option><option value="1833: Assault Amphibious Vehicle (AAV) Crewmember">1833: Assault Amphibious Vehicle (AAV) Crewmember</option><option value="1834: Expeditionary Fighting Vehicle (EFV) Crewman">1834: Expeditionary Fighting Vehicle (EFV) Crewman</option><option value="2111: Small Arms Repairer/Technician">2111: Small Arms Repairer/Technician</option><option value="2112: Precision Weapons Repairer/Technician">2112: Precision Weapons Repairer/Technician</option><option value="2131: Towed Artillery Systems Technician">2131: Towed Artillery Systems Technician</option><option value="2141: Assault Amphibious Vehicle (AAV) Repairer/Technician">2141: Assault Amphibious Vehicle (AAV) Repairer/Technician</option><option value="2146: Main Battle Tank (MBT) Repairer/Technician">2146: Main Battle Tank (MBT) Repairer/Technician</option><option value="2147: Light Armored Vehicle (LAV) Repairer/Technician">2147: Light Armored Vehicle (LAV) Repairer/Technician</option><option value="2148: Expeditionary Fighting Vehicle (EFV) Repairer/Technician">2148: Expeditionary Fighting Vehicle (EFV) Repairer/Technician</option><option value="2149: Ordnance vehicle maintenance chief">2149: Ordnance vehicle maintenance chief</option><option value="2161: Machinist">2161: Machinist</option><option value="2171: Electra-Optical Ordnance Repairer">2171: Electra-Optical Ordnance Repairer</option><option value="2181: Senior Ground Ordnance Weapons Chief">2181: Senior Ground Ordnance Weapons Chief</option><option value="2311: Ammunition Technician">2311: Ammunition Technician</option><option value="2336: Explosive Ordnance Disposal">2336: Explosive Ordnance Disposal</option><option value="2621: Special Communications Signals Collection Operator">2621: Special Communications Signals Collection Operator</option><option value="2629: Signals Intelligence Analyst">2629: Signals Intelligence Analyst</option><option value="2631: Electronic Intelligence (ELINT) Intercept Operator/Analyst">2631: Electronic Intelligence (ELINT) Intercept Operator/Analyst</option><option value="2651: Special Intelligence System Administrator/Communicator">2651: Special Intelligence System Administrator/Communicator</option><option value="2671: Middle East Cryptologic Linguist">2671: Middle East Cryptologic Linguist</option><option value="2673: Asia-Pacific Cryptologic Linguist">2673: Asia-Pacific Cryptologic Linguist</option><option value="2674: European I (West) Cryptologic Linguist">2674: European I (West) Cryptologic Linguist</option><option value="2676: European II (East) Cryptologic Linguist">2676: European II (East) Cryptologic Linguist</option><option value="2691: Signals Intelligence/Electronic Warfare Chief">2691: Signals Intelligence/Electronic Warfare Chief</option><option value="2711: Afghan Pashto Linguist (MGySgt-Pvt) EMOS">2711: Afghan Pashto Linguist (MGySgt-Pvt) EMOS</option><option value="2712: Arabic (Mod Std) Linguist">2712: Arabic (Mod Std) Linguist</option><option value="2713: Arabic (Egyptian) Linguist">2713: Arabic (Egyptian) Linguist</option><option value="2714: Arabic (Syrian) Linguist">2714: Arabic (Syrian) Linguist</option><option value="2715: Persian-Afghan (Dari) Linguist">2715: Persian-Afghan (Dari) Linguist</option><option value="2716: Amharic Linguist">2716: Amharic Linguist</option><option value="2717: Bengali Linguist">2717: Bengali Linguist</option><option value="2718: Hebrew Linguist">2718: Hebrew Linguist</option><option value="2719: Hindi Linguist">2719: Hindi Linguist</option><option value="2721: Kurdish Linguist">2721: Kurdish Linguist</option><option value="2722: Persian-Farsi Linguist">2722: Persian-Farsi Linguist</option><option value="2723: Somali Linguist">2723: Somali Linguist</option><option value="2724: Swahili Linguist">2724: Swahili Linguist</option><option value="2726: Turkish Linguist">2726: Turkish Linguist</option><option value="2727: Urdu Linguist">2727: Urdu Linguist</option><option value="2728: Arabic (Iraqi)">2728: Arabic (Iraqi)</option><option value="2733: Burmese Linguist">2733: Burmese Linguist</option><option value="2734: Cambodian Linguist">2734: Cambodian Linguist</option><option value="2736: Chinese (Cant) Linguist">2736: Chinese (Cant) Linguist</option><option value="2738: Indonesian Linguist">2738: Indonesian Linguist</option><option value="2739: Japanese Linguist">2739: Japanese Linguist</option><option value="2741: Korean Linguist">2741: Korean Linguist</option><option value="2742: Laotian Linguist">2742: Laotian Linguist</option><option value="2743: Malay Linguist">2743: Malay Linguist</option><option value="2744: Tagalog Linguist">2744: Tagalog Linguist</option><option value="2746: Thai Linguist">2746: Thai Linguist</option><option value="2754: Dutch Linguist">2754: Dutch Linguist</option><option value="2756: Finnish Linguist">2756: Finnish Linguist</option><option value="2757: French Linguist">2757: French Linguist</option><option value="2758: German Linguist">2758: German Linguist</option><option value="2759: Greek Linguist">2759: Greek Linguist</option><option value="2761: Haitian-Creole Linguist">2761: Haitian-Creole Linguist</option><option value="2763: Italian Linguist">2763: Italian Linguist</option><option value="2764: Norwegian Linguist">2764: Norwegian Linguist</option><option value="2766: Portuguese (BR) Linguist">2766: Portuguese (BR) Linguist</option><option value="2767: Portuguese (EU) Linguist">2767: Portuguese (EU) Linguist</option><option value="2768: Spanish Linguist">2768: Spanish Linguist</option><option value="2769: Swedish Linguist">2769: Swedish Linguist</option><option value="2776: Albanian Linguist">2776: Albanian Linguist</option><option value="2777: Armenian Linguist">2777: Armenian Linguist</option><option value="2778: Bulgarian Linguist">2778: Bulgarian Linguist</option><option value="2779: Czech Linguist">2779: Czech Linguist</option><option value="2781: Estonian Linguist">2781: Estonian Linguist</option><option value="2782: Georgian Linguist">2782: Georgian Linguist</option><option value="2783: Hungarian Linguist">2783: Hungarian Linguist</option><option value="2784: Latvian Linguist">2784: Latvian Linguist</option><option value="2786: Lithuanian Linguist">2786: Lithuanian Linguist</option><option value="2787: Macedonian Linguist">2787: Macedonian Linguist</option><option value="2788: Polish Linguist">2788: Polish Linguist</option><option value="2789: Romanian Linguist">2789: Romanian Linguist</option><option value="2791: Russian Linguist">2791: Russian Linguist</option><option value="2792: Serb-Croat Linguist">2792: Serb-Croat Linguist</option><option value="2793: Slovenian Linguist">2793: Slovenian Linguist</option><option value="2794: Ukrainian Linguist">2794: Ukrainian Linguist</option><option value="2799: Military Interpreter/Translator">2799: Military Interpreter/Translator</option><option value="2821: Technical Controller Marine">2821: Technical Controller Marine</option><option value="2822: Electronic Switching Equipment Technician">2822: Electronic Switching Equipment Technician</option><option value="2823: Technical Control Chief">2823: Technical Control Chief</option><option value="2827: Tactical Electronic Reconnaissance Process/Evaluation Systems (TERPES) Technician">2827: Tactical Electronic Reconnaissance Process/Evaluation Systems (TERPES) Technician</option><option value="2831: AN/TRC-170 Technician">2831: AN/TRC-170 Technician</option><option value="2834: Satellite Communications (SATCOM) Technician">2834: Satellite Communications (SATCOM) Technician</option><option value="2841: Ground Radio Repairer">2841: Ground Radio Repairer</option><option value="2844: Ground Communications Organizational Repairer">2844: Ground Communications Organizational Repairer</option><option value="2846: Ground Radio Intermediate Repairer">2846: Ground Radio Intermediate Repairer</option><option value="2847: Telephone Systems/PersonalComputer Intermediate Repairer">2847: Telephone Systems/PersonalComputer Intermediate Repairer</option><option value="2848: Tactical Remote Sensor System (TRSS) Maintainer">2848: Tactical Remote Sensor System (TRSS) Maintainer</option><option value="2862: Electronics Maintenance Technician">2862: Electronics Maintenance Technician</option><option value="2871: Test Measurement and Diagnostic Equipment Technician">2871: Test Measurement and Diagnostic Equipment Technician</option><option value="2874: Metrology Technician">2874: Metrology Technician</option><option value="2881: 2M/ATE Technician">2881: 2M/ATE Technician</option><option value="2887: Artillery Electronics Technician">2887: Artillery Electronics Technician</option><option value="2891: Electronics Maintenance Chief">2891: Electronics Maintenance Chief</option><option value="3043: Supply Administration and Operations Specialist">3043: Supply Administration and Operations Specialist</option><option value="3044: Contract Specialist">3044: Contract Specialist</option><option value="3051: Warehouse Clerk">3051: Warehouse Clerk</option><option value="3052: Packaging Specialist">3052: Packaging Specialist</option><option value="3112: Traffic Management Specialist">3112: Traffic Management Specialist</option><option value="3372: Marine Aide">3372: Marine Aide</option><option value="3381: Food Service, Specialist">3381: Food Service, Specialist</option><option value="3432: Finance Technician">3432: Finance Technician</option><option value="3441: NAF Audit Technician">3441: NAF Audit Technician</option><option value="3451: Financial Management Resource Analyst">3451: Financial Management Resource Analyst</option><option value="3521: Automotive Organizational Mechanic">3521: Automotive Organizational Mechanic</option><option value="3522: Automotive Intermediate Mechanic">3522: Automotive Intermediate Mechanic</option><option value="3523: Logistics Vehicle System Mechanic">3523: Logistics Vehicle System Mechanic</option><option value="3524: Fuel and Electrical Systems Mechanic">3524: Fuel and Electrical Systems Mechanic</option><option value="3526: Crash/Fire/Rescue Vehicle Mechanic">3526: Crash/Fire/Rescue Vehicle Mechanic</option><option value="3529: Motor Transport Maintenance Chief">3529: Motor Transport Maintenance Chief</option><option value="3531: Motor Vehicle Operator">3531: Motor Vehicle Operator</option><option value="3533: Logistics Vehicle System Operator">3533: Logistics Vehicle System Operator</option><option value="3534: Semitrailer Refueler operator">3534: Semitrailer Refueler operator</option><option value="3536: Vehicle Recovery Operator">3536: Vehicle Recovery Operator</option><option value="3537: Motor Transport Operations Chief">3537: Motor Transport Operations Chief</option><option value="3538: Licensing Examiner">3538: Licensing Examiner</option><option value="4034: Main Frame Operator">4034: Main Frame Operator</option><option value="4038: Production Control">4038: Production Control</option><option value="4063: Computer Programmer">4063: Computer Programmer</option><option value="4066: Network Engineer">4066: Network Engineer</option><option value="4068: Advanced Networking">4068: Advanced Networking</option><option value="4069: Systems Programmer">4069: Systems Programmer</option><option value="4071: Database Administrator">4071: Database Administrator</option><option value="4133: Marine Corps Community Services Marine">4133: Marine Corps Community Services Marine</option><option value="4313: Broadcast Journalist">4313: Broadcast Journalist</option><option value="4341: Combat Correspondent">4341: Combat Correspondent</option><option value="4421: Legal Services Specialist">4421: Legal Services Specialist</option><option value="4429: Legal Services Reporter">4429: Legal Services Reporter</option><option value="4611: Combat Illustrator">4611: Combat Illustrator</option><option value="4612: Combat Lithographer">4612: Combat Lithographer</option><option value="4641: Combat Photographer">4641: Combat Photographer</option><option value="4671: Combat Videographer">4671: Combat Videographer</option><option value="4691: Visual Information Chief">4691: Visual Information Chief</option><option value="4821: Career Planner">4821: Career Planner</option><option value="5511: Member, &quot;The President's Own,&quot; United States Marine Band">5511: Member, "The President's Own," United States Marine Band</option><option value="5512: Member, &quot;The Commadant's Own,&quot; U.S. Marine Drum &amp; Bugle Corps">5512: Member, "The Commadant's Own," U.S. Marine Drum &amp; Bugle Corps</option><option value="5517: Bandmaster">5517: Bandmaster</option><option value="5519: Enlisted Conductor">5519: Enlisted Conductor</option><option value="5521: Drum Major">5521: Drum Major</option><option value="5522: Small Ensemble Leader">5522: Small Ensemble Leader</option><option value="5523: Instrument Repair Technician">5523: Instrument Repair Technician</option><option value="5524: Musician">5524: Musician</option><option value="5526: Musician, Oboe">5526: Musician, Oboe</option><option value="5528: Musician, Bassoon">5528: Musician, Bassoon</option><option value="5534: Musician, Clarinet">5534: Musician, Clarinet</option><option value="5536: Musician, Flute/Piccolo">5536: Musician, Flute/Piccolo</option><option value="5537: Musician, Saxophone">5537: Musician, Saxophone</option><option value="5541: Musician, Trumpet">5541: Musician, Trumpet</option><option value="5543: Musician, Euphonium">5543: Musician, Euphonium</option><option value="5544: Musician, Horn">5544: Musician, Horn</option><option value="5546: Musician, Trombone">5546: Musician, Trombone</option><option value="5547: Musician, Tuba/Sousaphone">5547: Musician, Tuba/Sousaphone</option><option value="5548: Musician, Electric Bass">5548: Musician, Electric Bass</option><option value="5563: Musician, Percussion (Drums, Tympani, and Mallets)">5563: Musician, Percussion (Drums, Tympani, and Mallets)</option><option value="5566: Musician, Guitar">5566: Musician, Guitar</option><option value="5711: Chemical, Biological, Radiological, and Nuclear (CBRN) Defense Specialist">5711: Chemical, Biological, Radiological, and Nuclear (CBRN) Defense Specialist</option><option value="5731: JCBRNRS LAV Operator">5731: JCBRNRS LAV Operator</option><option value="5811: Military Police">5811: Military Police</option><option value="5812: Working Dog Handler">5812: Working Dog Handler</option><option value="5813: Accident Investigator">5813: Accident Investigator</option><option value="5814: Physical Security Specialist">5814: Physical Security Specialist</option><option value="5816: Special Reaction Team (3RT) Member">5816: Special Reaction Team (3RT) Member</option><option value="5819: Military Police Investigator (MPI)">5819: Military Police Investigator (MPI)</option><option value="5821: Criminal Investigator CID Agent">5821: Criminal Investigator CID Agent</option><option value="5822: Forensic Psycho-physiologist (Polygraph Examiner)">5822: Forensic Psycho-physiologist (Polygraph Examiner)</option><option value="5831: Correctional Specialist">5831: Correctional Specialist</option><option value="5832: Correctional Counselor">5832: Correctional Counselor</option><option value="5912: Avenger System Maintainer">5912: Avenger System Maintainer</option><option value="5923: Hawk Radar Repair Technician">5923: Hawk Radar Repair Technician</option><option value="5939: Aviation Communication Systems Technician">5939: Aviation Communication Systems Technician</option><option value="5942: Aviation Radar Repairer">5942: Aviation Radar Repairer</option><option value="5948: Aviation Radar Technician">5948: Aviation Radar Technician</option><option value="5952: Air Traffic Control Navigational Aids Technician">5952: Air Traffic Control Navigational Aids Technician</option><option value="5953: Air Traffic Control Radar Technician">5953: Air Traffic Control Radar Technician</option><option value="5954: Air Traffic Control Communications Technician">5954: Air Traffic Control Communications Technician</option><option value="5959: Air Traffic Control Systems Maintenance">5959: Air Traffic Control Systems Maintenance</option><option value="5974: Tactical Data Systems Administrator">5974: Tactical Data Systems Administrator</option><option value="5979: Tactical Air Operations Module/Air Defense Technician">5979: Tactical Air Operations Module/Air Defense Technician</option><option value="5993: Electronics Maintenance Chief">5993: Electronics Maintenance Chief</option><option value="6012: Aviation Maintenance Controller/Production Controller">6012: Aviation Maintenance Controller/Production Controller</option><option value="6018: Aviation QAR/CDQAR/COI">6018: Aviation QAR/CDQAR/COI</option><option value="6019: Aircraft Maintenance Chief">6019: Aircraft Maintenance Chief</option><option value="6023: Aircraft Power Plants Test Cell Operator">6023: Aircraft Power Plants Test Cell Operator</option><option value="6033: Aircraft Nondestructive Inspection Technician">6033: Aircraft Nondestructive Inspection Technician</option><option value="6042: Individual Material Readiness List (IMRS) Asset Manager">6042: Individual Material Readiness List (IMRS) Asset Manager</option><option value="6043: Aircraft Welder">6043: Aircraft Welder</option><option value="6046: Aircraft Maintenance Administration Specialist">6046: Aircraft Maintenance Administration Specialist</option><option value="6048: Flight Equipment Technician">6048: Flight Equipment Technician</option><option value="6049: NALCOMIS Application Administrator/Analyst">6049: NALCOMIS Application Administrator/Analyst</option><option value="6062: Aircraft Intermediate Level Hydraulic/Pneumatic Mechanic">6062: Aircraft Intermediate Level Hydraulic/Pneumatic Mechanic</option><option value="6072: Aircraft Maintenance Support Equipment Hydraulic/Pneumatic/Structures Mechanic">6072: Aircraft Maintenance Support Equipment Hydraulic/Pneumatic/Structures Mechanic</option><option value="6073: Aircraft Maintenance Support Equipment Electrician/Refrigeration Mechanic">6073: Aircraft Maintenance Support Equipment Electrician/Refrigeration Mechanic</option><option value="6074: Cryogenics Equipment Operator">6074: Cryogenics Equipment Operator</option><option value="6092: Aircraft Intermediate Level Structures Mechanic">6092: Aircraft Intermediate Level Structures Mechanic</option><option value="6111: Helicopter/Tiltrotor Mechanic-Trainee">6111: Helicopter/Tiltrotor Mechanic-Trainee</option><option value="6112: Helicopter Mechanic, CH-46">6112: Helicopter Mechanic, CH-46</option><option value="6113: Helicopter Mechanic, CH-53">6113: Helicopter Mechanic, CH-53</option><option value="6114: Helicopter Mechanic, UH/AH-1">6114: Helicopter Mechanic, UH/AH-1</option><option value="6116: Tiltrotor Mechanic, MV-22">6116: Tiltrotor Mechanic, MV-22</option><option value="6122: Helicopter Power Plants Mechanic, T-5E1">6122: Helicopter Power Plants Mechanic, T-5E1</option><option value="6123: Helicopter Power Plants Mechanic, T-64">6123: Helicopter Power Plants Mechanic, T-64</option><option value="6124: Helicopter Power Plants Mechanic, T-400/T-700">6124: Helicopter Power Plants Mechanic, T-400/T-700</option><option value="6132: Helicopter/Tiltrotor Dynamic Components Mechanic">6132: Helicopter/Tiltrotor Dynamic Components Mechanic</option><option value="6152: Helicopter Airframe Mechanic, CH-46">6152: Helicopter Airframe Mechanic, CH-46</option><option value="6153: Helicopter Airframe Mechanic, CH-53">6153: Helicopter Airframe Mechanic, CH-53</option><option value="6154: Helicopter Airframe Mechanic, UH/AH-1">6154: Helicopter Airframe Mechanic, UH/AH-1</option><option value="6156: Tiltrotor Airframe Mechanic, MV-22">6156: Tiltrotor Airframe Mechanic, MV-22</option><option value="6162: Presidential Support Specialist">6162: Presidential Support Specialist</option><option value="6172: Helicopter Crew Chief, CH-46">6172: Helicopter Crew Chief, CH-46</option><option value="6173: Helicopter Crew Chief, CH-53">6173: Helicopter Crew Chief, CH-53</option><option value="6174: Helicopter Crew Chief, UH-1">6174: Helicopter Crew Chief, UH-1</option><option value="6176: Tiltrotor Crew Chief, MV-22">6176: Tiltrotor Crew Chief, MV-22</option><option value="6177: Weapons and Tactics Crew Chief Instructor">6177: Weapons and Tactics Crew Chief Instructor</option><option value="6178: VH-GON Presidential Helicopter Crew Chief">6178: VH-GON Presidential Helicopter Crew Chief</option><option value="6179: VH-3D Presidential Helicopter Crew Chief">6179: VH-3D Presidential Helicopter Crew Chief</option><option value="6199: Enlisted Aircrew/Aerial Observer/Gunner">6199: Enlisted Aircrew/Aerial Observer/Gunner</option><option value="6212: Fixed-Wing Aircraft Mechanic, AV-8/TAV-S">6212: Fixed-Wing Aircraft Mechanic, AV-8/TAV-S</option><option value="6213: Fixed-Wing Aircraft Mechanic, EA-6">6213: Fixed-Wing Aircraft Mechanic, EA-6</option><option value="6214: Unmanned Aerial Vehicle (UAV) Mechanic">6214: Unmanned Aerial Vehicle (UAV) Mechanic</option><option value="6216: Fixed-Wing Aircraft Mechanic, KC-130">6216: Fixed-Wing Aircraft Mechanic, KC-130</option><option value="6217: Fixed-Wing Aircraft Mechanic, F/A-18">6217: Fixed-Wing Aircraft Mechanic, F/A-18</option><option value="6222: Fixed Wing Aircraft Power Plants Mechanic, F-402">6222: Fixed Wing Aircraft Power Plants Mechanic, F-402</option><option value="6223: Fixed-wing Aircraft Power Plants Mechanic, J-52">6223: Fixed-wing Aircraft Power Plants Mechanic, J-52</option><option value="6226: Fixed-Wing Aircraft Power Plants Mechanic, T-56">6226: Fixed-Wing Aircraft Power Plants Mechanic, T-56</option><option value="6227: Fixed-Wing Aircraft Power Plants Mechanic, F-404">6227: Fixed-Wing Aircraft Power Plants Mechanic, F-404</option><option value="6242: Fixed-Wing Aircraft Flight Engineer, KC-130">6242: Fixed-Wing Aircraft Flight Engineer, KC-130</option><option value="6243: Fixed-Wing Transport Aircraft Specialist, C-9">6243: Fixed-Wing Transport Aircraft Specialist, C-9</option><option value="6244: Fixed-Wing Transport Aircraft Specialist, C-12">6244: Fixed-Wing Transport Aircraft Specialist, C-12</option><option value="6246: Fixed-Wing Transport Aircraft Specialist, C-20">6246: Fixed-Wing Transport Aircraft Specialist, C-20</option><option value="6247: Fixed-Wing Transport Aircraft Specialist, UC-35">6247: Fixed-Wing Transport Aircraft Specialist, UC-35</option><option value="6251: Fixed-Wing Aircraft Airframe Mechanic-Trainee">6251: Fixed-Wing Aircraft Airframe Mechanic-Trainee</option><option value="6252: Fixed-Wing Aircraft Airframe Mechanic, AV-8/TAV-8">6252: Fixed-Wing Aircraft Airframe Mechanic, AV-8/TAV-8</option><option value="6253: Fixed-Wing Aircraft Airframe Mechanic, EA-6">6253: Fixed-Wing Aircraft Airframe Mechanic, EA-6</option><option value="6256: Fixed-Wing Aircraft Airframe Mechanic, KC-130">6256: Fixed-Wing Aircraft Airframe Mechanic, KC-130</option><option value="6257: Fixed-wing Aircraft Airframe Mechanic, F/A-18">6257: Fixed-wing Aircraft Airframe Mechanic, F/A-18</option><option value="6276: Fixed-Wing Aircraft Crew Chief, KC-130">6276: Fixed-Wing Aircraft Crew Chief, KC-130</option><option value="6281: Fixed-Wing Aircraft Safety Equipment Mechanic-Trainee">6281: Fixed-Wing Aircraft Safety Equipment Mechanic-Trainee</option><option value="6282: Fixed-Wing Aircraft Safety Equipment Mechanic, AV-8/TJW-B">6282: Fixed-Wing Aircraft Safety Equipment Mechanic, AV-8/TJW-B</option><option value="6283: Fixed-Wing Aircraft Safety Equipment Mechanic, EA-6">6283: Fixed-Wing Aircraft Safety Equipment Mechanic, EA-6</option><option value="6286: Fixed-Wing Aircraft Safety Equipment Mechanic, KC-130">6286: Fixed-Wing Aircraft Safety Equipment Mechanic, KC-130</option><option value="6287: Fixed-Wing Aircraft Safety Equipment Mechanic, F/A-18">6287: Fixed-Wing Aircraft Safety Equipment Mechanic, F/A-18</option><option value="6312: Aircraft Communications/Navigation/Radar Systems Technician, AV-8">6312: Aircraft Communications/Navigation/Radar Systems Technician, AV-8</option><option value="6313: Aircraft Communications/Navigation/Radar Systems Technician, EA-6">6313: Aircraft Communications/Navigation/Radar Systems Technician, EA-6</option><option value="6314: Unmanned Aerial System (UAS) Avionics Technician">6314: Unmanned Aerial System (UAS) Avionics Technician</option><option value="6316: Aircraft Communications/Navigation Systems Technician, KC-130">6316: Aircraft Communications/Navigation Systems Technician, KC-130</option><option value="6317: Aircraft communications/Navigation/Radar Systems Technician, F/A-18">6317: Aircraft communications/Navigation/Radar Systems Technician, F/A-18</option><option value="6322: Aircraft Communications/Navigation Electrical Systems Technician, CH-46">6322: Aircraft Communications/Navigation Electrical Systems Technician, CH-46</option><option value="6323: Aircraft Communications/Navigation/ElectricalSystems Technician, CH-53">6323: Aircraft Communications/Navigation/ElectricalSystems Technician, CH-53</option><option value="6324: Aircraft Communications/Navigation/Electrical/Weapon Systems Technician, U/AH-l">6324: Aircraft Communications/Navigation/Electrical/Weapon Systems Technician, U/AH-l</option><option value="6326: Aircraft Communications/Navigation/Electrical/Systems Technician, V-22">6326: Aircraft Communications/Navigation/Electrical/Systems Technician, V-22</option><option value="6332: Aircraft Electrical Systems Technician, AV-8">6332: Aircraft Electrical Systems Technician, AV-8</option><option value="6333: Aircraft Electrical Systems Technician, EA-6">6333: Aircraft Electrical Systems Technician, EA-6</option><option value="6336: Aircraft Electrical Systems Technician, KC-130">6336: Aircraft Electrical Systems Technician, KC-130</option><option value="6337: Aircraft Electrical Systems Technician, F/A-18">6337: Aircraft Electrical Systems Technician, F/A-18</option><option value="6386: Aircraft Electronic Countermeasures Systems Technician, EA-6B">6386: Aircraft Electronic Countermeasures Systems Technician, EA-6B</option><option value="6391: Avionics Maintenance Chief">6391: Avionics Maintenance Chief</option><option value="6412: Aircraft Communications Systems Technician, IMA">6412: Aircraft Communications Systems Technician, IMA</option><option value="6413: Aircraft Navigation Systems Technician, I FF/RADAR/TACAN , IMA">6413: Aircraft Navigation Systems Technician, I FF/RADAR/TACAN , IMA</option><option value="6414: Advanced Aircraft Communications/Navigation Systems Technician, IMA">6414: Advanced Aircraft Communications/Navigation Systems Technician, IMA</option><option value="6422: Aircraft cryptographic Systems Technician, IMA">6422: Aircraft cryptographic Systems Technician, IMA</option><option value="6423: Aviation Electronic Microminiature/Instrument and Cable Repair Technician, IMA">6423: Aviation Electronic Microminiature/Instrument and Cable Repair Technician, IMA</option><option value="6432: Aircraft Electrical/lnstrument/Flight Control Systems Technician, Fixed Wing, IMA">6432: Aircraft Electrical/lnstrument/Flight Control Systems Technician, Fixed Wing, IMA</option><option value="6433: Aircraft Electrical/Instrument/Flight Control Systems Technician, Helicopter, IMA">6433: Aircraft Electrical/Instrument/Flight Control Systems Technician, Helicopter, IMA</option><option value="6434: Advanced Aircraft Electrical/Instrument/Flight Control Systems Technician, IMA">6434: Advanced Aircraft Electrical/Instrument/Flight Control Systems Technician, IMA</option><option value="6461: Hybrid Test Set Technician, IMA">6461: Hybrid Test Set Technician, IMA</option><option value="6462: Avionics Test Set (ATS) Technician, IMA">6462: Avionics Test Set (ATS) Technician, IMA</option><option value="6463: CASS HP Configuration Operator/Maintainer/Technician, IMA">6463: CASS HP Configuration Operator/Maintainer/Technician, IMA</option><option value="6464: Aircraft Inertial Navigation System Technician, IMA">6464: Aircraft Inertial Navigation System Technician, IMA</option><option value="6466: CASS EO Configuration Operator/Maintainer/Technician/IMA">6466: CASS EO Configuration Operator/Maintainer/Technician/IMA</option><option value="6467: Consolidated Automatic Support System (CASS) Technician, IMA">6467: Consolidated Automatic Support System (CASS) Technician, IMA</option><option value="6469: CASS Test Station IMA Advanced Maintenance Technician, IMA">6469: CASS Test Station IMA Advanced Maintenance Technician, IMA</option><option value="6482: Aircraft Electronic Countermeasures Systems Technician, Fixed-Wing, IMA">6482: Aircraft Electronic Countermeasures Systems Technician, Fixed-Wing, IMA</option><option value="6483: Aircraft Electronic Countermeasures Systems Technician, Helicopter, IMA">6483: Aircraft Electronic Countermeasures Systems Technician, Helicopter, IMA</option><option value="6484: CASS EW Configuration Operator/Maintainer/Technician, IMA">6484: CASS EW Configuration Operator/Maintainer/Technician, IMA</option><option value="6492: Aviation Precision Measurement Equipment/Calibration and Repair Technician, IMA">6492: Aviation Precision Measurement Equipment/Calibration and Repair Technician, IMA</option><option value="6493: Aviation Meteorological Equipment Technician, OMA/IMA">6493: Aviation Meteorological Equipment Technician, OMA/IMA</option><option value="6531: Aircraft Ordnance Technician">6531: Aircraft Ordnance Technician</option><option value="6541: Aviation Ordnance Systems Technician">6541: Aviation Ordnance Systems Technician</option><option value="6591: Aviation Ordnance Chief">6591: Aviation Ordnance Chief</option><option value="6617: Enlisted Aviation Logistician">6617: Enlisted Aviation Logistician</option><option value="6672: Aviation Supply Specialist">6672: Aviation Supply Specialist</option><option value="6694: Aviation Information Systems (AIS) Specialist">6694: Aviation Information Systems (AIS) Specialist</option><option value="6821: METOC Observer">6821: METOC Observer</option><option value="6842: METOC Analyst Forecaster">6842: METOC Analyst Forecaster</option><option value="6852: METOC Impact Analyst">6852: METOC Impact Analyst</option><option value="7011: Exeditionary Airfield Systems Technician">7011: Exeditionary Airfield Systems Technician</option><option value="7041: Aviation Operations Specialist">7041: Aviation Operations Specialist</option><option value="7051: Aircraft Rescue and Firefighting Specialist">7051: Aircraft Rescue and Firefighting Specialist</option><option value="7212: Low Altitude Air Defense (LAAD) Gunner">7212: Low Altitude Air Defense (LAAD) Gunner</option><option value="7222: Hawk Missile Operator">7222: Hawk Missile Operator</option><option value="7234: Air Control Electronics Operator">7234: Air Control Electronics Operator</option><option value="7236: Tactical Air Defense Controller">7236: Tactical Air Defense Controller</option><option value="7242: Air Support Operations Operator">7242: Air Support Operations Operator</option><option value="7251: Air Traffic Controller-Trainee">7251: Air Traffic Controller-Trainee</option><option value="7252: Air Traffic Controller-Tower">7252: Air Traffic Controller-Tower</option><option value="7253: Air Traffic Controller-Radar Arrival/Departure Controller">7253: Air Traffic Controller-Radar Arrival/Departure Controller</option><option value="7254: Air Traffic Controller-Radar Approach Controller">7254: Air Traffic Controller-Radar Approach Controller</option><option value="7257: Air Traffic Controller">7257: Air Traffic Controller</option><option value="7291: Senior Air Traffic Controller">7291: Senior Air Traffic Controller</option><option value="7312: Fixed-Wing Transport Aircraft Specialist, KC-130J">7312: Fixed-Wing Transport Aircraft Specialist, KC-130J</option><option value="7313: Helicopter specialist, AH-1Z/UH-1Y">7313: Helicopter specialist, AH-1Z/UH-1Y</option><option value="7314: Unmanned Aerial Vehicle (UAV) Air Vehicle Operator">7314: Unmanned Aerial Vehicle (UAV) Air Vehicle Operator</option><option value="7371: Aerial Navigator-Trainee">7371: Aerial Navigator-Trainee</option><option value="7372: First Navigator">7372: First Navigator</option><option value="7374: Vertical Takeoff Unmanned Aerial Vehicle Specialist">7374: Vertical Takeoff Unmanned Aerial Vehicle Specialist</option><option value="7382: Airborne Radio Operator/In-flight Refueling Observer/Loadmaster">7382: Airborne Radio Operator/In-flight Refueling Observer/Loadmaster</option><option value="8060: Acquisition Specialist">8060: Acquisition Specialist</option><option value="8152: Basic Security Guard.">8152: Basic Security Guard.</option><option value="8156: Marine Security Guard">8156: Marine Security Guard</option><option value="8411: Basic Recruiter">8411: Basic Recruiter</option><option value="8412: Career Recruiter">8412: Career Recruiter</option><option value="8541: Scout Sniper">8541: Scout Sniper</option><option value="8621: Sensor Surveillance Operator">8621: Sensor Surveillance Operator</option><option value="8999: Sergeant Major/First Sergeant">8999: Sergeant Major/First Sergeant</option></select>
											</div>
											<div class="form-group">
												<label>Did you ever deploy to a formally designated combat zone?</label>
												<select name="military_zone" class="form-control"><option value="">Please select...</option><option value="iraq_war">Iraq War</option><option value="afghanistan_war">Afghanistan War</option><option value="iraq_post_gulf_war">Iraq (post-Gulf War)</option><option value="persian_gulf_war">Persian Gulf War</option><option value="haiti">Haiti</option><option value="macedonia">Macedonia</option><option value="libya">Libya</option><option value="liberia">Mano River Region (Liberia)</option><option value="somalia">Horn of Africa (Somalia)</option><option value="vietnam_war">Vietnam War</option><option value="korean_war">Korean War</option><option value="indochina_war">Indochina War</option><option value="central_america_carribean">Central America &amp; the Carribean</option><option value="south_america">South America</option><option value="europe">Europe</option><option value="asia">Asia</option><option value="other">Other</option><option value="no">No</option></select>
											</div>
											<div class="form-group">
												<label>Are you eligible for or enrolled in the VA healthcare system?</label>
												<select name="military_health" class="form-control"><option value="">Please select...</option><option value="ineligible">Not eligible</option><option value="eligible_but_unenrolled">Eligible but not enrolled</option><option value="enrolled">Enrolled</option></select>
											</div>
										</div>
									</form>
									<div class="modal-footer">
										<button type="submit" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-sm btn-primary saveMilitary">Save</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="addMoreDemo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title text-black" id="exampleModalCenterTitle">More Demographics</h4>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<form class="post">
										<div class="modal-body">
											<div class="form-group">
												<label for="inputUsernameEmail">Ethnicity</label>
												<select class="form-control" name="ethnicity">
													<option value="" selected="" disabled="">Ethnicity</option>
													<option value="I'm Hispanic or Latino">I'm Hispanic or Latino</option>
													<option value="I'm not Hispanic or Latino">I'm not Hispanic or Latino</option>
													<option value="I prefer not to answer">I prefer not to answer</option>
												</select>
											</div>
											<div class="form-group">
												<label for="inputUsernameEmail">Race</label>
												<select class="form-control" name="race">
													<option value="" selected="" disabled="">Select Race</option>
													<option value="White">White</option>
													<option value="Black or African American">Black or African American</option>
													<option value="Asian">Asian</option>
													<option value="Native Hawaiian or Pacific Islander">Native Hawaiian or Pacific Islander</option>
													<option value="Americam Indian or Alaska Native">Americam Indian or Alaska Native</option>
													<option value="I prefer not to answer">I prefer not to answer</option>
												</select>
											</div>
											<div class="form-group">
												<label for="inputUsernameEmail">Education</label>
												<select class="form-control" name="education">
													<option value="" disabled="">Select Education</option>
													<option value="8th grade or less (left school around 14)">8th grade or less (left school around 14)</option>
													<option value="Some high school, but did not graduate (left school around 16)">Some high school, but did not graduate (left school around 16)</option>
													<option value="High school graduate or GED (left school around 18)">High school graduate or GED (left school around 18)</option>
													<option value="Some college but less than a bachelor's / undergraduate degree">Some college but less than a bachelor's / undergraduate degree</option>
													<option value="College bachelor's / undergraduate degree">College bachelor's / undergraduate degree</option>
													<option value="Postgraduate degree (Master's, doctorate, etc.)">Postgraduate degree (Master's, doctorate, etc.)</option>
													<option value="I prefer not to answer">I prefer not to answer</option>
												</select>
											</div>
											<div class="form-group">
												<label for="inputUsernameEmail">Insurance</label>
												<select class="form-control" name="insurance">
													<option value="" disabled="">Select Insurance</option>
													<option value="Private (through employer or union)">Private (through employer or union)</option>
													<option value="Private (individual plan)">Private (individual plan)</option>
													<option value="National health service">National health service</option>
													<option value="No Insurance">No Insurance</option>
													<option value="I prefer not to answer">I prefer not to answer</option>
												</select>
											</div>
										</div>
									</form>
									<div class="modal-footer">
										<button type="submit" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-sm btn-primary saveDemographics">Save</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="addBio" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
								<div class="modal-content text-black">
									<div class="modal-header">
										<h4 class="modal-title" id="exampleModalCenterTitle">Brief Bio about yourself</h4>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<form class="post">
										<div class="modal-body">
											<label>Your brief Bio</label>
											<textarea cols="60" rows="3" name="brief_bio" class="form-control" placeholder="Brief bio about yourself"></textarea>
										</div>
									</form>
									<div class="modal-footer">
										<button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-sm btn-primary saveInfoBio">Save</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript" src="<?php echo base_url('assets/lib/countries.js')?>"></script>
<?php include_once(APPPATH . 'views/include/footer-without-menu.php'); ?> 
<script type="text/javascript" src="<?php echo base_url('assets/js/aboutme/index.js')?>"></script> 