<section class="contact-section area-padding">
	<div class="container">
		<div class="d-none d-sm-block mb-5 pb-4">
			<div class="row">
				<div class="col-12">
					<h2 class="contact-title">DailyMe history</h2>
					<p>See how your status changes over time and learn what factors affect it. Is how youâ€™re feeling influenced by the time of day, the day of the week or the seasons? Rating your DailyMe frequently can help you find out.</p>
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">How are you feeling right now?</h5>
							<p class="card-text">Add a new DailyMe at anytime. Write a note to track the moments that affect your health and well-being.</p>
							<a href="#" class="btn btn-primary">Add a DailyMe</a>
						</div>
					</div>
				</div>
				<div class="col-12">
					<br>
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">DailyMe charts and history</h5>
							<div class="dropdown show">
								<a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									View
								</a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
									<a class="dropdown-item" href="#">Last Month</a>
									<a class="dropdown-item" href="#">Last 3 Months</a>
									<a class="dropdown-item" href="#">Last 6 Months</a>
									<a class="dropdown-item" href="#">Last Year</a>
									<a class="dropdown-item" href="#">Last 2 Year</a>
									<a class="dropdown-item" href="#">Since Joining SameCondion</a>
									<a class="dropdown-item" href="#">Everything</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="col-12">
					<br>
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Discovering patterns in your DailyMe history</h5>
							<p>Looking at your DailyMe entries in different ways might help you answer questions like these:</p>
							<br>
							<p>1.Are there particular days when I tend to update DailyMe more?</p>
							<p>2.Are there patterns in how my mood changes?</p>
							<p>3.How does my mood relate to my health over time?</p>
							<br>
							<p>Have you used this feature? Has it helped you learn anything about your mood? Weâ€™d love to hear from you.</p>
							<button type="button" class="btn btn-primary">Tell us what you about DailyMe</button>
							<div class="dropdown show">
								<br>
								<a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Color calendar days by
								</a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
									<a class="dropdown-item" href="#">number of DailyMe entries on that day</a>
									<a class="dropdown-item" href="#">DailyMe status on that Day</a>
									<a class="dropdown-item" href="#">high/low DailyMe status on that day(if more than one)</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="col-12">
					<br>
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Understanding larger trends</h5>
							<p>These charts are based on the date range you have selected above, so you can focus in on specific times of the year that interest you. The more you tell us about yourself, the better picture weâ€™ll be to able to present.</p>
							<br>
							<div class="dropdown show">
								<a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									For the selected date range, show
								</a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
									<a class="dropdown-item" href="#">just the number of DailyMe entries in that Hour/Day/Month</a>
									<a class="dropdown-item" href="#">the number of DailyMe entries by status in that Hour/Day/Month</a>
									<a class="dropdown-item" href="#">high/low DailyMe status on that day(if more than one)</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>