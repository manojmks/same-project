<?php
require_once APPPATH . '/third_party/vendor/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
class Mail {
    public function __construct(){
        log_message('Debug', 'PHPMailer class is loaded.');
    }

    public function send_email($to, $from_name, $subject, $message, $attachment_path = '') {
        $mail = new PHPMailer(true);
        $mail->isSMTP();
        $mail->Host     = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'care.samecondition@gmail.com';
        $mail->Password = 'Care@Same123';
        $mail->SMTPSecure = 'tls';
        $mail->Port     = 587;
        $mail->setFrom('care.samecondition@gmail.com', 'SameCondition');
        $mail->addAddress($to);
        $mail->Subject = $subject;
        $mail->isHTML(true);
        $mailContent = $message;
        $mail->Body = $mailContent;
        if (!$mail->send()) {
            $return = array(
                'success' => false,
                'message' => $mail->ErrorInfo
            );
            return $return;
        } else {
            $return = array(
                'success' => true,
                'message' => 'Message has been sent'
            );
            return $return;
        }
    }
}


