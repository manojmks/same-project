<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hospitalization_Model extends CI_Model {

	function get_reasons() {
		return $this->db->get_where( 'hospitalization_reasons', array( 'status' => '1' ))->result_array();
	}

	function add_user_hospitalization($params) {
		$this->db->insert( 'user_hospitalization', $params );
		return $this->db->insert_id();
	}

	function get_hospitalization($id, $userId) {
		return $this->db->get_where( 'user_hospitalization', array('userId' => $userId, 'id' => $id))->row_array();
	}

	function get_data($userId) {
		$this->db->select('*, user_hospitalization.id as id');
		$this->db->join( 'hospitalization_reasons', 'user_hospitalization.hospitalization_reason_id = hospitalization_reasons.id', 'left' );
		return $this->db->get_where( 'user_hospitalization', array('user_hospitalization.userId' => $userId))->result_array();
	}

	function delete_hospitalization($id, $userId) {
		$this->db->delete( 'user_hospitalization', array('id' => $id, 'userId' => $userId));
		return true;
	}
}
?>