<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Patients_Model extends CI_Model {

	function get_patients() {
		$this->db->select('*, users.updated_at as updated_at');
		$this->db->join( 'user_info', 'users.id = user_info.userId', 'left' );
		return $this->db->get_where( 'users', array('users.type' => 'patient'))->result_array();
	}
}
?>