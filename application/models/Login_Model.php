<?php
class Login_Model extends CI_Model {

	var $details;

	function validate_user( $email, $password ) {
		$this->db->from( 'users' );
		$this->db->where( 'email', $email );
		$this->db->where( 'password', md5( $password ) );
		$login = $this->db->get()->result();

		if (is_array( $login ) && count( $login ) == 1) {
			$this->details = $login[0]; 
			$this->set_session();
			return true;
		} else {
			return false;
		}
	}

	function get_user_password($id) {
		$user = $this->db->get_where( 'user_password', array( 'userId' => $id ))->row_array();
		if ($user) {
			return $user['password'];
		} else {
			return null;
		}
	}

	function checkusername($username) {
		$q = $this->db->get_where( 'users', array( 'username' => $username ), 1 );
		if ( $this->db->affected_rows() > 0 ) {
			$row = $q->row_array();
			return true;
		} else {
			return false;
		}
	}

	function verify($params) {
		if ($params['ipassword']) {
			if(password_verify($params['ipassword'], $params['upassword'] )) {
				$this->details = new \stdClass();
				$this->details->name = $params['name'];
				$this->details->username = $params['username'];
				$this->details->language = $params['language'];
				$this->details->email = $params['email'];
				$this->details->id = $params['userId'];
				$this->details->type = $params['type'];
				$this->set_session();
				return true;
			} else {
				return false;
			}
		}
	}

	function create_account( $params, $param ) {
		$this->db->insert( 'users', $params );
		$id = $this->db->insert_id();
		$param['userId'] = $params['id'];
		$data = $this->db->insert( 'user_password', $param );
		$this->session->set_userdata( array(
			'usr_id' => $params['id'],
			'id' => $params['id'],
			'name' => $params['name'],
			'username' => $params['username'],
			'language' => $params['language'],
			'email' => $params['email'],
			'type' => $params['type'],
			'LoginOK' => true
		));
		return $param['userId'];
	}

	function create_social_account( $params ) {
		$this->db->insert( 'users', $params );
		$id = $this->db->insert_id();
		$param = array(
			'userId' => $id,
			'updated_at' => date( 'Y-m-d H:i:s' )
		);
		$data = $this->db->insert( 'user_password', $param );
		$this->details = new \stdClass();
		$this->details->name = $params['name'];
		$this->details->username = $params['username'];
		$this->details->email = $params['email'];
		$this->details->id = $params['id'];
		$this->details->type = null;
		$this->set_session();
		return $id;
	}

	function add_user( $params ) {
		$this->db->insert( 'users', $params );
		$id = $this->db->insert_id();
		$param = array(
			'userId' => $id,
			'updated_at' => date( 'Y-m-d H:i:s' )
		);
		$data = $this->db->insert( 'user_password', $param );
		return $id;
	}

	function set_session() {
		$this->session->set_userdata( array(
			'usr_id' => $this->details->id,
			'id' => $this->details->id,
			'name' => $this->details->name,
			'username' => $this->details->username,
			'language' => $this->details->language,
			'email' => $this->details->email,
			'type' => $this->details->type,
			'LoginOK' => true
		) );
	}
}