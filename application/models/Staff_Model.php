<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staff_Model extends CI_Model {

	function getUserInfoByEmail( $email ) {
		$q = $this->db->get_where( 'users', array( 'email' => $email ), 1 );
		if ( $this->db->affected_rows() > 0 ) {
			$row = $q->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getUserInfoByEmailUserName( $email ) {
		$this->db->where('(users.email="'.$email.'" OR users.username="'.$email.'")');
		$q = $this->db->get_where('users', array());
		if ( $this->db->affected_rows() > 0 ) {
			$row = $q->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getUserInfoByUserName($username) {
		$this->db->where('(username="'.$username.'")');
		$q = $this->db->get_where('users', array());
		if ( $this->db->affected_rows() > 0 ) {
			$row = $q->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function getUserInfoById($id, $type) {
		return $this->db->get_where( 'users', array( 'id' => $id ))->row_array();
	}

	function get_user_details() {
		return $this->db->get_where('users', array('id' => $this->session->userdata('usr_id')))->row_array();
	}

	function add_user_info($params) {
		$this->db->insert( 'user_info', $params );
		$id = $this->db->insert_id();
		if ($id) {
			return $id;
		} else {
			return false;
		}
	}

	function add_user_interests($params) {
		$this->db->insert( 'user_interests', $params );
		$id = $this->db->insert_id();
		if ($id) {
			return $id;
		} else {
			return false;
		}
	}

	function update_user_info($params, $userId) {
		$this->db->where( 'userId', $userId );
		$this->db->update( 'user_info', $params );
		return true;
	}

	function update_interests($params, $userId) {
		$this->db->where( 'userId', $userId );
		$this->db->update( 'user_interests', $params );
		return true;
	}

	function add_military_info($params) {
		$this->db->insert( 'user_military', $params );
		$id = $this->db->insert_id();
		if ($id) {
			return $id;
		} else {
			return false;
		}
	}

	function get_military_info($userId) {
		$q = $this->db->get_where( 'user_military', array( 'userId' => $userId ), 1 );
		if ( $this->db->affected_rows() > 0 ) {
			$row = $q->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function update_military($params, $userId) {
		$this->db->where( 'userId', $userId );
		$this->db->update( 'user_military', $params );
		return true;
	}

	function get_user_info($userId) {
		$q = $this->db->get_where( 'user_info', array( 'userId' => $userId ), 1 );
		if ( $this->db->affected_rows() > 0 ) {
			$row = $q->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function get_user_interests($userId) {
		$q = $this->db->get_where( 'user_interests', array( 'userId' => $userId ), 1 );
		if ( $this->db->affected_rows() > 0 ) {
			$row = $q->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function get_payment_details($id) {
		if ($id) {
			$q = $this->db->get_where( 'user_payments', array( 'userId' => $id ), 1 );
			if ( $this->db->affected_rows() > 0 ) {
				$row = $q->row_array();
				return $row;
			} else {
				$params = array(
					'userId' => $id,
					'payment_mode' => 'bank',
					'account_holder_name' => '',
					'account_number' => '',
					'ifsc_code' => '',
					'pan_number' => '',
					'gift_type' => 'amazon',
					'git_email' => '',
					'updated_at' => date('Y-m-d H:m:s')
				);
				$add = $this->add_payment_details($params);
				if ($add) {
					return $this->db->get_where('user_payments', array('userId' => $id))->row_array();
				} else {
					return null;
				}
			}
		} else {
			return null;
		}
	}

	function add_payment_details($params) {
		$this->db->insert( 'user_payments', $params );
		$id = $this->db->insert_id();
		if ($id) {
			return $id;
		} else {
			return false;
		}
	}

	function change_password($params, $id, $type, $token, $user) {
		$this->db->where( 'userId', $id );
		$this->db->update( 'user_password', $params );
		$param['status'] = 1;
		$param['updated_at'] = $params['updated_at'];
		$this->db->where('id', $id);
		$this->db->update('users', $param);
		$this->db->delete( 'tokens', array( 'token' => $token ));
		$params['name'] = $user['name'];
		$message = $this->load->view('emails/password-changed', $params, true);
		$this->load->library('mail'); 
		$data = $this->mail->send_email($user['email'], 'SameCondition', 'Password Changed', $message);
	}

	function update_user_and_profile($user, $profile, $id) {
		$this->db->where( 'id', $id );
		$this->db->update( 'users', $user );
		$this->db->where( 'userId', $id );
		$this->db->update( 'user_profile', $profile );
		return true;
	}

	function activate_account($user, $token) {
		$this->db->where( 'id', $user['id'] );
		$this->db->update( 'users', array('status' => 1) );
		$this->db->delete( 'tokens', array( 'token' => $token ));
		$fields = 'email_type=welcome_email&user_name='.$user['name'].'&user_email='.$user['email'];
		sendEmail($fields);
	}

	function get_staff($id) {
		return $this->db->get_where( 'admins', array( 'id' => $id ))->row_array();
	}

	function remove_staff($id) {
		$this->db->delete( 'admins', array( 'id' => $id ) );
		return true;
	}

	function send_verification_token_email($params) {
		$param = array(
			'token' => random_string(),
			'user' => $params['id'],
			'type' => 'user',
			'created_at' => date( 'Y-m-d H:i:s' ),
			'status' => 1,
			'is_new' => 1
		);
		$user = array(
			'id' => $params['id'],
			'name' => $params['name'],
			'email' => $params['email']
		);
		$isDuplicate = $this->is_duplicate_token($user, $param['type']);
		$this->db->insert( 'tokens', $param );
		$id = $this->db->insert_id();
		$Link = 'https://www.vedak.com/login/account_verify/'.$param['token'];
		$fields = 'email_type=account_verify&user_name='.$user['name'].'&user_email='.$user['email'].'&Link='.$Link;
		sendEmail($fields);
	}

	function create_verification_token($userId) {
		$param = array(
			'token' => random_string(80),
			'userId' => $userId,
			'type' => 'account',
			'created_at' => date( 'Y-m-d H:i:s' ),
			'status' => 1
		);
		$this->db->insert( 'tokens', $param );
		return $param['token'];
	}

	function getAdminInfoByEmail($email){
		$q = $this->db->get_where( 'admins', array( 'email' => $email ), 1 );
		if ( $this->db->affected_rows() > 0 ) {
			$row = $q->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function check_token($token) {
		$q = $this->db->get_where( 'tokens', array( 'token' => $token ), 1 );
		if ( $this->db->affected_rows() > 0 ) {
			$row = $q->row_array();
			return $row;
		} else {
			return false;
		}
	}

	function is_account_verified($userId) {
		$q = $this->db->get_where( 'users', array( 'id' => $userId ), 1 );
		if ($this->db->affected_rows() > 0) {
			$row = $q->row_array();
			if ($row['status'] == '1') {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	function verify_account($params) {
		$this->db->where( 'id', $params['userId'] );
		$this->db->update( 'users', array('status' => 1) );
		$this->db->delete( 'tokens', array( 'userId' => $params['userId'],  'type' => $params['type']));
	}

	function isDuplicate( $email ) {
		$this->db->get_where( 'users', array( 'email' => $email ), 1 );
		return $this->db->affected_rows() > 0 ? TRUE : FALSE;
	}

	function isAdminDuplicate($email) {
		$this->db->get_where( 'admins', array( 'email' => $email ), 1 );
		return $this->db->affected_rows() > 0 ? TRUE : FALSE;
	}

	function create_password_token($params, $user) {
		$isDuplicate = $this->is_duplicate_token($user, $params['type']);
		$this->db->insert( 'tokens', $params );
		$id = $this->db->insert_id();
		$params['link'] = base_url('login/reset_password/'.$params['token']);
		$params['name'] = $user['name'];
		$message = $this->load->view('emails/forgot-password', $params, true);
		$this->load->library('mail'); 
		$data = $this->mail->send_email($user['email'], 'SameCondition', 'Password Reset', $message);
		if ($data['success'] == true) {
			return true;
		} else {
			return false;
		}
	}

	function is_duplicate_token($user, $type) {
		$this->db->delete( 'tokens', array( 'userId' => $user['id'],  'type' => $type));
	}

	function get_token_info($token) {
		return $this->db->get_where( 'tokens', array( 'token' => $token ))->row_array();
	}
}
?>