<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Member extends SAME_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index($member) { 
		$data['member'] = $member;
		$memberData = $this->Staff_Model->getUserInfoByUserName($member);
		if ($memberData) {
			$this->load->view('include/profile-header');
			$this->load->view('member/member_detail', $data);
		} else {
			$this->load->view('include/profile-header');
			$this->load->view('member/member_error', $data);
		}
		
	}
}