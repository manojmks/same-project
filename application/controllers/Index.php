<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Index extends SAME_Controller {

	function __construct() {
		parent::__construct();
		$this->lang->load('english_lang', 'english');
	}

	public function index() {
		//$this->load->view('coming-soon');
		if ($this->session->userdata('LoginOK')) {
			$user = $this->Staff_Model->get_user_details();
			if ($user) {
				$data['profile'] = $user['profile'];
				$data['username'] = $user['username'];
				$data['name'] = $user['name'];
				$this->load->view('include/header');
				$this->load->view('home/index', $data);
			} else {
				$this->load->view('include/header');
				$this->load->view('home/index');
				$this->load->view('include/footer');
			}
		} else {
			$this->load->view('include/header');
			$this->load->view('home/index');
			$this->load->view('include/footer');
		}
	}

	function page_404() {
		$this->load->view('error-404');
	}
}