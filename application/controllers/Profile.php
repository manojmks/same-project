<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Profile extends SAME_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->view('include/header');
		$this->load->view('profile/index');
		$this->load->view('include/footer');
	}

	function get_info() {
		$info = $this->Staff_Model->get_user_info($this->session->userdata('usr_id'));
		if ($info) {
			$return['success'] = true;
			$return['message'] = ($info);
			echo json_encode($return);
		} else {
			$params = array(
				'ethnicity' => null,
				'race' => null,
				'education' => null,
				'insurance' => null,
				'userId' => $this->session->userdata('usr_id'),
				'updated_at' => date('Y-m-d H:i:s')
			);
			$addInfo = $this->Staff_Model->add_user_info($params, $this->session->userdata('usr_id'));
			$info = $this->Staff_Model->get_user_info($this->session->userdata('usr_id'));
			$return['success'] = true;
			$return['message'] = $info;
			echo json_encode($return);
		}
	}

	function update_demographics() {
		$userId = $this->session->userdata('usr_id');
		$params = array(
			'ethnicity' => $this->input->post('ethnicity'),
			'race' => $this->input->post('race'),
			'education' => $this->input->post('education'),
			'insurance' => $this->input->post('insurance'),
			'updated_at' => date('Y-m-d H:i:s')
		);
		$this->Staff_Model->update_user_info($params, $userId);
		$return['success'] = true;
		$return['message'] = 'Data has been saved successfully!';
		echo json_encode($return);
	}

	function update_info() {
		$userId = $this->session->userdata('usr_id');
		$params = $this->input->post();
		$params['updated_at'] = date('Y-m-d H:i:s');
		$this->Staff_Model->update_user_info($params, $userId);
		$return['success'] = true;
		$return['message'] = 'Data has been saved successfully!';
		echo json_encode($return);
	}

	function change_profile() {
		if ( isset( $_POST ) ) {
			$config[ 'upload_path' ] = './assets/img/users/';
			$config[ 'allowed_types' ] = 'jpg|png|jpeg';
			$config['encrypt_name'] = TRUE;
			$config[ 'allowed_types' ] = 'zip|rar|tar|gif|jpg|png|jpeg|gif|pdf|doc|docx|xls|xlsx|txt|csv|ppt|opt';
			$config['max_size'] = '500';
			$new_name = preg_replace("/[^a-z0-9\_\-\.]/i", '', basename($_FILES["file"]['name']));
			if ($_FILES['file']['size']>597152) {
				$this->session->set_flashdata( 'error', 'File size exceeded <br/>Maximum upload file size: 500KB');
				redirect( base_url('profile' ));
			} else {
				$this->load->library( 'upload', $config );
				$config['file'] = $new_name;
				if (!$this->upload->do_upload('file')) {
					$this->session->set_flashdata( 'error', $this->upload->display_errors());
					redirect( base_url('dashboard' ));
				} else {
					$image_data = $this->upload->data();
					$data_upload_files = $this->upload->data();
					$image_data = $this->upload->data();
					$userDetails = $this->Staff_Model->get_user_details();
					if ($userDetails['profile']) {
						unlink('./assets/img/users/'.$userDetails['profile']);
					}
					$this->db->where('id', $this->session->userdata('usr_id'));
					$response = $this->db->update( 'users', array('profile' => $image_data[ 'file_name' ] ) );
					$this->session->set_flashdata( 'success', 'Profile picture has been updated successfully');
					redirect( base_url('dashboard' ));
				}
			}
		}
	}

	function remove_picture() {
		$userDetails = $this->Staff_Model->get_user_details();
		if ($userDetails['profile']) {
			unlink('./assets/img/users/'.$userDetails['profile']);
		}
		$this->db->where( 'id', $this->session->userdata('usr_id') );
		$response = $this->db->update( 'users', array('profile' => null ) );
		$this->session->set_flashdata( 'success', 'Profile picture has been removed successfully');
		redirect( base_url('dashboard' ));
	}

	function get_interests() {
		$info = $this->Staff_Model->get_user_interests($this->session->userdata('usr_id'));
		if ($info) {
			$return['success'] = true;
			$return['message'] = ($info);
			echo json_encode($return);
		} else {
			$params = array(
				'userId' => $this->session->userdata('usr_id'),
				'updated_at' => date('Y-m-d H:i:s')
			);
			$addInfo = $this->Staff_Model->add_user_interests($params, $this->session->userdata('usr_id'));
			$info = $this->Staff_Model->get_user_interests($this->session->userdata('usr_id'));
			$return['success'] = true;
			$return['message'] = $info;
			echo json_encode($return);
		}
	}

	function update_interests() {
		$info = $this->Staff_Model->get_user_interests($this->session->userdata('usr_id'));
		if ($info) {
			$userId = $this->session->userdata('usr_id');
			$params = array(
				'primary_interest' => $this->input->post('primary_interest'),
				'updated_at' => date('Y-m-d H:i:s')
			);
			$this->Staff_Model->update_interests($params, $userId);
			$return['success'] = true;
			$return['message'] = 'Interests has been updated successfully!';
			echo json_encode($return);
		} else {
			$return['success'] = false;
			$return['message'] = 'Something went wrong, please try again or contact us for more help';
			echo json_encode($return);
		}
	}

	function get_military() {
		$userId = $this->session->userdata('usr_id');
		$military = $this->Staff_Model->get_military_info($userId);
		if ($military) {
			$return['success'] = true;
			$return['message'] = $military;
			echo json_encode($return);
		} else {
			$military_params = array(
				'military_updated_at' => date('Y-m-d H:i:s'),
				'userId' => $this->session->userdata('usr_id')
			);
			$this->Staff_Model->add_military_info($military_params);
			$military = $this->Staff_Model->get_military_info($userId);
			$return['success'] = true;
			$return['message'] = $military;
			echo json_encode($return);
		}
	}

	function update_military() {
		$params = $this->input->post();
		$params['military_updated_at'] = date('Y-m-d H:i:s');
		$userId = $this->session->userdata('usr_id');
		$this->Staff_Model->update_military($params, $userId);
		$return['success'] = true;
		$return['message'] = 'Military status has been updated successfully!';
		echo json_encode($return);
	}
}
