<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Started extends SAME_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$user = $this->Staff_Model->get_user_info($this->session->userdata('usr_id'));
		if ($user) {
			redirect(base_url('dashboard'));
		} else {
			$this->load->view('include/header-without-menu');
			$this->load->view('profile/get_started');
		}
	}

	function continue() {
		$params = array(
			'age' => $this->input->post('age'),
			'userId' => $this->session->userdata('usr_id'),
			'country' => $this->input->post('country'),
			'ethnicity' => $this->input->post('ethnicity'),
			'race' => $this->input->post('race'),
			'sex' => $this->input->post('sex'),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		);
		$military_params = array(
			'military_updated_at' => date('Y-m-d H:i:s'),
			'military_status' => $this->input->post('military'),
			'userId' => $this->session->userdata('usr_id'),
		);
		$this->Staff_Model->add_user_info($params);
		$this->Staff_Model->add_military_info($military_params);
		$return['success'] = true;
		$return['message'] = 'Updated Successfully';
		echo json_encode($return);
	}
}