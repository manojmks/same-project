<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Hospitalization extends SAME_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->view('include/header');
		$this->load->view('hospitalization/index');
		$this->load->view('include/footer');
	}

	public function add() {
		$this->load->view('include/header');
		$this->load->view('hospitalization/add');
		$this->load->view('include/footer');
	}

	function get_reasons() {
		$reasons = $this->Hospitalization_Model->get_reasons();
		$return['success'] = true;
		$return['message'] = $reasons;
		echo json_encode($return);
	}

	function add_hospitalization() {
		$params = $this->input->post();
		$params['created_at'] = date('Y-m-d H:i:s');
		$params['updated_at'] = date('Y-m-d H:i:s');
		$params['userId'] = $this->session->userdata('usr_id');
		$added = $this->Hospitalization_Model->add_user_hospitalization($params);
		if ($added) {
			$return['success'] = true;
			$return['message'] = 'Hospitalization added successfully!';
			echo json_encode($return);
		} else {
			$return['success'] = true;
			$return['message'] = 'Error Occured!';
			echo json_encode($return);
		}
	}

	function data() {
		$userId = $this->session->userdata('usr_id');
		$reasons = $this->Hospitalization_Model->get_data($userId);
		$return['success'] = true;
		$return['message'] = $reasons;
		echo json_encode($return);
	}

	function delete($id) {
		$userId = $this->session->userdata('usr_id');
		$hospitalization = $this->Hospitalization_Model->get_hospitalization($id, $userId);
		if ($hospitalization) {
			$this->Hospitalization_Model->delete_hospitalization($id, $userId);
			$this->session->set_flashdata( 'success', 'Hospitalization has been deleted successfully');
			$return['success'] = true;
			$return['message'] = 'Deleted';
			echo json_encode($return);
		} else {
			$return['success'] = false;
			$return['message'] = 'No scuh hospitalization found to delete';
			echo json_encode($return);
		}
	}
}
