<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Treatments extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->view('include/header');
		$this->load->view('treatments/index');
		$this->load->view('include/footer');
	}
}
