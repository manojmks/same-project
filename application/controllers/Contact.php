<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Contact extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->view('include/profile-header');
		$this->load->view('page/contact');
		$this->load->view('include/footer');
	}

	function submit_support() {
		if (isset($_POST) && count($_POST) > 0) {
			if ($this->input->post('email')) {
				$email = $this->input->post('email');
				$email = $this->security->xss_clean($email);
				if (filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $email)) {
					$params = array(
						'email' => $email
					);
					$message = $this->load->view('emails/support_to_admin', $params, true);
					$this->load->library('mail'); //manojswami600@gmail.com 
					$data = $this->mail->send_email('mail@samecondition.com', 'Samecondition Contact', 'New submit from Samecondition', $message);
					$return['success'] = true;
					$return['message'] = 'Email has been sent successfully'; 
					echo json_encode($return);
				} else {
					$return['success'] = false;
					$return['message'] = 'Please enter correct email'; 
					echo json_encode($return);
				}
			} else {
				$return['success'] = false;
				$return['message'] = 'Please provide your email'; 
				echo json_encode($return);
			}
		} else {
			$return['success'] = false;
			$return['message'] = 'Please provide your email'; 
			echo json_encode($return);
		}
	}
}
