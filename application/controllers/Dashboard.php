<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Dashboard extends SAME_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() { 
		$user = $this->Staff_Model->get_user_details();
		if ($user) {
			if ($user['profile']) {
				$data['profile'] = $user['profile'];
			} else {
				$data['profile'] = 'no-user.svg';
			}
			$data['username'] = $user['username'];
			$data['name'] = $user['name'];
			$this->load->view('include/header');
			$this->load->view('dashboard/index', $data);
		}
	}
}
