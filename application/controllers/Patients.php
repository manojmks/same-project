<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Patients extends SAME_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->view('include/header');
		$this->load->view('patients/index');
	}

	function get_patients() {
		$patients = $this->Patients_Model->get_patients();
		$return['success'] = true;
		$return['message'] = $patients;
		echo json_encode($return);
	}
}
