<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Login extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Login_Model');
		$this->load->model('Staff_Model');
	}

	public function index() {
		$this->load->view('admin/login/index');
	}

	public function register($type = 'patient') {
		$data['type'] = $type;
		$this->load->view('login/register', $data);
	}

	public function forgotpassword() {
		$this->load->view('include/header');
		$this->load->view('login/forgot-password');
	}

	public function passwordreset($token='') {
		$this->load->view('include/header');
		$this->load->view('login/change-password');
	}

	function auth() {
		$email = $this->input->post( 'email' );
		$passwords = $this->input->post( 'password' );
		$clean = $this->security->xss_clean( $email );
		$userInfo = $this->Staff_Model->getUserInfoByEmailUserName( $clean );
		if ($userInfo) {
			if ($userInfo['status'] == '1') { 
				$password = $this->Login_Model->get_user_password($userInfo['id']);
				$params = array(
					'name' => $userInfo['name'],
					'username' => $userInfo['username'],
					'email' => $userInfo['email'],
					'upassword' => $password,
					'ipassword' => $this->input->post( 'password' ),
					'type' => $userInfo['type'],
					'userId' => $userInfo['id'],
				); 
				if ($email && $passwords && $this->Login_Model->verify( $params )) {
					$return['success'] = true;
					$return['message'] = 'Login'; 
					echo json_encode($return);
				} else {
					$return['success'] = false; 
					$return['message'] = 'Invalid password, please enter correct password'; 
					echo json_encode($return);
				}
			} else {
				$return['success'] = false; 
				$return['message'] = 'Your account is not verified yet, please signin using Google or use Forget Password to activate your account'; 
				echo json_encode($return);
			}
		} else {
			$return['success'] = false; 
			$return['message'] = 'Please enter correct username or email'; 
			echo json_encode($return);
		}
	}

	function checkusername() {
		if ($this->input->post('username')) {
			$check = $this->Login_Model->checkusername($this->input->post('username'));
			if ($check) {
				$return['success'] = false; 
				$return['message'] = 'This username is already taken'; 
				echo json_encode($return);
			} else {
				$return['success'] = true;
				$return['message'] = 'Valid Username'; 
				echo json_encode($return);
			}
		}
	}

	function google_login() {
		include APPPATH.'../assets/lib/social-login/google-login-api.php';
		try {
			$gapi = new GoogleLoginApi();
			$data = $gapi->GetAccessToken(CLIENT_ID, CLIENT_REDIRECT_URL, CLIENT_SECRET, $_GET['code']);
			$user_info = $gapi->GetUserProfileInfo($data['access_token']);
			$email =$user_info['emails'][0]['value'];
			$name = $name =$user_info['displayName'];
			if ($email) {
				if ( $userInfo = $this->Staff_Model->getUserInfoByEmail( $email ) ) {
					$this->session->set_userdata( array(
						'usr_id' => $userInfo['id'],
						'username' => $userInfo['username'],
						'staffname' => $userInfo['name'],
						'email' => $userInfo['email'],
						'type' => $userInfo['type'],
						'LoginOK' => true
					));
					redirect(base_url('dashboard'));
				} else {
					$params = array(
						'name' => $name,
						'username' => $email,
						'email' => $email,
						'created' => date('Y-m-d H:i:s'),
						'updated' => date('Y-m-d H:i:s'),
					);
					$id = $this->Login_Model->create_social_account( $params );
					if ($id) {
						redirect(base_url('dashboard'));
					} else {
						redirect(base_url('login'));
					}
				}
			}
		} catch(Exception $e) {
			echo $e->getMessage();exit();
		}
	}

	function createAccount() {
		if (isset($_POST) && count($_POST) > 0) {
			$email = $this->input->post( 'email' );
			$clean = $this->security->xss_clean( $email );
			if ( $this->Staff_Model->isDuplicate($clean)) {
				$return['success'] = false; 
				$return['message'] = 'Email is already registered, please signin into your account'; 
				echo json_encode($return);
			} else if ($this->Login_Model->checkusername($this->input->post('username'))) {
				$return['success'] = false; 
				$return['message'] = 'This username is already taken'; 
				echo json_encode($return);
			} else {
				$options = ['cost' => 12,];
				$password = password_hash($this->input->post( 'password' ), PASSWORD_BCRYPT, $options);
				$params = array(
					'name' => $this->input->post( 'name' ),
					'username' => $this->input->post( 'username' ),
					'email' => $this->input->post( 'email' ),
					'created_at' => date( 'Y-m-d H:i:s' ),
					'updated_at' => date( 'Y-m-d H:i:s' ),
					'status' => 0
				);
				$param = array(
					'password' => $password,
					'updated_at' => date( 'Y-m-d H:i:s' ),
				);
				$id = $this->Login_Model->create_account( $params, $param );
				if ($id) {
					$params['id'] = $id;
					$return['success'] = true;
					$return['message'] = 'Your Account has been created successfully!';
					echo json_encode($return);
				}
			}
		}
	}

	function forgot_password() {
		if (isset($_POST) && count($_POST) > 0) {
			$email = $this->input->post('email');
			if (isset($email)) {
				if ( $userInfo = $this->Staff_Model->getUserInfoByEmail( $email ) ) {
					$params = array(
						'token' => random_string(),
						'user' => $userInfo['id'],
						'type' => 'user',
						'created_at' => date( 'Y-m-d H:i:s' ),
						'status' => 1
					);
					$user= array(
						'name' => $userInfo['name'],
						'email' => $userInfo['email'],
						'id' => $userInfo['id']
					);
					$this->Staff_Model->create_password_token($params, $user);
					$return['success'] = true; 
					$return['message'] = 'Password reset link has been sent to your email'; 
					echo json_encode($return);
				} else {
					$return['success'] = false; 
					$return['message'] = 'No such user found with this email, please enter correct email or create your new account'; 
					echo json_encode($return);
				}
			} else {
				$return['success'] = false; 
				$return['message'] = 'Please enter email to receive pasword reset link'; 
				echo json_encode($return);
			}
		}
	}

	function reset_password($token) {
		if (isset($token)) {
			$tokenInfo = $this->Staff_Model->get_token_info($token);
			if ($tokenInfo) {
				$userInfo = $this->Staff_Model->getUserInfoById($tokenInfo['user'], $tokenInfo['type']);
				$return['error'] = null;
				$return['type'] = $tokenInfo['type'];
				$return['name'] = $userInfo['name'];
				$return['token'] = $token;
				$this->load->view( 'header');
				$this->load->view( 'reset_password', $return);
				$this->load->view( 'footer');
			} else {
				$return['error'] = 'Invalid token or Token has expired!'; 
				$this->load->view( 'header');
				$this->load->view( 'reset_password', $return);
				$this->load->view( 'footer');
			}
		} else {
			$return['error'] = 'Invalid token or Token has expired!'; 
			$this->load->view( 'header');
			$this->load->view( 'reset_password', $return);
			$this->load->view( 'footer');
		}
	}

	function account_verify($token) {
		if (isset($token)) {
			$tokenInfo = $this->Staff_Model->get_token_info($token);
			if ($tokenInfo) {
				$userInfo = $this->Staff_Model->getUserInfoById($tokenInfo['user'], $tokenInfo['type']);
				if ($userInfo) {
					if ($tokenInfo['is_new'] == '1') {
						$return['error'] = null;
						$return['type'] = $tokenInfo['type'];
						$return['username'] = $userInfo['username'];
						$return['name'] = $userInfo['name'];
						$return['token'] = $token;
						$user = array(
							'name' => $userInfo['name'],
							'email' => $userInfo['email'],
							'id' => $userInfo['id']
						);
						$this->Staff_Model->activate_account($user, $token);
						$this->load->view( 'header');
						$this->load->view( 'account_verify', $return);
						$this->load->view( 'footer');
					} else {
						$return['error'] = 'Error Occured, please use forgot password to activate your account';
						$return['type'] = $tokenInfo['type'];
						$return['name'] = $userInfo['name'];
						$return['token'] = $token;
						$this->load->view( 'header');
						$this->load->view( 'account_verify', $return);
						$this->load->view( 'footer');
					}
				} else {
					$return['error'] = 'Error Occured, please use forgot password to activate your account';
					$return['type'] = $tokenInfo['type'];
					$return['name'] = $userInfo['name'];
					$return['token'] = $token;
					$this->load->view( 'header');
					$this->load->view( 'account_verify', $return);
					$this->load->view( 'footer');
				}
			} else {
				$return['error'] = 'Invalid token or Token has expired!'; 
				$this->load->view( 'header');
				$this->load->view( 'account_verify', $return);
				$this->load->view( 'footer');
			}
		} else {
			$return['error'] = 'Invalid token or Token has expired!'; 
			$this->load->view( 'header');
			$this->load->view( 'account_verify', $return);
			$this->load->view( 'footer');
		}
	}

	function change_password($token) {
		if (isset($_POST) && count($_POST) > 0) {
			if (isset($token)) {
				$tokenInfo = $this->Staff_Model->get_token_info($token);
				if ($tokenInfo) {
					if (!$this->input->post( 'password' ) || $this->input->post( 'password' ) == '') {
						$data['success'] = false;
						$data['message'] = 'Please enter password';
						echo json_encode($data);
					} else {
						$userInfo = $this->Staff_Model->getUserInfoById($tokenInfo['user'], $tokenInfo['type']);
						$options = ['cost' => 12,];
						$password = password_hash($this->input->post( 'password' ), PASSWORD_BCRYPT, $options);
						$params = array(
							'password' => $password,
							'updated' => date( 'Y-m-d H:i:s' )
						);
						$user = array(
							'name' => $userInfo['name'],
							'email' => $userInfo['email']
						);
						$this->Staff_Model->change_password($params, $tokenInfo['user'], $tokenInfo['type'], $token, $user);
						$data['success'] = true;
						$data['type'] = $tokenInfo['type'];
						$data['message'] = 'Your login password has been changed successfully!';
						echo json_encode($data);
					}
				} else {
					$data['success'] = false;
					$data['message'] = 'Invalid token or token has expired!';
					echo json_encode($data);
				}
			} else {
				$data['success'] = false;
				$data['message'] = 'Invalid token or token has expired!';
				echo json_encode($data);
			}
		} else {
			$data['success'] = false;
			$data['message'] = 'Error Occured!';
			echo json_encode($data);
		}
	}

	function logout() {
		$this->session->sess_destroy();
		redirect(base_url('index'));
	}
}