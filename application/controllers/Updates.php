<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Updates extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->view('include/header');
		$this->load->view('updates/index');
		$this->load->view('include/footer');
	}
}
