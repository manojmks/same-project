<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
header( 'Access-Control-Allow-Origin: *' );
class Api extends Same_Controller {

	function index() {
		echo 'Ciuis RestAPI Service';
	}
	function languages() {
		$languages = $this->Settings_Model->get_languages();
		$lang = array();
		foreach ($languages as $language) {
			$lang[] = array(
				'name' => lang($language['name']),
				'foldername' => $language['foldername'],
				'id' => $language['id'],
				'langcode' => $language['langcode']
			);
		}
		echo json_encode($lang);
	}
}