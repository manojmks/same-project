<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Charts extends SAME_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->view('include/header');
		$this->load->view('charts/index');
		$this->load->view('include/footer');
	}
}