<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Conditions extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->view('include/header');
		$this->load->view('conditions/index');
		$this->load->view('include/footer');
	}
}
