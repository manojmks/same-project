<?php 

function get_timeline($datetime) {
	$today = time();
	$createdday = strtotime( $datetime );
	$datediff = abs( $today - $createdday );
	$difftext = "";
	$years = floor( $datediff / ( 365 * 60 * 60 * 24 ) );
	$months = floor( ( $datediff - $years * 365 * 60 * 60 * 24 ) / ( 30 * 60 * 60 * 24 ) );
	$days = floor( ( $datediff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 ) / ( 60 * 60 * 24 ) );
	$hours = floor( $datediff / 3600 );
	$minutes = floor( $datediff / 60 );
	$seconds = floor( $datediff );
	if ( $difftext == "" ) {
		if ( $years > 1 )
			$difftext = $years . ' years ago';
		elseif ( $years == 1 )
			$difftext = $years . ' year ago';
	}
	if ( $difftext == "" ) {
		if ( $months > 1 )
			$difftext = $months . ' months ago';
		elseif ( $months == 1 )
			$difftext = $months . ' month ago';
	}
	if ( $difftext == "" ) {
		if ( $days > 1 )
			$difftext = $days . ' days ago';
		elseif ( $days == 1 )
			$difftext = $days . ' day ago';
	}
	if ( $difftext == "" ) {
		if ( $hours > 1 )
			$difftext = $hours . ' hours ago';
		elseif ( $hours == 1 )
			$difftext = $hours . ' hour ago';
	}
	if ( $difftext == "" ) {
		if ( $minutes > 1 )
			$difftext = $minutes . ' minutes ago';
		elseif ( $minutes == 1 )
			$difftext = $minutes . ' minute ago';
	}
	if ( $difftext == "" ) {
		if ( $seconds > 1 )
			$difftext = $seconds . ' seconds ago';
		elseif ( $seconds == 1 )
			$difftext = $seconds . ' second ago';
	}
	return $difftext;
}

function random_number() {
	$today = time();
	$min = 1000000;
	$max = 9999999;
	return $today+rand($min, $max)+$min;
}

function random_string($length = 40) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

function encrypt_data($data) { 
	$password = "HiThere@SAMECONDITION@Encryption";
	return openssl_encrypt($data, "AES-128-ECB", $password);
}

function decrypt_data($data) {
	$password = "HiThere@SAMECONDITION@Encryption";
	return openssl_decrypt($data, "AES-128-ECB", $password);
}

function check_admin_privilege() {
	$ci = & get_instance();
	$id = $ci->session->userdata('admin_id');
	if ($id) {
		$data = $ci->db->get_where('admins', array('id' => $id))->row_array();
		if ($data) {
			if ($data['type'] == 'admin') {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	} else {
		return false;
	}
}

function sendEmail($fields) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "url");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	$server_output = curl_exec($ch);
	curl_close ($ch);
}

?>