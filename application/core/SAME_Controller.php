<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SAME_Controller extends CI_Controller {

	public $loggedinuserid;
	protected

	function __construct() {
		parent::__construct();
		$this->load->helper('security');
		$this->load->model('Hospitalization_Model');
		$this->load->model('Staff_Model');
		$this->load->model('Patients_Model');
		$this->load->model('Member_Model');

		if ($this->session->userdata('LoginOK')) {
			$language = $this->session->userdata('language')?$this->session->userdata('language'):'english';
			$this->lang->load($language.'_lang', $language);
		}
		if ($this->session->userdata('LoginOK')) {
			$this->load->model('Login_Model');
		} else {
			//redirect(base_url('login'));
		}
	}
}
