/**
 * @author Manoj Kumar
 * Date: 29-03-2020
 *
 * This file will contain all the MySql changes info-
 */

-- Date  Release Sequence- 

--06-04-2020
DROP TABLE ` users `;
ALTER TABLE `user_password` CHANGE `userId` `userId` VARCHAR(50) NOT NULL;
