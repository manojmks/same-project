## SameCondition

#### Prerequisite

Install following softwares-

1. XAMPP or Laragon
2. MySql
3. Git

#### How to execute project

1. Clone the repository `git clone gitpath` inside of your php directory (eg: xampp/htdoc)
2. Once git is cloned, go to `samecondition`
3. Now go to `application/config/database.php` and go inside the file, and change the database credentials as per your system.
4. Now visit `localhost/samecondition` in your browser


#### How to contribute

1. Create your own branch in repository (follow steps 2 and 3)
2. Go to project `cd samecondition`
3. create a new branch `git checkout -b spider` here 'spider' is branch name. Choose your own. 
4. Take a pull form bitbucket so as to get latest code `git pull origin master`
5. Make changes to your branch i.e. edit files
6. After editing some files you can use `git status` to check status of your current branch
7. Add those changes `git add <filename>` or `git add .` to add all files in one go
8. Commit changes `git commit -m "implemented validation"` Provide exlanatory comment
9. Push changes to your branch `git push`
10. Go to bitbucket and create a merge request from your branch to master branch.
11. Once approved, it will be merged to master


#### Coding standards-

For Javascript development please follow below link-

https://www.w3schools.com/js/js_conventions.asp


#### About Git-
add .
git commit -m "message"
git push origin kavya


And before staring any work, just make sure you have the latest code.
You can take a pull from master branch:
git pull origin master (run this command everytime before using above commands)

then go to bitbucket, and check if there is any open pull-request,
if not then create a new pull request and add me as reviewer.

In pull request you can review your own code, if anything is wrong,
then make those changes and push your changed files again using the above commands.

Once you're confident about your code, just inform me,
I'll review it and merge it with master branch.