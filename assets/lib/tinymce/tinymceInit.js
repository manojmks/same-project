tinymce.init({ 
    selector:'textarea',
    theme: 'modern',
    //height: 200,
    editor_selector : "mceEditor",
    theme: 'modern',
    valid_elements : '*',
    valid_styles: '*', 
    plugins: 'print preview searchreplace autoresize autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking importcss anchor code insertdatetime advlist lists textcolor wordcount imagetools  contextmenu colorpicker textpattern',
    //body_class: 'my_class',
    valid_children : "+body[style]",
    valid_elements: "@[id|class|title|style],"
+ "a[name|href|target|title|alt],"
+ "#p,-ol,,div,h1,h2,h3,h4,h5,h6,strong,-ul,-li,br,img[src|unselectable],-sub,-sup,-b,-i,-u,"
+ "-span[data-mce-type],hr",

valid_child_elements : "body[p,ol,ul,div,h1,h2,h3,h4,h5,h6,strong,b]"
+ ",p[a|span|b|i|u|sup|sub|img|hr|#text]"
+ ",span[a|b|i|u|sup|sub|img|#text]"
+ ",a[span|b|i|u|sup|sub|img|#text]"
+ ",b[span|a|i|u|sup|sub|img|#text]"
+ ",i[span|a|b|u|sup|sub|img|#text]"
+ ",sup[span|a|i|b|u|sub|img|#text]"
+ ",sub[span|a|i|b|u|sup|img|#text]"
+ ",li[span|a|b|i|u|sup|sub|img|ol|ul|#text]"
+ ",ol[li]"
+ ",ul[li]",
    content_css: [
    //fonts.googleapis.com/css?family=Lato:300,300i,400,400i’,
    //www.tinymce.com/css/codepen.min.css’
    ],
    toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat'
  });
