(function ($) {
    "use strict"; // Start of use strict
    $("#contactForm").validator().on("submit", function (event) {
        if (event.isDefaultPrevented()) {
            formError();
            submitMSG(false, "Please fill all the required fields.");
        } else {
            event.preventDefault();
            submitForm();
        }
    });


    function submitForm(){
        var name = $("#name").val();
        var email = $("#email").val();
        var msg_subject = $("#msg_subject").val();
        var message = $("#message").val();
        $("#SubmitButton").text('Please wait...');
        $("#SubmitButton").addClass('disabled');
        $("#SubmitButton").prop('disabled', true);
        submitMSG(false, '');

        $.ajax({
            type: "POST",
            url: BASE_URL+'contact/submit_support',
            dataType: "json",
            data: "name=" + name + "&email=" + email + "&msg_subject=" + msg_subject + "&message=" + message,
            success : function(text) {
                if (text.success) {
                    $("#SubmitButton").text('Submit');
                    formSuccess();
                } else {
                    $("#SubmitButton").text('Submit');
                    formError();
                    submitMSG(false, text.message);
                }
                $("#SubmitButton").removeClass('disabled');
                $("#SubmitButton").prop('disabled', false);
            }
        });
    }

    function formSuccess(){
        $("#contactForm")[0].reset();
        submitMSG(true, "Message Submitted!")
    }

    function formError(){
        $("#contactForm").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            $(this).removeClass();
        });
    }

    function submitMSG(valid, msg){
        if(valid){
            var msgClasses = "h4 text-left tada animated text-success";
        } else {
            var msgClasses = "h4 text-left text-danger";
        }
        $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
    }
}(jQuery));