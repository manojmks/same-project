/**
* @author Manoj Kumar<manojswami600@gmail.com>
* Date: 10-04-2020
*/
'use strict';
var loadingInterests = true;
var loadingInfo = true;
var loadingMilitary = true;
var military_statuses = ["", "I haven't served in the military", "I am currently serving", "I previously served", "I prefer not to answer"]
$(document).ready(function() {
	importCountries();
	getInterests();
	getInfo();
	getMilitary();
	$('.loading').css('background', 'transparent');
});

function importCountries() {
	$('#countires').empty();
	$('#countires').append('<option value="" selected="" disabled="">Select Country</option>')
	for (var i = 0; i < countries.length; i++) {
		$('#countires').append('<option value="'+countries[i].name+'">'+countries[i].name+'</option>');
	}
}

function getInterests() {
	loadingInterests = true;
	globals.ajaxService.request({url: 'profile/get_interests', method: 'get'}, function successCallback(data) {
		loadingInterests = false;
		displayInterests(data);
	}, function errorCallback(error) {
		globals.showToastMessage('Error', error, 'error');
	});
}

function displayInterests(data) {
	$('.primary_interest').html(data.primary_interest?data.primary_interest:'<a class="text-primary cursor" data-toggle="modal" data-target="#addInterest">Click to Add</a>');
	$('.secondary_interest').html(data.secondary_interest?data.secondary_interest:'<a class="text-primary cursor" data-toggle="modal" data-target="#addInterest">Click to Add</a>');
	$('select[name="primary_interest"]').val(data.primary_interest);
}

function getMilitary() {
	loadingInterests = true;
	globals.ajaxService.request({url: 'profile/get_military', method: 'get'}, function successCallback(data) {
		loadingInterests = false;
		displayMilitary(data);
	}, function errorCallback(error) {
		globals.showToastMessage('Error', error, 'error');
	});
}

function displayMilitary(data) {
	$('.military_status').html(data.military_status?military_statuses[data.military_status]:'<a class="text-primary cursor" data-toggle="modal" data-target="#addMilitary">Click to Add</a>');
	$('select[name="military_status"]').val(data.military_status);
	$('.military_branch').text(data.military_branch);
	$('select[name="military_status"]').val(data.military_status);
	$('select[name="military_branch"]').val(data.military_branch);
	$('.military_branch').text(data.military_branch);
	if (data.military_branch) {
		$('.military_branch_show').removeClass('none');
	} else {
		$('.military_branch_show').addClass('none');
	}
	$('.military_rank').text(data.military_rank);
	$('select[name="military_rank"]').val(data.military_rank);
	if (data.military_rank) {
		$('.military_rank_show').removeClass('none');
	} else {
		$('.military_rank_show').addClass('none');
	}
	$('.military_job').text(data.military_job);
	$('select[name="military_job"]').val(data.military_job);
	if (data.military_job) {
		$('.military_job_show').removeClass('none');
	} else {
		$('.military_job_show').addClass('none');
	}
	$('.military_zone').text(data.military_zone);
	$('select[name="military_zone"]').val(data.military_zone);
	if (data.military_zone) {
		$('.military_zone_show').removeClass('none');
	} else {
		$('.military_zone_show').addClass('none');
	}
	$('.military_health').text(data.military_health);
	$('select[name="military_health"]').val(data.military_health);
	if (data.military_health) {
		$('.military_health_show').removeClass('none');
	} else {
		$('.military_health_show').addClass('none');
	}
	if (data.military_status == 1 || data.military_status == "") {
		$('select[name="military_branch"]').prop('disabled', true);
		$('select[name="military_rank"]').prop('disabled', true);
		$('select[name="military_job"]').prop('disabled', true);
		$('select[name="military_zone"]').prop('disabled', true);
		$('select[name="military_health"]').prop('disabled', true);
	} else {
		$('select[name="military_branch"]').prop('disabled', false);
		$('select[name="military_rank"]').prop('disabled', false);
		$('select[name="military_job"]').prop('disabled', false);
		$('select[name="military_zone"]').prop('disabled', false);
		$('select[name="military_health"]').prop('disabled', false);
	}
}

$('select[name="military_status"]').on('change', () => {
	var status = $('select[name="military_status"]').val();
	if ((status == '1') || (status == "")) {
		$('select[name="military_branch"]').prop('disabled', true);
		$('select[name="military_rank"]').prop('disabled', true);
		$('select[name="military_job"]').prop('disabled', true);
		$('select[name="military_zone"]').prop('disabled', true);
		$('select[name="military_health"]').prop('disabled', true);
	} else {
		$('select[name="military_branch"]').prop('disabled', false);
		$('select[name="military_rank"]').prop('disabled', false);
		$('select[name="military_job"]').prop('disabled', false);
		$('select[name="military_zone"]').prop('disabled', false);
		$('select[name="military_health"]').prop('disabled', false);
	}
})

function getInfo() {
	loadingInfo = true;
	globals.ajaxService.request({url: 'profile/get_info', method: 'get'}, function successCallback(data) {
		loadingInfo = false;
		displayInfo(data);
	}, function errorCallback(error) {
		globals.showToastMessage('Error', error, 'error');
	});
}

function displayInfo(data) {
	$('.brief_bio').html(data.brief_bio?data.brief_bio:'<p class="text-primary cursor text-center" data-toggle="modal" data-target="#addBio">Click to Add</p>');
	$('textarea[name="brief_bio"]').val(data.brief_bio);
}

$('.saveDemographics').on('click', () => {
	let params = {
		education: $('select[name="education"]').val(),
		insurance: $('select[name="insurance"]').val(),
		ethnicity: $('select[name="ethnicity"]').val(),
		race: $('select[name="race"]').val(),
		method: 'post',
		url: 'profile/update_demographics'
	};
	$('.saveDemographics').addClass('disabled');
	$('.saveDemographics').prop('disabled', false);
	$('.saveDemographics').text('Saving...');
	globals.ajaxService.request(params, function successCallback(data) {
		$('.saveDemographics').removeClass('loading');
		$('.saveDemographics').removeClass('disabled');
		$('.saveDemographics').text('Save');
		getInfo();
		$('#addMoreDemo').modal('hide');
		globals.showToastMessage('Success', data, 'success');
	}, function errorCallback(error) {
		$('.saveDemographics').text('Save');
		$('.saveDemographics').removeClass('loading');
		$('.saveDemographics').removeClass('disabled');
		globals.showToastMessage('Error', error, 'error');
		$('.saveDemographics').prop('disabled', false);
	});
});

$('.saveInterests').on('click', () => {
	let params = {
		primary_interest: $('select[name="primary_interest"]').val(),
		method: 'post',
		url: 'profile/update_interests'
	};
	$('.saveInterests').addClass('disabled');
	$('.saveInterests').prop('disabled', false);
	$('.saveInterests').text('Saving...');
	globals.ajaxService.request(params, function successCallback(data) {
		$('.saveInterests').removeClass('loading');
		$('.saveInterests').removeClass('disabled');
		$('.saveInterests').text('Save');
		getInterests();
		$('#addInterest').modal('hide');
		globals.showToastMessage('Success', data, 'success');
	}, function errorCallback(error) {
		$('.saveInterests').text('Save');
		$('.saveInterests').removeClass('loading');
		$('.saveInterests').removeClass('disabled');
		globals.showToastMessage('Error', error, 'error');
		$('.saveInterests').prop('disabled', false);
	});
});

$('.saveInfoBio').on('click', () => {
	let params = {
		brief_bio: $('textarea[name="brief_bio"]').val(),
		method: 'post',
		url: 'profile/update_info'
	};
	$('.saveInfoBio').addClass('disabled');
	$('.saveInfoBio').prop('disabled', false);
	$('.saveInfoBio').text('Saving...');
	globals.ajaxService.request(params, function successCallback(data) {
		$('.saveInfoBio').removeClass('loading');
		$('.saveInfoBio').removeClass('disabled');
		$('.saveInfoBio').text('Save');
		getInfo();
		$('#addBio').modal('hide');
		globals.showToastMessage('Success', data, 'success');
	}, function errorCallback(error) {
		$('.saveInfoBio').text('Save');
		$('.saveInfoBio').removeClass('loading');
		$('.saveInfoBio').removeClass('disabled');
		globals.showToastMessage('Error', error, 'error');
		$('.saveInfoBio').prop('disabled', false);
	});
});

$('.saveMilitary').on('click', () => {
	let params = {
		military_status: $('select[name="military_status"]').val(),
		military_branch: $('select[name="military_branch"]').val(),
		military_rank: $('select[name="military_rank"]').val(),
		military_job: $('select[name="military_job"]').val(),
		military_zone: $('select[name="military_zone"]').val(),
		military_health: $('select[name="military_health"]').val(),
		method: 'post',
		url: 'profile/update_military'
	};
	$('.saveMilitary').addClass('disabled');
	$('.saveMilitary').prop('disabled', false);
	$('.saveMilitary').text('Saving...');
	globals.ajaxService.request(params, function successCallback(data) {
		$('.saveMilitary').removeClass('loading');
		$('.saveMilitary').removeClass('disabled');
		$('.saveMilitary').text('Save');
		getMilitary();
		$('#addMilitary').modal('hide');
		globals.showToastMessage('Success', data, 'success');
	}, function errorCallback(error) {
		$('.saveMilitary').text('Save');
		$('.saveMilitary').removeClass('loading');
		$('.saveMilitary').removeClass('disabled');
		globals.showToastMessage('Error', error, 'error');
		$('.saveMilitary').prop('disabled', false);
	});
});

$('.profileRemove').on('click', () => {
	$('.profileRemove').text('Removing...');
	window.location.href = BASE_URL+'profile/remove_picture';
});

$('.changeProfile').on('click', () => {
	if ($('input[name="file"]').val()) {
		$('.changeProfile').addClass('disabled');
		$('.changeProfile').text('uploading');
	}
});