/**
* @author Manoj Kumar<manojswami600@gmail.com>
* Date: 10-04-2020
*/
'use strict';

$(document).ready(function() {
	importCountries();
});

function importCountries() {
	$('#countires').empty();
	$('#countires').append('<option value="" selected="" disabled="">Select Country</option>')
	for (var i = 0; i < countries.length; i++) {
		$('#countires').append('<option value="'+countries[i].name+'">'+countries[i].name+'</option>');
	}
}

$('.continueButton').on('click', () => {
	let params = {
		age: $('input[name="age"]').val(),
		country: $('select[name="country"]').val(),
		military: $('select[name="military"]').val(),
		ethnicity: $('select[name="ethnicity"]').val(),
		race: $('select[name="race"]').val(),
		sex: $('input[name="sex"]:checked').val(),
		method: 'post',
		url: 'started/continue'
	};
	$('.continueButton').addClass('disabled');
	$('.continueButton').prop('disabled', false);
	$('.continueButton').text('Please wait...');
	globals.ajaxService.request(params, function successCallback(data) {
		$('.continueButton').removeClass('loading');
		$('.continueButton').removeClass('disabled');
		$('.continueButton').text('Please wait...');
		setTimeout(() => { 
			window.location.href = BASE_URL+'dashboard';
		}, 1000);
	}, function errorCallback(error) {
		$('.continueButton').text('Continue');
		$('.continueButton').removeClass('loading');
		$('.continueButton').removeClass('disabled');
		globals.showToastMessage('Error', error, 'error');
		$('.continueButton').prop('disabled', false);
	});
});