/**
* @author Manoj Kumar<manojswami600@gmail.com>
* Date: 10-04-2020
*/
'use strict';

$(document).ready(function() {
	getPatients();
	$('.loading').css('background', 'transparent');
});

function getPatients() {
	globals.ajaxService.request({url: 'patients/get_patients', method: 'get'}, function successCallback(data) {
		displayPatients(data);
		$('.totalMembersMessage').text('Showing 0 to 10 of '+data.length+' public patients');
		$('.totalMembers').text(data.length);
	}, function errorCallback(error) {
		globals.showToastMessage('Error', error, 'error');
	});
}

function displayPatients(data) {
	$('.patientsList tbody').empty();
	for (var i = 0; i < data.length; i++) {
		let user = data[i];
		let row = '<tr>';
		let profile = BASE_URL+'assets/img/users/'+(user.profile?user.profile:'no-user.svg');
		let link = BASE_URL+'member/'+user.username;
		row += '<td><p class="text-muted">Updated 3 minutes ago</p><img src="'
		+profile+'" class="rounded-circle" alt="Profile image" width="80" height="70"><a href="'+link+'">'+user.name+'</a></td>';
		row += '<td><a href="#" class="text-muted">Condition1</a><a href="#" class="text-muted">Condition2</a><a href="#" class="text-muted">Condition3</a><p class="text-primary"><a href="'+link+'">Show more</a></p></td>';
		row += '</tr>';
		$('.patientsList tbody').append(row);
	}
}