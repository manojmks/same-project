/**
* @author Manoj Kumar<manojswami600@gmail.com>
* Date: 19 November, 2019
*/
'use strict';
$(document).ready(function() {
	$('#resetPassword').on('click', function() {
		var password = $('input[name="password"]').val();
		var cpassword = $('input[name="cpassword"]').val();
		var hasError = false;
		if (password == '') {
			hasError = true;
			$('.errorMessage').text('Please enter new password');
		} else if (password.length < 8) {
			hasError = true;
			$('.errorMessage').text('Password must be at-least 8 digit');
		} else if (password != cpassword) {
			hasError = true;
			$('.errorMessage').text('Please confirm your password');
		}
		if ( !hasError ) {
			$('#resetPassword').text('Please wait...');
			$('#resetPassword').addClass('loading disabled');
			$('#resetPassword').prop('disabled', true);
			$('.errorMessage').text('');
			$.ajax({
				type: 'POST',
				url: BASE_URL+'login/change_password/'+Token,
				dataType: "json",
				data:({ password : password, cpassword:  cpassword}),
				success: function ( data ) {
					if (data.success) {
						$('.password_reset').addClass('none');
						$('.alert-success').css('display', 'block');
						globals.showToastMessage('Success', data.message, 'success');
					} else {
						$('#cpasswordError').text(data.message);
						$('#forgotEmailButton').text('Submit');
						$('#forgotEmailButton').prop('disabled', false);
						$('#forgotEmailButton').removeClass('disabled');
						globals.showToastMessage('Error', data.message, 'error');
					}
				}
			});
		}
	});
});