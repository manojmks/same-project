/**
* @author Manoj Kumar<manojswami600@gmail.com>
* Date: 31-03-2020
*/
'use strict';

$('input[name="email"]').on('change', () => {
	var email = $('input[name="email"]').val();
	if (email) {
		$('input[name="email"]').val(email.replace(/\s/g,''));
	}
});

(function() {
	'use strict';
	window.addEventListener('load', function() {
		var forms = document.getElementsByClassName('needs-validation');
		var validation = Array.prototype.filter.call(forms, function(form) {
			form.addEventListener('submit', function(event) {
				if (form.checkValidity() === false) {
					event.preventDefault();
					event.stopPropagation();
				} else {
					event.preventDefault();
					signup();
				}
				form.classList.add('was-validated');
			}, false);
		});
	}, false);
})();

function signup() {
	var name = $('input[name="name"]').val();
	var username = $('input[name="username"]').val();
	var email = $('input[name="email"]').val();
	var password = $('input[name="password"]').val();
	var cpassword = $('input[name="cpassword"]').val();
	var hasError = false;
	$('.errorMessage').text('');
	if (name == '') {
		hasError = true;
		globals.showToastMessage('Error', 'Please provide your name', 'error');
	} else if (username == '') {
		hasError = true;
		globals.showToastMessage('Error', 'Please enter username', 'error');
	} else if (email == '') {
		hasError = true;
		globals.showToastMessage('Error', 'Please provide your email', 'error');
	} else if (!globals.validateEmail(email)) {
		hasError = true;
		globals.showToastMessage('Error', 'Please enter correct email', 'error');
	}
	if (password == '') {
		hasError = true;
		globals.showToastMessage('Error', 'Please enter your password', 'error');
	} else if (password.length < 6) {
		hasError = true;
		globals.showToastMessage('Error', 'Password must be atleast 6 digit', 'error');
	} else if (password != cpassword) {
		hasError = true;
		globals.showToastMessage('Error', "Confirm password didn't match", 'error');
	}
	var regexp = new RegExp(/^[a-zA-Z0-9]*$/);
	if (!regexp.test(username)) {
		hasError = true;
		$('.errorMessage').text('Sorry, only letters (a-z) and numbers (0-9) are allowed. Try a different Username.');
		globals.showToastMessage('Error', 'Sorry, only letters (a-z) and numbers (0-9) are allowed. Try a different Username.', 'error', 8000);
	}
	if ( !hasError ) {
		$('#Signup').addClass('loading disabled');
		$('#Signup').prop('disabled', true);
		$('#Signup').text('Please wait...');
		let params = {
			name: $('input[name="name"]').val(),
			username: $('input[name="username"]').val(),
			email: $('input[name="email"]').val(),
			password: $('input[name="password"]').val(),
			cpassword: $('input[name="cpassword"]').val(),
			type: TYPE,
			method: 'post',
			url: 'login/createAccount'
		}
		globals.ajaxService.request(params, function successCallback(data) {
			$('#Signup').removeClass('loading');
			$('#Signup').removeClass('disabled');
			globals.showToastMessage('Success', data, 'success');
			setTimeout(() => {
				window.location.href = BASE_URL+'started';
			}, 500);
		}, function errorCallback(error) {
			$('#Signup').text('Signup');
			$('#Signup').removeClass('loading');
			$('#Signup').removeClass('disabled');
			globals.showToastMessage('Error', error, 'error');
			$('#Signup').prop('disabled', false);
		});
	}
}

$('input[name="forgotEmail"]').on('change', () => {
	var email = $('input[name="forgotEmail"]').val();
	if (email) {
		$('input[name="forgotEmail"]').val(email.replace(/\s/g,''));
	}
});

$('#forgotEmailButton').on('click', function() {
	var email = $('input[data-field="forgotEmail"]').val();
	var hasError = false;
	$('#forgotEmailError').text('');
	if (email == '') {
		hasError = true;
		$('#forgotEmailError').text('Please provide your email');
		$('input[data-field="forgotEmail"]').addClass('error');
	} else if (!globals.validateEmail(email)) {
		hasError = true;
		$('#forgotEmailError').text('Please enter correct email');
		$('input[data-field="forgotEmail"]').addClass('error');
	}
	if (!hasError) {
		$('input').removeClass('error');
		$.ajax({
			type: 'POST',
			url: BASE_URL+'login/forgot_password',
			dataType: "json",
			data:({"email":email}),
			success: function (data) {
				if (data.success) {
					$('#passwordSent').css('display', 'block');
					$('input[data-field="forgotEmail"]').val('');
				} else {
					$('#forgotEmailError').text(data.message);
					globals.bottomMessage('Error', data.message, 'error');
				}
				console.log(data);
			}
		});
	}
});

$('.forgotButton').on('click', function() {
	$('.loginSection').css('display', 'none');
	$('.forgotSection').css('display', 'block');
});
$('.moveToLogin').on('click', function() {
	$('.loginSection').css('display', 'block');
	$('.forgotSection').css('display', 'none');
});