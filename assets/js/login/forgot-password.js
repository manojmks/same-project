/**
* @author Manoj Kumar<manojswami600@gmail.com>
* Date: 31-03-2020
*/
'use strict';

$('input[name="email"]').on('change', () => {
	var email = $('input[name="email"]').val();
	if (email) {
		$('input[name="email"]').val(email.replace(/\s/g,''));
	}
});

$('#forgotEmailButton').on('click', function() {
	var email = $('input[name="email"]').val();
	var hasError = false;
	$('.errorMessage').text('');
	if (email == '') {
		hasError = true;
		$('.errorMessage').text('Please provide your email');
	} else if (!globals.validateEmail(email)) {
		hasError = true;
		$('.errorMessage').text('Please enter correct email');
	}
	if (!hasError) {
		$('#forgotEmailButton').text('Please wait...');
		$('#forgotEmailButton').addClass('loading disabled');
		$('#forgotEmailButton').prop('disabled', true);
		$('.errorMessage').text('');
		$.ajax({
			type: 'POST',
			url: BASE_URL+'login/forgot_password',
			dataType: "json",
			data:({"email": email}),
			success: function (data) {
				if (data.success) {
					$('.alert-success').css('display', 'block');
					globals.showToastMessage('Success', data.message, 'success');
					$('.errorMessage').text('');
					$('#forgotEmailButton').text('Submit');
					$('input[name="email"]').val('');
					$('#forgotEmailButton').prop('disabled', false);
					$('#forgotEmailButton').removeClass('disabled');
				} else {
					$('.errorMessage').text(data.message);
					globals.showToastMessage('Error', data.message, 'error');
					$('#forgotEmailButton').text('Submit');
					$('#forgotEmailButton').prop('disabled', false);
					$('#forgotEmailButton').removeClass('disabled');
				}
			}
		});
	}
});