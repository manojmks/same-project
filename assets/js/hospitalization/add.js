/**
* @author Manoj Kumar<manojswami600@gmail.com>
* Date: 10-04-2020
*/
'use strict';
var loadingInterests = true;
var loadingInfo = true;
var loadingMilitary = true;
$(document).ready(function() {
	getHospitalizationReasons();
	$('.loading').css('background', 'transparent');
});

function getHospitalizationReasons() {
	loadingInterests = true;
	globals.ajaxService.request({url: 'hospitalization/get_reasons', method: 'get'}, function successCallback(data) {
		loadingInterests = false;
		displayReasons(data);
	}, function errorCallback(error) {
		globals.showToastMessage('Error', error, 'error');
	});
}

function displayReasons(data) {
	$('select[name="hospitalization_reason_id"]').empty();
	for (var i = 0; i < data.length; i++) {
		$('select[name="hospitalization_reason_id"]').append('<option value="'+data[i].id+'">'+data[i].reason+'</option>')
	}
}

$('.addHospitalization').on('click', () => {
	let params = {
		hospitalization_reason_id: $('select[name="hospitalization_reason_id"]').val(),
		admitted_year: $('select[name="admitted_year"]').val(),
		admitted_month: $('select[name="admitted_month"]').val(),
		admitted_date: $('select[name="admitted_date"]').val(),
		is_discharged: ($('input[name="is_discharged"]:checked').val() == 'yes'?'1':'0'),
		discharged_year: $('select[name="discharged_year"]').val(),
		discharged_month: $('select[name="discharged_month"]').val(),
		discharged_date: $('select[name="discharged_date"]').val(),
		method: 'post',
		url: 'hospitalization/add_hospitalization'
	};
	if (!params.admitted_year && !params.admitted_month && !params.admitted_date) {
		globals.showToastMessage('Error', 'Please provide details about the admitted date or year or month', 'error');
	} else {
		$('.addHospitalization').addClass('disabled');
		$('.addHospitalization').prop('disabled', false);
		$('.addHospitalization').text('Saving...');
		globals.ajaxService.request(params, function successCallback(data) {
			globals.showToastMessage('Success', data, 'success');
			setTimeout(() => {
				window.location.href = BASE_URL+'hospitalization';
			}, 500);
		}, function errorCallback(error) {
			$('.addHospitalization').text('Save this Hospitalization');
			$('.addHospitalization').removeClass('loading');
			$('.addHospitalization').removeClass('disabled');
			globals.showToastMessage('Error', error, 'error');
			$('.addHospitalization').prop('disabled', false);
		});
	}
});

$('input[name="is_discharged"]').on('change', () => {
	let value = $('input[name="is_discharged"]:checked').val();
	if (value == 'no') {
		$('.is_discharged').addClass('none');
	} else {
		$('.is_discharged').removeClass('none');
	}
})