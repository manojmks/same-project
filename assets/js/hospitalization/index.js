/**
* @author Manoj Kumar<manojswami600@gmail.com>
* Date: 10-04-2020
*/

const months = ["" ,"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

'use strict';
$(document).ready(function() {
	getHospitalizations();
	$('.loading').css('background', 'transparent');
});

function getHospitalizations() {
	globals.ajaxService.request({url: 'hospitalization/data', method: 'get'}, function successCallback(data) {
		displayHospitalizations(data);
		if (data.length < 1) {
			window.location.href = BASE_URL+'hospitalization/add';
		}
	}, function errorCallback(error) {
		globals.showToastMessage('Error', error, 'error');
	});
}

function displayHospitalizations(data) {
	$('.hospitalizations tbody').empty();
	for (var i = 0; i < data.length; i++) {
		let hospitalization = data[i];
		let row = '<tr>';
		let date = '';
		if (hospitalization.admitted_date) {
			date = hospitalization.admitted_date;
		}
		if (date && hospitalization.admitted_month) {
			date += '-'+months[parseInt(hospitalization.admitted_month)];
		} else if (hospitalization.admitted_month) {
			date = months[parseInt(hospitalization.admitted_month)];
		}
		if (date && hospitalization.admitted_year) {
			date += '-'+hospitalization.admitted_year;
		} else if (hospitalization.admitted_year) {
			date = hospitalization.admitted_year;
		}
		row += '<td>'+date+'</td><td>'+hospitalization.reason+'</td><td><i class="fas fa-edit text-primary cursor mr-2" title="Edit"></i>'
		+'<i class="fas fa-trash-alt text-danger cursor" title="Delete" onclick="deleteHospitalization('+hospitalization.id+')"></i></td></tr>';
		$('.hospitalizations tbody').append(row);
	}
}

function deleteHospitalization(id) {
	var x = confirm("Are you sure you want to delete?");
	if (x) {
		globals.ajaxService.request({url: 'hospitalization/delete/'+id, method: 'get'}, function successCallback(data) {
			getHospitalizations(data);
			globals.showToastMessage('Success', 'Hospitalization has been deleted successfully', 'success');
		}, function errorCallback(error) {
			globals.showToastMessage('Error', error, 'error');
		});
	}
}