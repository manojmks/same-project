/**
* @author Manoj Kumar<manojswami600@gmail.com>
* Date: 29-03-2020
*/
'use strict';
var globals = {};
function  makeAjaxPostRequest(url, params, successCallback, failureCallback) {
    let method = (params.method == 'get')?'get':'post';
    delete params.method;
    delete params.url;
    $.ajax({
        url: url,
        method: method,
        data: params,
        dataType: 'json',
        success: function(response) {
            if (response.success) {
                if (response.message) {
                    successCallback(response.message);
                } else {
                    successCallback(response.result);
                }
            } else {
                failureCallback(response.message);
            }
        },
        error: globals.ajaxErrorHandler
    });
}

globals.ajaxErrorHandler = function(jqXHR, exception) {
    if (jqXHR.status === 0) {
        globals.showToastMessage('Error', 'Not connect.\n Verify Network.', 'error');
    } else if (jqXHR.status == 404) {
        globals.showToastMessage('Error', 'Requested page not found. [404]', 'error');
    } else if (jqXHR.status == 500) {
        globals.showToastMessage('Error', 'Internal Server Error [500].', 'error');
    } else if (exception === 'parsererror') {
        globals.showToastMessage('Error', 'Requested JSON parse failed.', 'error');
    } else if (exception === 'timeout') {
        globals.showToastMessage('Error', 'Time out error.', 'error');
    } else if (exception === 'abort') {
        globals.showToastMessage('Error', 'Ajax request aborted.', 'error');
    } else {
        globals.showToastMessage('Error', 'Uncaught Error.\n' + jqXHR.responseText, 'error');
    }
};

globals.showToastMessage = function(heading, message, icon, time = 6000) {
    $.toast({
        showHideTransition : 'slide',
        heading: heading,
        text: message,
        position : 'bottom-left',
        icon: icon,
        hideAfter : time
    });
};

globals.validateEmail = function(email) {
    var emailFormat = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z]{2,4})+$/;
    return emailFormat.test( email );
},

globals.validatePhone = function(phone) {
    var re = /^[6-9]\d{9}$/;
    return re.test(phone);
},

globals.formatDate = function(date, separator) {
    if (date) {
        var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
        if (month.length < 2) {
            month = '0' + month;
        }
        if (day.length < 2) {
            day = '0' + day;
        }
        if (separator == '/') {
            return [day, month, year].join(separator);
        }
        if (separator == '-') {
            return [year, month, day].join(separator);
        }
        if (separator == ',') {
            return [day, month, year].join('-');
        }
    } else {
        return '';
    }
},

globals.timeOnly = function(date) {
    if (date) {
        var now= new Date(date),
        ampm = 'AM',
        h = now.getHours(),
        m = now.getMinutes(),
        s = now.getSeconds();
        if( h >= 12 ){
            if( h > 12 ) h -= 12;
            ampm= 'PM';
        }
        if( m < 10 ) m = '0' + m;
        if( s < 10) s = '0' + s;
        return h + ':' + m + ' ' + ampm;
    } else {
        return '';
    }
},

globals.stringLength = function(string, setLength) {
    if (string) {
        if (string.length > setLength) {
            return string.substring(0, setLength) + "...";
        } else {
            return string;
        }
    } else {
        return '';
    }
}

globals.dateDiff = function(date1, date2) {
    date1 = new Date(date1);
    if (date2) {
        date2 = new Date(date2);
    } else {
        date2 = new Date();
    }
    var time = 60*60*24*1000;
    return  Math.round((date2 - date1) / time);
}

globals.ajaxService = {
    request: function(params, successCallback, failureCallback) {
        var url = BASE_URL+params.url;
        makeAjaxPostRequest(url, params, successCallback, failureCallback);
    },
}