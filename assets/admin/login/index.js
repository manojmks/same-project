/**
* @author Manoj Kumar<manojswami600@gmail.com>
* Date: 31-03-2020
*/
'use strict';

$('input[name="email"]').on('change', () => {
	var email = $('input[name="email"]').val();
	if (email) {
		$('input[name="email"]').val(email.replace(/\s/g,''));
	}
});

(function() {
	'use strict';
	window.addEventListener('load', function() {
		var forms = document.getElementsByClassName('needs-validation');
		var validation = Array.prototype.filter.call(forms, function(form) {
			form.addEventListener('submit', function(event) {
				if (form.checkValidity() === false) {
					event.preventDefault();
					event.stopPropagation();
				} else {
					event.preventDefault();
					login();
				}
				form.classList.add('was-validated');
			}, false);
		});
	}, false);
})();

function login() {
	var email = $('input[name="email"]').val();
	var password = $('input[name="password"]').val();
	var hasError = false;
	if (email == '') {
		hasError = true;
		globals.showToastMessage('Error', 'Please provide your email or username', 'error');
	} else if (email.length < 3) {
		hasError = true;
		globals.showToastMessage('Error', 'Please enter correct email or username', 'error');
	}
	if (password == '') {
		hasError = true;
		globals.showToastMessage('Error', 'Please enter your password', 'error');
	} else if (password.length < 6) {
		hasError = true;
		globals.showToastMessage('Error', 'Please enter correct password', 'error');
	}
	if ( !hasError ) {
		$('#Signup').text('Logging...');
		$('#Signup').addClass('loading disabled');
		$('#Signup').prop('disabled', true);
		let params = {
			email: $('input[name="email"]').val(),
			password: $('input[name="password"]').val(),
			method: 'post',
			url: 'login/auth'
		}
		globals.ajaxService.request(params, function successCallback(data) {
			$('#Signup').removeClass('loading');
			$('#Signup').removeClass('disabled');
			$('#Signup').text('Please wait...');
			//globals.showToastMessage('Success', data, 'success');
			setTimeout(() => {
				window.location.href = BASE_URL+'dashboard';
			}, 0);
		}, function errorCallback(error) {
			$('#Signup').text('Login');
			$('#Signup').removeClass('loading');
			$('#Signup').removeClass('disabled');
			globals.showToastMessage('Error', error, 'error');
			$('#Signup').prop('disabled', false);
		});
	}
}